#include "map_loader.h"

#include "jobs.h"
#include "prefs.h"
#include "reader.h"
#include "stretchy_buffer.h"

Map* load_map(const char* path) {
    FILE* fp = fopen(path, "rb");
    if (fp == NULL) {
        fprintf(stderr, "Failed to open file: %s\n", path);
        return NULL;
    }
    fseek(fp, 0, SEEK_END);
    int count = ftell(fp);
    rewind(fp);
    uint8_t* buffer = malloc(count);
    fread(buffer, count, 1, fp);
    fclose(fp);

    uint8_t* r = buffer;
    Map* m = map_loader_deserialize(&r);
    free(buffer);
    return m;
}

/*
 * Deserialize map
 * Returns NULL on failure.
 * TODO somehow prevent buffer overruns
 */
Map* map_loader_deserialize(uint8_t** r) {
    int version = r_i32(r);

    if (version != MAP_BINARY_VERSION) {
        return NULL;
    }

    char* name = r_str(r);
    char* author = r_str(r);

    int size = r_i32(r);

    Map* m = map_create(size, name, author);

    if (m == NULL) return NULL;

    for (int i = 0; i < m->size * m->size; i++) {
        m->tile_types[i] = r_u8(r);
    }
    for (int i = 0; i < m->size * m->size; i++) {
        m->tile_resource_counts[i] = r_u8(r);
    }

    int building_count = r_i32(r);
    Building* b = sbadd(m->buildings, building_count);
    for (int i = 0; i < building_count; i++) {
        map_loader_building(b, r);
        b++;
    }

    int penguin_count = r_i32(r);
    Penguin* p = sbadd(m->penguins, penguin_count);
    for (int i = 0; i < penguin_count; i++) {
        map_loader_penguin(p, r);
        job_load(r, &p->job);
        p++;
    }

    int snowball_count = r_i32(r);
    Snowball* s = sbadd(m->snowballs, snowball_count);
    for (int i = 0; i < snowball_count; i++) {
        map_loader_snowball(s, r);
        s++;
    }

    map_recalculate_state_views(m);
    return m;
}

void map_loader_building(Building* b, uint8_t** r) {
    b->type = r_u8(r);
    b->tile_x = r_i32(r);
    b->tile_y = r_i32(r);
    b->owner_id = r_i32(r);
    b->health = r_i32(r);
    b->progress = r_i32(r);
    b->level = r_u8(r);
    b->status = r_u8(r);
    b->changed = false;
    for (int j = 0; j < RESOURCE_TYPE_END; j++) {
        b->resources[j] = r_i32(r);
    }
}

void map_loader_penguin(Penguin* p, uint8_t** r) {
    p->type = r_u8(r);
    p->cargo = r_u8(r);
    p->x = r_i32(r);
    p->y = r_i32(r);
    p->owner_id = r_i32(r);
    p->colony_id = r_i32(r);
    p->health = r_i32(r);
    p->changed = false;
}

void map_loader_snowball(Snowball* s, uint8_t** r) {
    s->x = r_i32(r);
    s->y = r_i32(r);
    s->target_x = r_i32(r);
    s->target_y = r_i32(r);
    s->progress = r_i32(r);
    s->total_time = r_i32(r);
    s->used = r_u8(r);
}
