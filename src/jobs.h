#ifndef __SB2_JOBS__
#define __SB2_JOBS__
#include <stdint.h>
#include <stdio.h>

#include "types.h"

enum job_type {
    JOB_TYPE_MOVE = 0,
    JOB_TYPE_HARVEST,
    JOB_TYPE_UNLOAD,
    JOB_TYPE_SEARCH_RESOURCE,
    JOB_TYPE_AIM,
    JOB_TYPE_REVIVE,
    JOB_TYPE_UPGRADE,
    JOB_TYPE_ENLIST,
    JOB_TYPE_IDLE,
};

typedef struct job_move {
    enum job_type type;  // JOB_TYPE_MOVE
    int x, y;
} JobMove;

typedef struct job_search_resource {
    enum job_type type;  // JOB_TYPE_SEARCH_RESOURCE
    enum resource_type resource;
} JobSearchResource;

// When a building is under construction, the unload job can be thought of as a
// "constructing" job. Functionally they are very similar for now. Differences
// will be in animation and how long the job takes.
typedef struct job_unload {
    enum job_type type;  // JOB_TYPE_UNLOAD
    int progress;
    int building;
} JobUnload;

typedef struct job_harvest {
    enum job_type type;  // JOB_TYPE_HARVEST
    int tile_x, tile_y;
    int progress;
    enum resource_type resource;
} JobHarvest;

typedef struct job_aim {
    enum job_type type;  // JOB_TYPE_AIM
    int progress;
} JobAim;

typedef struct job_revive {
    enum job_type type;  // JOB_TYPE_REVIVE
    int progress;
    int building;
} JobRevive;

typedef struct job_upgrade {
    enum job_type type;  // JOB_TYPE_UPGRADE
    int progress;
    int building;
} JobUpgrade;

typedef struct job_enlist {
    enum job_type type;  // JOB_TYPE_ENLIST
    int progress;
    int building;
} JobEnlist;

typedef union job {
    enum job_type type;
    JobMove move;
    JobHarvest harvest;
    JobUnload unload;
    JobSearchResource search_resource;
    JobAim aim;
    JobRevive revive;
    JobUpgrade upgrade;
    JobEnlist enlist;
} Job;

struct i_writer;
struct i_reader;

void job_save(struct i_writer*, const Job*);
void job_load(uint8_t**, Job*);

#endif
