#include "nav_path.h"

#include <assert.h>
#include <stdlib.h>

#include "../mat.h"
#include "../priority_queue.h"
#include "../queue.h"
#include "../stretchy_buffer.h"
#include "../utils.h"

#define LESS_THAN(a, b) ((a) < (b))
PRIORITY_QUEUE_TEMPLATE(pqueue, unsigned int, float, LESS_THAN)

// Using 1.4 tends to expand fewer nodes. It probably acts as a tie-breaker of
// some sort.
#define DIAGONAL_COST 1.4f
// #define DIAGONAL_COST M_SQRT2

extern const int building_sizes[BUILDING_TYPE_END];

// #define NAV_PATH_DEBUG

#ifdef NAV_PATH_DEBUG
#include <sys/ioctl.h>
#include <sys/time.h>
#include <unistd.h>

static int get_terminal_width() {
    struct winsize w;
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
    return w.ws_col;
}

#define TIME_BEGIN(n)             \
    struct timeval start_time##n; \
    gettimeofday(&start_time##n, 0)

#define TIME_END(n)                                                 \
    struct timeval end_time##n;                                     \
    gettimeofday(&end_time##n, 0);                                  \
    printf("Timer '" #n "' took %ld\n",                             \
           ((end_time##n.tv_sec - start_time##n.tv_sec) * 1000000 + \
            end_time##n.tv_usec - start_time##n.tv_usec))

#endif  // NAV_PATH_DEBUG

static inline uint8_t get_obstacle(const uint8_t* obstacles, int i) {
    return (obstacles[i / 8] & (1 << i % 8)) ? 1 : 0;
}

static inline void set_obstacle(uint8_t* obstacles, int i) {
    obstacles[i / 8] |= 1 << i % 8;
}

static inline void unset_obstacle(uint8_t* obstacles, int i) {
    obstacles[i / 8] = ~((~obstacles[i / 8]) | 1 << i % 8);
}

static void get_neighbors(const NavPath* np, int* neighbors, bool* diagonal,
                          int* count, int i, int s, bool skip_blocked) {
    assert(i >= 0 && i < s * s && s >= 0);
    int x = i % s;
    int y = i / s;
    *count = 0;

    for (int y_ = -1; y_ < 2; y_++) {
        for (int x_ = -1; x_ < 2; x_++) {
            if (!(x_ || y_)) continue;  // Skip if same tile
            // if (x_ && y_) continue; // Skip diagonals
            int nx = x + x_;
            if (nx < 0 || nx >= s) continue;
            int ny = y + y_;
            if (ny < 0 || ny >= s) continue;
            if (skip_blocked && get_obstacle(np->obstacles, ny * s + nx))
                continue;
            // Skip if it's diagonal and there's a corner
            if (skip_blocked && x_ && y_ &&
                (get_obstacle(np->obstacles, y * s + nx) ||
                 get_obstacle(np->obstacles, ny * s + x)))
                continue;
            neighbors[*count] = ny * s + nx;
            diagonal[*count] = x_ && y_;
            (*count)++;
        }
    }
    assert(*count <= 8);
}

static int get_nearest_empty_tile(NavPath* np, int i, int s) {
    for (int r = 1;; r++) {
        for (int y = -r; y <= r; y++) {
            if (!get_obstacle(np->obstacles, i + s * y - r))
                return i + s * y - r;
            if (!get_obstacle(np->obstacles, i + s * y + r))
                return i + s * y + r;
        }
        for (int x = -r; x <= r; x++) {
            if (!get_obstacle(np->obstacles, i - s * r + x))
                return i - s * r + x;
            if (!get_obstacle(np->obstacles, i + s * r + x))
                return i + s * r + x;
        }
    }
}

static int get_smallest_neighbor(float* m, int i, int s) {
    int x = i % s;
    int y = i / s;
    int smallest = i - 1;

    for (int y_ = -1; y_ < 2; y_++) {
        for (int x_ = -1; x_ < 2; x_++) {
            if (!(x_ && y_)) continue;

            int nx = x + x_;
            if (nx < 0 || nx >= s) continue;
            int ny = y + y_;
            if (ny < 0 || ny >= s) continue;

            int ni = ny * s + nx;
            if (m[ni] < m[smallest]) {
                smallest = ni;
            }
        }
    }
    return smallest;
}

void nav_path_gen(NavPath* nav_path, const Map* map) {
    int size = nav_path->map_size;
    assert(size == map->size);

    // Update the obstacles
    memset(nav_path->obstacles, 0, size * size / 8 + 1);
    for (int i = 0; i < size * size; i++) {
        if (map->tile_types[i] != MAP_TILE_TYPE_EMPTY) {
            set_obstacle(nav_path->obstacles, i);
        }
    }

    // Mark each building as an obstacle and save the ammount of colonies.
    int num_colonies = 0;
    sbforeachp (Building* b, map->buildings) {
        if (b->type == BUILDING_TYPE_END) continue;
        if (b->type == BUILDING_TYPE_COLONY) num_colonies++;
        for (int y = 0; y < building_sizes[b->type]; y++) {
            for (int x = 0; x < building_sizes[b->type]; x++) {
                set_obstacle(nav_path->obstacles,
                             (b->tile_y + y) * size + (b->tile_x + x));
            }
        }
    }

    // Update costs
    nav_path->costs =
        realloc(nav_path->costs, sizeof(*nav_path->costs) * size * size);

    // Update came_from
    nav_path->came_from = realloc(nav_path->came_from,
                                  sizeof(*nav_path->came_from) * size * size);

    // Update landmarks
    const int nl = num_colonies;
    sbresize(nav_path->landmarks, nl);
    nav_path->landmark_distances =
        realloc(nav_path->landmark_distances,
                sizeof(*nav_path->landmark_distances) * nl * size * size);

    // Set the landmark positions
    int i = 0;
    sbforeachp (Building* b, map->buildings) {
        if (b->type == BUILDING_TYPE_COLONY) {
            Landmark* l = &nav_path->landmarks[i];
            l->position = get_nearest_empty_tile(
                nav_path, b->tile_y * size + b->tile_x, size);
            l->distances = nav_path->landmark_distances + (i * size * size);
            i++;
        }
    }
    assert(i == nl);
    float max = 0.f;

    // Calculate the distance to each tile from each landmark
    for (int i = 0; i < nl * size * size; i++) {
        nav_path->landmark_distances[i] = 1000000.f;
    }
    int* queue = 0;
    queue_resize(queue, size * M_PI);
    sbforeachp (Landmark* l, nav_path->landmarks) {
        l->distances[l->position] = 0.f;
        queue_clear(queue);
        queue_push(queue, l->position);

        while (queue_count(queue)) {
            int current = queue_pop(queue);
            assert(l->distances[current] < 1000000.f);
            int neighbors[8];
            bool diagonals[8];
            int nc;
            get_neighbors(nav_path, neighbors, diagonals, &nc, current,
                          map->size, true);
            for (int n = 0; n < nc; n++) {
                int next = neighbors[n];
                float new_cost = l->distances[current] +
                                 (diagonals[n] ? DIAGONAL_COST : 1.f);
                if (new_cost < l->distances[next]) {
                    l->distances[next] = new_cost;
                    queue_push(queue, next);
                    if (new_cost > max) {
                        max = new_cost;
                    }
                }
            }
        }
    }
    queue_free(queue);
#ifdef NAV_PATH_DEBUG
    char m[size * size];
    for (int i = 0; i < size * size; i++) {
        m[i] = nav_path->landmark_distances[i] < max
                   ? (int)(nav_path->landmark_distances[i] / max * 26.f) + 'a'
                   : '-';
    }
    for (int y = 0; y < size; y++) {
        for (int x = 0; x < MIN(size, get_terminal_width() / 2); x++) {
            printf("%c ", m[y * size + x]);
        }
        printf("\n");
    }
#endif
}

static inline float distance_heuristic(int a, int b, int w) {
    int dx = abs(a % w - b % w);
    int dy = abs(a / w - b / w);
    return 1.f * (dx + dy) + (DIAGONAL_COST - 2.f * 1.f) * MIN(dx, dy);
    /*
    return MAX(dx, dy);
    return dx + dy;
    return sqrt(dx*dx + dy*dy);
    */
}

static float heuristic(const NavPath* np, int a, int b, int w) {
    float d = distance_heuristic(a, b, w);
    // Loop through all the landmarks, finding the longest estimated distance
    sbforeachp (Landmark* l, np->landmarks) {
        float dd = fabsf(l->distances[a] - l->distances[b]);
        if (dd > d) {
            d = dd;
        }
    }
    assert(d >= 0);
    return d;
}

// Fill path_out with a path at most max_path long from (sx, sy) to (gx, gy).
void nav_path_find_path(const NavPath* nav_path, const Map* map, int sx, int sy,
                        int gx, int gy, Path* path) {
    path->goal_x = gx;
    path->goal_y = gy;

    // A hack to make the goal enterable
    bool obstacle = get_obstacle(nav_path->obstacles, gy * map->size + gx);
    if (obstacle) unset_obstacle(nav_path->obstacles, gy * map->size + gx);

    int start = sy * map->size + sx;
    int goal = gy * map->size + gx;
    int size = map->size * map->size;

    if (start == goal) {
        path->length = 0;
        return;
    }

    pqueue q;
    pqueue_init(&q, heuristic(nav_path, start, goal, map->size));
    pqueue_push(&q, start, 1.f);

    for (int i = 0; i < size; i++) {
        nav_path->came_from[i] = -1;
        nav_path->costs[i] = 1000000.f;
    }

    nav_path->came_from[start] = 0;

    bool found_path = false;
    while (q.count > 0 && !found_path) {
        int current = pqueue_pop(&q);
        if (current == goal) {
            found_path = true;
            break;
        }

        int neighbors[8];
        bool diagonals[8];
        int nc;
        get_neighbors(nav_path, neighbors, diagonals, &nc, current, map->size,
                      true);
        for (int n = 0; n < nc; n++) {
            int next = neighbors[n];
            float new_cost =
                nav_path->costs[current] + (diagonals[n] ? DIAGONAL_COST : 1);
            if (nav_path->came_from[next] < 0 ||
                new_cost < nav_path->costs[next]) {
                nav_path->costs[next] = new_cost;
                /*
                // Prefer tiles in line between start and goal
                int dx1 = next%map->size - gx;
                int dy1 = next/map->size - gy;
                int dx2 = sx - gx;
                int dy2 = sy - gy;
                float cross = fabsf(dx1*dy2 - dx2*dy1);
                float h = heuristic(next, goal, map->size) + cross*0.0005f;
                */
                float h = heuristic(nav_path, next, goal, map->size);
                pqueue_push(&q, next, new_cost + h);
                nav_path->came_from[next] = current;
            }
        }
    }

    if (obstacle) set_obstacle(nav_path->obstacles, gy * map->size + gx);

    if (found_path) {
        // Follow the path
        uint16_t* p = malloc(sizeof(*p) * size / 2);
        p[0] = gx;
        p[1] = gy;
        int len = 1;
        int i = nav_path->came_from[goal];
        // Last checked node
        int lx = gx;
        int ly = gy;
        // Last appended node
        int lax = gx;
        int lay = gy;
        while (i != start) {
            int x = i % map->size;
            int y = i / map->size;
            /* Add the previous node if this node isn't on the same row or
             * collumn of the last appened node.
             *  @ <-- last appended
             *
             *  v-- previous node
             *  @ @ <-- this node; different row and collumn
             */
            if (x != lax && y != lay) {
                p[len * 2] = lx;
                p[len * 2 + 1] = ly;
                lax = lx;
                lay = ly;
                len++;
            }
            lx = x;
            ly = y;
            i = nav_path->came_from[i];
        }
        // Fill path->tiles with as much of the end of p[] as we can fit
        path->length = MIN(len, MAX_NAV_PATH);
        int offset = MAX(0, len - MAX_NAV_PATH);
        memcpy(path->tiles, p + offset * 2,
               sizeof(uint16_t) * path->length * 2);
        free(p);
    } else {
        // printf("FAILED\n");
        path->length = 0;
        return;
    }

#ifdef NAV_PATH_DEBUG

    uint8_t m[size];
    printf("\n");
    char* symbols[14] = {
        "\u2190 ",  // west
        "\u2191 ",  // north
        "\u2192 ",  // west
        "\u2193 ",  // sourth
        "\u2196 ",  // north-west
        "\u2197 ",  // north-east
        "\u2198 ",  // south-east
        "\u2199 ",  // south-west
        "@ ",      "$ ", "M ", "# ", "* ", "- ",
    };
    for (int i = 0; i < size; i++) {
        m[i] = 13;
        int f = came_from[i];
        if (i == start)
            m[i] = 8;
        else if (i == goal)
            m[i] = 9;
        else if (map->tile_types[i] == MAP_TILE_TYPE_TREE)
            m[i] = 10;
        else if (get_obstacle(nav_path->obstacles, i))
            m[i] = 11;
        else if (f == i - 1)
            m[i] = 0;
        else if (f == i - map->size)
            m[i] = 1;
        else if (f == i + 1)
            m[i] = 2;
        else if (f == i + map->size)
            m[i] = 3;
        else if (f == i - 1 - map->size)
            m[i] = 4;
        else if (f == i + 1 - map->size)
            m[i] = 5;
        else if (f == i + 1 + map->size)
            m[i] = 6;
        else if (f == i - 1 + map->size)
            m[i] = 7;
    }
    /*
       for (int i=0; i<path->length; i++) {
       m[path->tiles[i*2+1] * map->size + path->tiles[i*2]] = 12;
       }
       */
    for (int y = 0; y < map->size; y++) {
        for (int x = 0; x < MIN(map->size, get_terminal_width() / 2); x++) {
            printf(symbols[m[y * map->size + x]]);
        }
        printf("\n");
    }
#endif
}
