const std = @import("std");

pub const SpiralGenerator = struct {
    // The starting point
    x: i32,
    y: i32,

    // Directions: right, up, left, down
    directions: [4][2]i32,

    // Initializing step size and direction index
    step_size: i32,
    direction_index: usize,

    step_index: i32,

    pub fn init() SpiralGenerator {
        return SpiralGenerator{
            .x = 0,
            .y = 0,
            .directions = .{ .{ 1, 0 }, .{ 0, 1 }, .{ -1, 0 }, .{ 0, -1 } },
            .step_size = 1,
            .direction_index = 0,
            .step_index = 0,
        };
    }

    // This function returns the next point in the spiral
    pub fn next(self: *SpiralGenerator) ?[2]i32 {
        if (self.step_size == 0) {
            return null; // Done with the generation (can add exit condition)
        }

        if (self.step_index < self.step_size) {
            // Update position
            self.step_index += 1;
            self.x += self.directions[self.direction_index][0];
            self.y += self.directions[self.direction_index][1];
            return .{ self.x, self.y };
        }

        self.step_index = 0;
        if (self.direction_index == 3 or (self.step_size == 1 and self.direction_index % 2 != 0)) {
            self.step_size += 1;
        }
        self.direction_index = (self.direction_index + 1) % 4;

        return self.next();
    }
};

test "spiral generator" {
    var gen = SpiralGenerator.init(0, 0);
    const values = [_][2]i32{
        .{ 1, 0 },
        .{ 1, 1 },
        .{ 0, 1 },
        .{ -1, 1 },
        .{ -1, 0 },
        .{ -1, -1 },
        .{ 0, -1 },
        .{ 1, -1 },

        .{ 2, -1 },
        .{ 2, 0 },
    };

    for (values) |point| {
        try std.testing.expectEqual(point, gen.next());
    }
}
