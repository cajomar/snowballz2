#ifndef __SB2_MAP_RESOURCE_NAV__
#define __SB2_MAP_RESOURCE_NAV__
#include <stdint.h>

#include "../map.h"

/**
 * Navigation map for finding a resource;
 */
typedef struct map_resource_nav {
    enum map_tile_type type;
    int map_size;
    int32_t* map;
} NavResource;

void nav_resource_init();
void nav_resource_shutdown();
void nav_resource_gen(NavResource* nav, const Map* map);
int32_t nav_resource_get(const NavResource* nav, int tile_x, int tile_y);

#endif
