#ifndef __SB2_GAME_AI__
#define __SB2_GAME_AI__

#include "../map.h"

void game_ai_step(float dt, Map* map);
void game_ai_init(Map* map);
void game_ai_shutdown(void);
void game_ai_on_new_penguin(Penguin* p);
void game_ai_clear_penguin_assignment(int penguin_idx);
void game_ai_assign_penguin_to_building(Map* map, int penguin_idx,
                                        int building_idx);

#endif
