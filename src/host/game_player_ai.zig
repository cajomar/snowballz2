const c = @cImport(@cInclude("game_player_ai.h"));
const std = @import("std");

const SpiralGenerator = @import("spiral_search.zig").SpiralGenerator;

pub fn sbslice(sb: anytype) []@TypeOf(sb.*) {
    // Create and return a Zig slice from the C array pointer and the length
    return sb[0..c.zig_sbcount(sb)];
}

const Ctx = struct {
    player_id: i32,
    map: *const c.Map,
    messages: [*c][*c]c.struct_player_message,

    fn find_colony(self: Ctx) !usize {
        const buildings = sbslice(self.map.buildings);
        for (buildings, 0..) |b, i| {
            if (b.owner_id == self.player_id and b.type == c.BUILDING_TYPE_COLONY) {
                return i;
            }
        }
        return error.NoColonyFound;
    }

    fn build_an_igloo(self: Ctx) !void {
        const colony = try self.find_colony();

        const cx =
            self.map.buildings[colony].tile_x;
        const cy =
            self.map.buildings[colony].tile_y;

        var gen = SpiralGenerator.init();

        while (gen.next()) |point| {
            const x = cx + point[0] * 4;
            const y = cy + point[1] * 4;
            if (c.map_can_place_building_at(self.map, c.BUILDING_TYPE_IGLOO, x, y)) {
                self.place_building(c.BUILDING_TYPE_IGLOO, x, y);
                break;
            }
        }
    }

    fn place_building(self: Ctx, t: c.building_type, tile_x: i32, tile_y: i32) void {
        const w = c.game_player_ai_push_message(self.messages, self.player_id);
        c.w_u8(w, c.CMD_place_building);
        c.w_u8(w, @intCast(t));
        c.w_i32(w, tile_x);
        c.w_i32(w, tile_y);
    }
};

export fn game_player_ai(map: *c.Map, player_id: i32, messages: [*c][*c]c.struct_player_message) void {
    const buildings = map.buildings[0..c.zig_sbcount(map.buildings)];
    var ctx: Ctx = .{ .map = map, .player_id = player_id, .messages = messages };

    for (buildings) |b| {
        if (b.owner_id == player_id and b.type == c.BUILDING_TYPE_IGLOO) {
            break;
        }
    } else {
        ctx.build_an_igloo() catch |err| {
            std.debug.print("No colony for {} {}\n", .{ player_id, err });
        };
    }
}
