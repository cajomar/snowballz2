#ifndef __SB2_GAME_NAV__
#define __SB2_GAME_NAV__

#include <stdbool.h>

#include "../map.h"

void game_nav_init(Map*);
void game_nav_shutdown(void);
void game_nav_on_new_penguin(Penguin*);
void game_nav_step(float dt, Map* map);
bool game_nav_target_reached(int penguin_index);
void game_nav_tiles_changed(void);
int32_t game_nav_get_tree(int x, int y);

#endif
