#include "nav_resource.h"

#include <assert.h>

#include "../map.h"
#include "../stretchy_buffer.h"
#include "../utils.h"

extern const int building_sizes[BUILDING_TYPE_END];

#define BUFFERS_COUNT 5
static int32_t* buffers = 0;
static int buffer_size = 0;

void nav_resource_init() { assert(buffer_size == 0); }
void nav_resource_shutdown() {
    if (buffers) {
        free(buffers);
        buffers = 0;
    }
    buffer_size = 0;
}

void nav_resource_gen(NavResource* nav, const Map* map) {
    int size = nav->map_size;
    assert(size == map->size);
    int advance_size = size * size;
    if (buffer_size != size) {
        buffer_size = size;
        buffers =
            realloc(buffers, BUFFERS_COUNT * advance_size * sizeof(int32_t));
    }

    int max_distance = size * 2;

#define B(i) (buffers + (i) * advance_size)
    int32_t* obstacles_row = B(3);
    int32_t* obstacles_col = B(4);

    for (int i = 0; i < size * size; i++) {
        B(0)[i] = map->tile_types[i] == nav->type ? 0 : max_distance;
        obstacles_row[i] = 0;
        obstacles_col[i] = 0;
    }

    sbforeachp (Building* b, map->buildings) {
        if (b->type == BUILDING_TYPE_END) continue;
        for (int y = 0; y < building_sizes[b->type]; y++) {
            for (int x = 0; x < building_sizes[b->type]; x++) {
                // Store obstacles both in row order and column order.
                obstacles_row[(b->tile_y + y) * size + (b->tile_x + x)] =
                    max_distance;
                obstacles_col[(b->tile_x + x) * size + (b->tile_y + y)] =
                    max_distance;
            }
        }
    }

    // TODO Use simd on each pass

    for (int iter = 0; iter < 4; iter++) {
        for (int y = 0; y < size; y++) {
            // Sweep right, values getting higher the further from the target
            // resource we get.
            int32_t v = max_distance;
            for (int x = 0; x < size; x++) {
                int idx = y * size + x;
                v++;
                v = MIN(B(0)[idx], v);
                v = MAX(obstacles_row[idx], v);
                B(1)[idx] = v;
            }
        }

        for (int y = 0; y < size; y++) {
            // Sweep left
            int32_t v = max_distance;
            for (int x = size - 1; x >= 0; x--) {
                int idx = y * size + x;
                v++;
                v = MIN(B(1)[idx], v);
                v = MAX(obstacles_row[idx], v);
                // Store transposed for column traversal
                B(2)[y + x * size] = v;
            }
        }

        for (int x = 0; x < size; x++) {
            // Sweep top to bottom.
            int32_t v = max_distance;
            for (int y = 0; y < size; y++) {
                int idx = y + x * size;
                v++;
                v = MIN(B(2)[idx], v);
                v = MAX(obstacles_col[idx], v);
                B(1)[idx] = v;
            }
        }

        for (int x = 0; x < size; x++) {
            // Bottom up.
            int32_t v = max_distance;
            for (int y = size - 1; y >= 0; y--) {
                int idx = y + x * size;
                v++;
                v = MIN(B(1)[idx], v);
                v = MAX(obstacles_col[idx], v);
                B(0)[y * size + x] = v;
            }
        }
    }

    memcpy(nav->map, B(0), size * size * sizeof(*buffers));
#undef B
}

int32_t nav_resource_get(const NavResource* nav, int tile_x, int tile_y) {
    if (tile_x < 0 || tile_x >= nav->map_size || tile_y < 0 ||
        tile_y >= nav->map_size) {
        return nav->map_size;
    }
    return nav->map[tile_y * nav->map_size + tile_x];
}
