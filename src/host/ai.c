#include "ai.h"

#include <assert.h>
#include <stdio.h>

#include "../map.h"
#include "../mat.h"
#include "../rules.h"
#include "../stretchy_buffer.h"
#include "../types.h"
#include "../utils.h"
#include "nav.h"

#define AI_EMPTY_ASSIGNMENT -1

extern bool* host_tile_changed;

enum job_status {
    JOB_STATUS_UNCOMPLETED,
    JOB_STATUS_COMPLETED,
    JOB_STATUS_COMPLETED_ASSIGNMENT,
};

/**
 * Manages a penguin being assigned to a building.
 */
typedef struct penguin_ai {
    // For now, penguins can only be assigned to a building.
    // In the future we may change it so that you never set jobs directly,
    // instead assignments can be a specific job. We would then handle job
    // queueing here.
    int assigned_building;
} PenguinAi;

typedef struct ai {
    PenguinAi* penguins;  // stretchy_buffer
} Ai;

static Ai* ai = 0;

void game_ai_init(Map* map) {
    assert(ai == 0);
    ai = calloc(1, sizeof(*ai));
    sbforeachp (Penguin* p, map->penguins) {
        game_ai_on_new_penguin(p);
    }
}

void game_ai_shutdown(void) {
    if (ai) {
        sbfree(ai->penguins);
        free(ai);
        ai = 0;
    }
}

void game_ai_on_new_penguin(Penguin* p) {
    (void)p;  // No warning
    assert(ai != 0);
    PenguinAi* a = sbadd(ai->penguins, 1);
    a->assigned_building = AI_EMPTY_ASSIGNMENT;
}

static enum resource_type resource_for_building(const Building* b) {
    if (b->type == BUILDING_TYPE_IGLOO ||
        b->status == BUILDING_STATUS_CONSTRUCTING) {
        return RESOURCE_TYPE_TREE;
    } else {
        return RESOURCE_TYPE_END;
    }
}

static bool building_needs_resource(Building* b, enum resource_type r) {
    return (b->status == BUILDING_STATUS_CONSTRUCTING ||
            b->type == BUILDING_TYPE_IGLOO) &&
           r == RESOURCE_TYPE_TREE;
}

static bool harvest_tile(Map* map, Penguin* penguin, JobHarvest* job,
                         float dt) {
    assert(job->type == JOB_TYPE_HARVEST);

    int tile_idx = job->tile_x + job->tile_y * map->size;
    enum map_tile_type tt = map->tile_types[tile_idx];
    if (tt != resource_type_to_map_tile_type(job->resource)) {
        // No longer the expected resource at this tile.
        return true;
    }

    job->progress += dt * 1000;
    if (job->progress >= 1000) {
        // Finished harvesting.
        penguin->cargo = map_tile_type_to_resource_type(tt);
        map->tile_resource_counts[tile_idx]--;
        if (map->tile_resource_counts[tile_idx] == 0) {
            game_nav_tiles_changed();
            map->tile_types[tile_idx] = MAP_TILE_TYPE_EMPTY;
        }
        host_tile_changed[tile_idx] = true;
        return true;
    }
    return false;
}

static void clear_penguins_assigned_to_building(Map* map, int building_idx) {
    sbforeachp (Penguin* p, map->penguins) {
        if (p->type == PENGUIN_TYPE_END) continue;
        PenguinAi* a = &ai->penguins[stb__counter];
        if (a->assigned_building == building_idx) {
            game_ai_clear_penguin_assignment(stb__counter);
        }
    }
}

static void on_building_construction_progress(Building* b) {
    // Check if building has all resources needed.
    for (int i = 0; i < RESOURCE_TYPE_END; i++) {
        if (b->resources[i] < RULE_construct_building_cost[b->type][i]) {
            return;
        }
    }

    b->status = BUILDING_STATUS_IDLE;
    b->progress = 0;
    for (int i = 0; i < RESOURCE_TYPE_END; i++) {
        b->resources[i] -= RULE_construct_building_cost[b->type][i];
    }
    b->changed = true;
}

static bool unload(Map* map, Penguin* penguin, JobUnload* job, float dt) {
    assert(job->type == JOB_TYPE_UNLOAD);

    assert(penguin->cargo < RESOURCE_TYPE_END);

    job->progress += dt * 1000;

    Building* b = &map->buildings[job->building];
    if (b->status == BUILDING_STATUS_CONSTRUCTING) {
        if (job->progress >= RULE_construct_building_time[b->type]) {
            b->progress++;
            b->resources[penguin->cargo]++;
            penguin->cargo = RESOURCE_TYPE_END;
            on_building_construction_progress(b);
            b->changed = true;

            // FIXME Perhaps this should go in
            // `on_building_construction_progress()`
            if (b->status != BUILDING_STATUS_CONSTRUCTING) {
                clear_penguins_assigned_to_building(map, job->building);
            }

            return true;
        }
    } else {
        if (job->progress >= RULE_unload_time) {
            b->resources[penguin->cargo]++;
            penguin->cargo = RESOURCE_TYPE_END;
            b->changed = true;
            return true;
        }
    }
    return false;
}

static int get_max_penguin_health(enum penguin_type t) {
    switch (t) {
        case PENGUIN_TYPE_WORKER:
            return HEALTH_MAX_WORKER_PENGUIN;
        case PENGUIN_TYPE_WARRIOR:
            return HEALTH_MAX_WARRIOR_PENGUIN;
        default:
            assert(false);
            return 0;
    }
}

static bool revive(Map* map, Penguin* penguin, JobRevive* job, float dt) {
    assert(job->type == JOB_TYPE_REVIVE);

    Building* b = &map->buildings[job->building];
    if (b->resources[RESOURCE_TYPE_TREE] == 0) {
        return true;
    }

    job->progress += dt * 1000;
    if (job->progress >= 1000) {
        penguin->health = get_max_penguin_health(penguin->type);
        b->resources[RESOURCE_TYPE_TREE]--;
        b->changed = true;
        return true;
    }
    return false;
}

static bool upgrade(Map* map, Penguin* penguin, JobUpgrade* job, float dt) {
    assert(job->type == JOB_TYPE_UPGRADE);

    job->progress += dt * 1000;
    if (job->progress >= 5000) {
        penguin->type = PENGUIN_TYPE_WARRIOR;
        int idx = penguin - map->penguins;
        assert(idx < sbcount(map->penguins));
        game_ai_clear_penguin_assignment(idx);
    }
    return false;
}

static bool enlist(Map* map, Penguin* penguin, JobEnlist* job, float dt) {
    Building* b = &map->buildings[job->building];
    assert(b->type == BUILDING_TYPE_IGLOO);

    job->progress += dt * 1000;
    if (job->progress >= 1000 && b->resources[RESOURCE_TYPE_TREE] >= 2) {
        penguin->owner_id = b->owner_id;
        b->resources[RESOURCE_TYPE_TREE] -= 2;
        b->changed = true;
        return true;
    }
    return false;
}

static Snowball* spawn_snowball(Map* map) {
    Snowball* s;
    sbforeachp (s, map->snowballs) {
        if (!s->used) {
            goto initialize;
        }
    }
    s = sbadd(map->snowballs, 1);
initialize:
    s->used = true;
    s->changed = true;
    s->progress = 0;
    return s;
}

static void calculate_snowball_time(Snowball* s) {
    vec2 dist = {
        s->target_x - s->x,
        s->target_y - s->y,
    };
    s->total_time = vec2_len(dist) / SNOWBALL_SPEED * 1000.f;
}

/*
 * Return penguin id to throw at. Return NO_PENGUIN if not found
 */
static int find_penguin_to_throw_at(Map* map, Penguin* penguin) {
    // Initialize it to the maximum distance
    int nearest = MAX_SNOWBALL_DISTANCE * MAX_SNOWBALL_DISTANCE;
    int idx = NO_PENGUIN;
    sbforeachp (Penguin* p, map->penguins) {
        int d = DISTANCE_TO_SQUARED(p, penguin);
        if (d < nearest && p->health > 0 && p->owner_id != PLAYER_NEUTRAL &&
            p->owner_id != penguin->owner_id) {
            nearest = d;
            idx = stb__counter;
        }
    }
    return idx;
}

static bool make_snowball(Map* map, Penguin* penguin, JobAim* job, float dt) {
    assert(job->type == JOB_TYPE_AIM);
    job->progress += dt * 1000;
    if (job->progress >= 1500) {
        int target_idx = find_penguin_to_throw_at(map, penguin);
        if (target_idx != NO_PENGUIN) {
            Snowball* s = spawn_snowball(map);
            s->x = penguin->x;
            s->y = penguin->y;
            s->target_x = map->penguins[target_idx].x;
            s->target_y = map->penguins[target_idx].y;
            calculate_snowball_time(s);
            return true;
        }
    }
    return false;
}

static int find_nearest_igloo(int penguin_idx, const Map* map) {
    const Penguin* p = &map->penguins[penguin_idx];
    sbforeachp (const Building* b, map->buildings) {
        if (b->owner_id == p->owner_id &&
            b->resources[RESOURCE_TYPE_TREE] > 0) {
            return stb__counter;
        }
    }
    return NO_BUILDING;
}

static void assignment_ai(int penguin_idx, Map* map) {
    PenguinAi* a = &ai->penguins[penguin_idx];
    Penguin* p = &map->penguins[penguin_idx];
    if (p->type == PENGUIN_TYPE_END) return;

    if (p->health <= 0) {
        // Frozen penguins are idle
        if (p->job.type != JOB_TYPE_IDLE) {
            printf("Penguin %d is frozen\n", penguin_idx);
            p->job.type = JOB_TYPE_IDLE;
            p->changed = true;
        }
    } else if (p->health < 5) {
        // Find a building to warm up in
        int building_idx = find_nearest_igloo(penguin_idx, map);

        if (building_idx != NO_BUILDING) {
            printf("Penguin %d is freezing\n", penguin_idx);
            p->job.type = JOB_TYPE_REVIVE;
            p->job.revive.progress = 0;
            p->job.revive.building = building_idx;
            p->changed = true;
        }
    } else if (p->job.type == JOB_TYPE_IDLE) {
        if (a->assigned_building != AI_EMPTY_ASSIGNMENT) {
            // Do ai calculations based on assignment
            Building* b = &map->buildings[a->assigned_building];

            if (p->owner_id == b->owner_id) {
                switch (p->type) {
                    case PENGUIN_TYPE_WORKER:
                        if (building_needs_resource(b, p->cargo)) {
                            printf("Penguin %d will give his %d\n", penguin_idx,
                                   p->cargo);
                            p->job.type = JOB_TYPE_UNLOAD;
                            p->job.unload.building = a->assigned_building;
                            p->job.unload.progress = 0;
                            p->changed = true;
                        } else {
                            int r = resource_for_building(b);
                            if (r != RESOURCE_TYPE_END) {
                                printf("Penguin %d will search for %d\n",
                                       penguin_idx, r);
                                // Build the building or find resources
                                p->job.type = JOB_TYPE_SEARCH_RESOURCE;
                                p->job.search_resource.resource = r;
                                p->changed = true;
                            } else if (b->type == BUILDING_TYPE_BARRACKS) {
                                printf(
                                    "Penguin %d will become a superpenguin\n",
                                    penguin_idx);
                                p->job.type = JOB_TYPE_UPGRADE;
                                p->job.upgrade.building =
                                    ai->penguins[penguin_idx].assigned_building;
                                p->job.upgrade.progress = 0;
                                p->changed = true;
                            } else {
                                printf("Lazybones, %d\n", penguin_idx);
                                // Colonies don't need anything
                            }
                        }
                        break;
                    case PENGUIN_TYPE_WARRIOR:
                        printf(
                            "TODO Make warriors assigned to buildings do "
                            "something\n");
                        break;
                    case PENGUIN_TYPE_END:
                        assert(false);
                        break;
                }
            } else {
                // Penguin assigned to building owned by a different player
                assert(b->type == BUILDING_TYPE_IGLOO);
                printf("Penguin %d will defect to team %d\n", penguin_idx,
                       b->owner_id);
                p->job.type = JOB_TYPE_ENLIST;
                p->job.enlist.building = a->assigned_building;
                p->job.enlist.progress = 0;
                p->changed = true;
            }
        } else {
            // Invent some jobs
            switch (p->type) {
                case PENGUIN_TYPE_WORKER: {
                    int building_idx = find_nearest_igloo(penguin_idx, map);
                    if (building_idx != NO_BUILDING) {
                        printf("Assigning %d to %d\n", penguin_idx,
                               building_idx);
                        game_ai_assign_penguin_to_building(map, penguin_idx,
                                                           building_idx);
                    }
                } break;
                case PENGUIN_TYPE_WARRIOR:
                    if (p->owner_id != PLAYER_NEUTRAL &&
                        find_penguin_to_throw_at(map, p) != NO_PENGUIN) {
                        printf("%d will throw snowballz!\n", penguin_idx);
                        p->job.type = JOB_TYPE_AIM;
                        p->job.aim.progress = 0;
                        p->changed = true;
                    }
                    break;
                case PENGUIN_TYPE_END:
                    assert(false);
                    break;
            }
        }
    } else {
        printf("Penguin %d will keep doing his %d job\n", penguin_idx,
               p->job.type);
    }
}

void game_ai_assign_penguin_to_building(Map* map, int penguin_idx,
                                        int building_idx) {
    assert(ai != 0);
    assert(penguin_idx < sbcount(ai->penguins));
    ai->penguins[penguin_idx].assigned_building = building_idx;
    assignment_ai(penguin_idx, map);
}

void game_ai_clear_penguin_assignment(int penguin_idx) {
    ai->penguins[penguin_idx].assigned_building = AI_EMPTY_ASSIGNMENT;
}

static bool harvest_nearest_resource(Map* map, Penguin* p,
                                     enum resource_type resource) {
    int tile_x, tile_y;
    map_pixel_to_tile(map, p->x, p->y, &tile_x, &tile_y);
    if (map->tile_types[tile_y * map->size + tile_x] ==
        resource_type_to_map_tile_type(resource)) {
        p->job.type = JOB_TYPE_HARVEST;
        p->job.harvest.tile_x = tile_x;
        p->job.harvest.tile_y = tile_y;
        p->job.harvest.progress = 0;
        p->job.harvest.resource = resource;
        return true;
    }
    return false;
}

void game_ai_step(float dt, Map* map) {
    static float next_assignment_ai_step = 0.f;
    next_assignment_ai_step -= dt;
    if (next_assignment_ai_step <= 0.f) {
        // Only run assignment AI once a second.
        next_assignment_ai_step = 1.f;
        assert(sbcount(ai->penguins) == sbcount(map->penguins));
        for (int i = 0; i < sbcount(ai->penguins); i++) {
            assignment_ai(i, map);
        }
    }

    sbforeachp (Penguin* p, map->penguins) {
        if (p->type == PENGUIN_TYPE_END) continue;
        if (p->job.type != JOB_TYPE_IDLE) {
            if (game_nav_target_reached(stb__counter)) {
                p->changed = true;
                bool job_complete = false;
                switch (p->job.type) {
                    case JOB_TYPE_MOVE:
                        job_complete = game_nav_target_reached(stb__counter);
                        break;
                    case JOB_TYPE_HARVEST:
                        job_complete =
                            harvest_tile(map, p, &p->job.harvest, dt);
                        break;
                    case JOB_TYPE_UNLOAD:
                        job_complete = unload(map, p, &p->job.unload, dt);
                        break;
                    case JOB_TYPE_SEARCH_RESOURCE:
                        if (harvest_nearest_resource(
                                map, p, p->job.search_resource.resource)) {
                            assert(p->job.type == JOB_TYPE_HARVEST);
                            job_complete =
                                harvest_tile(map, p, &p->job.harvest, dt);
                        } else {
                            printf("No harvesting\n");
                        }
                        break;
                    case JOB_TYPE_AIM:
                        job_complete = make_snowball(map, p, &p->job.aim, dt);
                        break;
                    case JOB_TYPE_REVIVE:
                        job_complete = revive(map, p, &p->job.revive, dt);
                        break;
                    case JOB_TYPE_UPGRADE:
                        job_complete = upgrade(map, p, &p->job.upgrade, dt);
                        break;
                    case JOB_TYPE_ENLIST:
                        job_complete = enlist(map, p, &p->job.enlist, dt);
                    default:
                        break;
                }

                if (job_complete) {
                    printf("Penguin %d did a %d job well done\n", stb__counter,
                           p->job.type);
                    p->job.type = JOB_TYPE_IDLE;
                }
            }
        }
    }
}
