#ifndef __SB2_GAME_PLAYER_AI__
#define __SB2_GAME_PLAYER_AI__

#include "../map.h"
#include "../writer.h"
#include "game_host.h"

struct player_message {
    struct i_writer writer;
    int player_id;
};

void game_player_ai(Map* map, int player_id, struct player_message**);
struct i_writer* game_player_ai_push_message(struct player_message** messages,
                                             int player_id);
size_t zig_sbcount(void*);

#endif
