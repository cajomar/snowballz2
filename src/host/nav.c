// Handles penguin navigation and movment
// This is a sub-module of game.c, and will only be initialized while game is
// initialized.

#include "nav.h"

#include <assert.h>
#include <math.h>

#include "../map.h"
#include "../mat.h"
#include "../stretchy_buffer.h"
#include "nav_path.h"
#include "nav_resource.h"

typedef struct penguin_nav {
    vec2 velocity;
    vec2 position;
    vec2 goto_target;
    bool target_reached;
    Path path;
} PenguinNav;

typedef struct nav {
    PenguinNav* penguins;  // stretchy_buffer
    NavResource nav_tree;
    NavPath nav_path;
} Nav;

static Nav* nav = 0;

void game_nav_init(Map* map) {
    assert(nav == 0);
    nav = calloc(1, sizeof(*nav));
    sbforeachp (Penguin* p, map->penguins) {
        game_nav_on_new_penguin(p);
    }

    nav_resource_init();

    nav->nav_tree.type = MAP_TILE_TYPE_TREE;
    nav->nav_tree.map_size = map->size;
    nav->nav_tree.map =
        malloc(sizeof(*nav->nav_tree.map) * map->size * map->size);
    nav_resource_gen(&nav->nav_tree, map);

    nav->nav_path.map_size = map->size;
    nav->nav_path.obstacles = malloc(map->size * map->size / 8 + 1);
    nav_path_gen(&nav->nav_path, map);
}

void game_nav_shutdown() {
    if (nav) {
        free(nav->nav_tree.map);
        sbfree(nav->nav_path.landmarks);
        free(nav->nav_path.landmark_distances);
        free(nav->nav_path.obstacles);
        free(nav->nav_path.came_from);
        free(nav->nav_path.costs);
        sbfree(nav->penguins);
        free(nav);
        nav = 0;
    }
    nav_resource_shutdown();
}

void game_nav_on_new_penguin(Penguin* p) {
    assert(nav != 0);
    PenguinNav* n = sbadd(nav->penguins, 1);
    n->velocity[0] = 1;
    n->velocity[1] = 0;
    n->position[0] = p->x;
    n->position[1] = p->y;
    n->goto_target[0] = p->x;
    n->goto_target[1] = p->y;
    n->target_reached = false;
    n->path.length = 0;
    n->path.goal_x = 0;
    n->path.goal_y = 0;
}

static void rule_nav(PenguinNav* n, float* vec) {
    vec2 mass = {0, 0};
    vec2 avoid = {0, 0};
    vec2 velocity = {0, 0};
    int count_mass = 0;
    int count_velocity = 0;
    float max_attract_distance = 150.f * 150.f;
    float max_vel_match_distance = 100.f * 100.f;
    float min_attract_distance = 80.f * 80.f;
    float max_avoid_distance = 50.f * 50.f;
    sbforeachp (PenguinNav* c, nav->penguins) {
        if (c == n) continue;
        vec2 dist;
        vec2_sub(dist, c->position, n->position);
        float distance = vec2_mul_inner(dist, dist);
        if (distance < max_attract_distance &&
            distance > min_attract_distance) {
            // Accumulate center of mass attraction
            vec2_add(mass, mass, c->position);
            count_mass++;
        }

        if (distance < max_vel_match_distance) {
            // Accumulate to match velocity
            vec2_add(velocity, velocity, c->velocity);
            count_velocity++;
        }

        if (distance < max_avoid_distance) {
            // Avoid nearby penguins
            vec2 sub;
            vec2_sub(sub, c->position, n->position);
            vec2_sub(avoid, avoid, sub);
            vec2_scale(avoid, avoid, 0.08f);
        }
    }
    if (count_mass) {
        vec2_scale(mass, mass, 1.f / count_mass);
        mass[0] = (mass[0] - n->position[0]) * 0.0001f;
        mass[1] = (mass[1] - n->position[1]) * 0.0001f;
    }

    if (count_velocity) {
        vec2_scale(velocity, velocity, 1.f / count_velocity);
        vec2_sub(velocity, velocity, n->velocity);
        vec2_scale(velocity, velocity, 0.2f);
    }

    vec2_add(vec, mass, avoid);
    // TODO use velocity when traveling together.
    // vec2_add(vec, vec, velocity);
}

static void limit_magnitude(float* vec, float max) {
    float magnitude = vec2_mul_inner(vec, vec);
    if (magnitude > max) {
        vec2 d;
        vec2_scale(d, vec, 1.f / magnitude);
        vec2_scale(vec, d, max);
    }
}

static void rule_goto(PenguinNav* n, float* vec, Map* map) {
    int tile_x, tile_y;
    map_pixel_to_tile(map, n->position[0], n->position[1], &tile_x, &tile_y);
    int gx, gy;
    map_pixel_to_tile(map, n->goto_target[0], n->goto_target[1], &gx, &gy);

    // Recalculate a path if there aren't any more steps or if the destination
    // has changed.
    if (n->path.length <= 0 || gx != n->path.goal_x || gy != n->path.goal_y) {
        nav_path_find_path(&nav->nav_path, map, tile_x, tile_y, gx, gy,
                           &n->path);
    }

    if (n->path.length > 1) {
        // The last node is the goal

        // Have a path, follow it.
        int tx = n->path.tiles[(n->path.length - 1) * 2];
        int ty = n->path.tiles[(n->path.length - 1) * 2 + 1];

        // Pop the node if we're there
        if (tile_x == tx && tile_y == ty) {
            n->path.length--;
        }

        // Go to the center of the tile
        vec2 g;
        g[0] = tx * TILE_SIZE + TILE_SIZE / 2;
        g[1] = ty * TILE_SIZE + TILE_SIZE / 2;

        vec2_sub(vec, g, n->position);
        vec2_scale(vec, vec, 0.01f);
        limit_magnitude(vec, 4.f * 4.f);
        n->target_reached = false;

    } else {
        vec2_sub(vec, n->goto_target, n->position);
        float magnitude = vec2_mul_inner(vec, vec);

        float stop_within = 40.f * 40.f;
        n->target_reached = magnitude < stop_within;

        if (n->target_reached) {
            // Close to the target. How close should depend on the size of the
            // army. Try to slow down - trying harder the closer we are to the
            // middle.
            float power = -(stop_within - magnitude) / stop_within;
            vec2_scale(vec, n->velocity, power * 0.3f);
        } else {
            vec2_scale(vec, vec, 0.01f);
            limit_magnitude(vec, 4.f * 4.f);
        }
    }
}

static bool map_dirty = false;
void game_nav_tiles_changed() { map_dirty = true; }

void game_nav_step(float dt, Map* map) {
    if (map_dirty) {
        nav_resource_gen(&nav->nav_tree, map);
        nav_path_gen(&nav->nav_path, map);
        map_dirty = false;
        // TODO Check all the paths, make sure they're still good
    }
    vec2 v1;
    vec2 v2;
    sbforeachp (PenguinNav* n, nav->penguins) {
        Penguin* p = &map->penguins[stb__counter];

        if (p->job.type == JOB_TYPE_MOVE) {
            n->goto_target[0] = p->job.move.x;
            n->goto_target[1] = p->job.move.y;
        } else if (p->job.type == JOB_TYPE_HARVEST) {
            n->goto_target[0] =
                p->job.harvest.tile_x * TILE_SIZE + TILE_SIZE / 2;
            n->goto_target[1] =
                p->job.harvest.tile_y * TILE_SIZE + TILE_SIZE / 2;
        } else if (p->job.type == JOB_TYPE_SEARCH_RESOURCE) {
            int tile_x, tile_y;
            map_pixel_to_tile(map, p->x, p->y, &tile_x, &tile_y);
            int32_t here = nav_resource_get(&nav->nav_tree, tile_x, tile_y);
            if (here == 0) {
                n->goto_target[0] = tile_x * TILE_SIZE + TILE_SIZE / 2;
                n->goto_target[1] = tile_y * TILE_SIZE + TILE_SIZE / 2;
            } else {
                // TODO calculate vector to add to velocity based on adjacent
                // tiles instead of targeting a tile's center.
                for (int y = -1; y <= 1; y++) {
                    for (int x = -1; x <= 1; x++) {
                        int32_t v = nav_resource_get(&nav->nav_tree, tile_x + x,
                                                     tile_y + y);
                        if (v < here) {
                            n->goto_target[0] =
                                (tile_x + x) * TILE_SIZE + TILE_SIZE / 2;
                            n->goto_target[1] =
                                (tile_y + y) * TILE_SIZE + TILE_SIZE / 2;
                            break;
                        }
                    }
                }
            }
        } else if (p->job.type == JOB_TYPE_UNLOAD) {
            Building* b = &map->buildings[p->job.unload.building];
            // TODO building size
            n->goto_target[0] = b->tile_x * TILE_SIZE + TILE_SIZE / 2;
            n->goto_target[1] = b->tile_y * TILE_SIZE + TILE_SIZE / 2;
        } else if (p->job.type == JOB_TYPE_REVIVE) {
            Building* b = &map->buildings[p->job.revive.building];
            // TODO building size
            n->goto_target[0] = b->tile_x * TILE_SIZE + TILE_SIZE / 2;
            n->goto_target[1] = b->tile_y * TILE_SIZE + TILE_SIZE / 2;
        } else if (p->job.type == JOB_TYPE_UPGRADE) {
            Building* b = &map->buildings[p->job.revive.building];
            // TODO building size
            n->goto_target[0] = b->tile_x * TILE_SIZE + TILE_SIZE / 2;
            n->goto_target[1] = b->tile_y * TILE_SIZE + TILE_SIZE / 2;
        } else if (p->job.type == JOB_TYPE_ENLIST) {
            Building* b = &map->buildings[p->job.enlist.building];
            // TODO building size
            n->goto_target[0] = b->tile_x * TILE_SIZE + TILE_SIZE / 2;
            n->goto_target[1] = b->tile_y * TILE_SIZE + TILE_SIZE / 2;
        } else {
            n->goto_target[0] = p->x;
            n->goto_target[1] = p->y;
        }

        bool fixed_location =
            p->health <= 0 ||
            (n->target_reached && (p->job.type == JOB_TYPE_HARVEST ||
                                   p->job.type == JOB_TYPE_UNLOAD));

        rule_nav(n, v1);
        rule_goto(n, v2, map);
        vec2_add(n->velocity, n->velocity, v1);
        vec2_add(n->velocity, n->velocity, v2);
        limit_magnitude(n->velocity, 6.f * 6.f);

        if (!fixed_location) {
            vec2 v;
            vec2_scale(v, n->velocity, dt * 23.f);

            if (vec2_mul_inner(n->velocity, n->velocity) > 1.f * 1.f) {
                vec2_add(n->position, n->position, v);
            }

            int new_x = (int)n->position[0];
            int new_y = (int)n->position[1];
            if (new_x != p->x || new_y != p->y) {
                p->x = new_x;
                p->y = new_y;
                p->changed = true;
            }
        }
    }
}

bool game_nav_target_reached(int penguin_index) {
    assert(nav && penguin_index < sbcount(nav->penguins));
    return nav->penguins[penguin_index].target_reached;
}

// For debugging
int32_t game_nav_get_tree(int x, int y) {
    if (!nav || x < 0 || y < 0 || x > nav->nav_tree.map_size ||
        y > nav->nav_tree.map_size) {
        return 0;
    }
    return nav->nav_tree.map[y * nav->nav_tree.map_size + x];
}
