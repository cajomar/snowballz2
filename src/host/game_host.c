#include "game_host.h"
#include "ai.h"
#include "nav.h"
#include "../map.h"
#include "../map_loader.h"
#include "../stretchy_buffer.h"
#include "../map_saver.h"
#include "../reader.h"
#include "../utils.h"
#include "../writer.h"

#include <enet/enet.h>

#define MAX_PLAYERS 4

static ENetAddress address;
static ENetHost* server = 0;
static Map* host_map = 0;

static char* player_names[MAX_PLAYERS];
static bool player_connected[MAX_PLAYERS];

static struct i_writer cmd_writer = {.data=0};

bool* host_tile_changed = 0;

static void spawn_penguin(int player_id, int colony_id, int x, int y) {
    Penguin* p = sbadd(host_map->penguins, 1);
    p->type = PENGUIN_TYPE_WORKER;
    // p->type = PENGUIN_TYPE_WARRIOR;
    // p->type = rand() % PENGUIN_TYPE_END;
    p->x = x;
    p->y = y;
    p->owner_id = player_id;
    p->colony_id = colony_id;
    p->health = HEALTH_MAX_WORKER_PENGUIN;
    p->job.type = JOB_TYPE_IDLE;
    p->cargo = RESOURCE_TYPE_END;
    p->changed = true;
    game_nav_on_new_penguin(p);
    game_ai_on_new_penguin(p);
}

void game_host(const char* map_path) {
    assert(host_map == 0);
    assert(server == 0);
    Map* map = load_map(map_path);
    game_ai_init(map);
    game_nav_init(map);
    host_map = map;

    host_tile_changed = calloc(1, sizeof(bool) * map->size * map->size);

    address.host = ENET_HOST_ANY;
    address.port = NET_PORT;
    server = enet_host_create(&address, 32, 2, 0, 0);
    if (server == 0) {
        fprintf(stderr, "Error while trying to create an ENet server host.\n");
        exit(EXIT_FAILURE);
    }

    for (int i=0; i<MAX_PLAYERS; i++) {
        player_names[i] = 0;
        player_connected[i] = false;
    }

    sbforeachp(Building* b, map->buildings) {
        if (b->type == BUILDING_TYPE_COLONY) {
            for (int i=0; i<4; i++) {
                spawn_penguin(PLAYER_NEUTRAL, stb__counter,
                    b->tile_x*TILE_SIZE + (i % 3)*PENGUIN_SIZE,
                    b->tile_y*TILE_SIZE + (i/3)*PENGUIN_SIZE);
            }
        }
    }
}

static int connect_new_player(const char* name) {
    for (int player=PLAYER_NEUTRAL+1; player<MAX_PLAYERS; player++) {
        if (player_connected[player]) continue;
        player_names[player] = malloc(strlen(name)+1);
        strcpy(player_names[player], name);
        player_connected[player] = true;

        sbforeachp(Building* b, host_map->buildings) {
            if (b->type == BUILDING_TYPE_COLONY) {
                if (b->owner_id == player) {
                    // This player has already been spawned.
                    break;
                }
                if (b->owner_id == PLAYER_SPAWNPOINT) {
                    // Mark this colony as this player's spawn point. Players
                    // can't own colony buildings but this way we know that if
                    // a player re-connects not to re-spawn.
                    b->owner_id = player;
                    int building_id = stb__counter;
                    sbforeachp(Penguin* p, host_map->penguins) {
                        if (p->colony_id == building_id) {
                            p->owner_id = player;
                        }
                    }
                    break;
                }
            }
        }

        return player;
    }
    printf("Max players in game. TODO reject connection.\n");
    return 0;
}

static void send_entire_map(Map* map) {
    uint8_t* data = map_saver_serialize(map);
    int count = sbcount(data);
    ENetPacket* packet = enet_packet_create(NULL, 1 + count, ENET_PACKET_FLAG_RELIABLE);
    packet->data[0] = MSG_entire_map;
    memcpy(packet->data+1, data, count);
    enet_host_broadcast(server, 0, packet);
    sbfree(data);
}

static void send_assignment_info(ENetPeer* peer, int player) {
    ENetPacket* packet = enet_packet_create(NULL, 1 + 1, ENET_PACKET_FLAG_RELIABLE);
    packet->data[0] = MSG_player_id;
    packet->data[1] = player;
    enet_peer_send(peer, 0, packet);
}

static void game_host_command(enum message_type msg_type) {
    int count = sbcount(cmd_writer.data);
    ENetPacket* packet = enet_packet_create(NULL,
            1 + count,
            ENET_PACKET_FLAG_RELIABLE);
    packet->data[0] = msg_type;
    memcpy((char*)packet->data+1, cmd_writer.data, count);
    enet_host_broadcast(server, 0, packet);
    sbresize(cmd_writer.data, 0);
}

static void game_host_execute_command(uint8_t**, int player);

void game_host_step(float dt) {
    if (server == 0) return;
    ENetEvent event;
    while (enet_host_service(server, &event, 0)) {
        switch (event.type) {
            case ENET_EVENT_TYPE_CONNECT:
                printf("Server: new connection from %x:%u\n",
                        event.peer->address.host,
                        event.peer->address.port);
                break;
            case ENET_EVENT_TYPE_RECEIVE: {
                const char* data = (const char*)(event.packet->data)+1;
                switch ((enum message_type) event.packet->data[0]) {
                    case MSG_player_name: {
                        int player = connect_new_player(data);
                        printf("Server: new player: %s (id: %d)\n",
                                player_names[player], player);
                        event.peer->data = (void*)(size_t)player;
                        send_assignment_info(event.peer, player);
                        send_entire_map(host_map);
                        break;
                    }
                    case MSG_player_command: {
                        uint8_t* r = (uint8_t*) data;
                        int player_id = (int)(size_t)event.peer->data;
                        game_host_execute_command(&r, player_id);
                        break;
                    }
                    default:
                        printf("Server got unexpected message type: %d\n", event.packet->data[0]);
                        break;
                }
                enet_packet_destroy(event.packet);
                break;
            }
            case ENET_EVENT_TYPE_DISCONNECT: {
                int player_id = (int)(size_t)event.peer->data;
                player_connected[player_id] = false;
                free(player_names[player_id]);
                player_names[player_id] = 0;
                printf("Server: Player %d disconnected\n", player_id);
                event.peer->data = 0;
                break;
            }
            case ENET_EVENT_TYPE_NONE:
                break;
        }
    }

    game_nav_step(dt, host_map);
    game_ai_step(dt, host_map);

    sbforeachp(Snowball* s, host_map->snowballs) {
        if (!s->used) continue;

        s->progress += dt * 1000.f;

        if (s->progress > s->total_time) {
            sbforeachp(Penguin* p, host_map->penguins) {
                int d = distance_to_squared(p->x, p->y, s->target_x, s->target_y);
                if (d < PENGUIN_SIZE * PENGUIN_SIZE) {
                    p->health --;
                    p->changed = true;
                }
            }
            s->used = false;
            s->changed = true;
            continue;
        }
        // Let clients update snowball's position
        // s->changed = true;
    }


    static float send_period = 0;
    send_period += dt;
    if (send_period > 0.1f) {
        send_period -= 0.1f;

        // Send updated penguins.
        sbforeachp(Penguin* p, host_map->penguins) {
            if (p->changed) {
                p->changed = false;
                w_u8(&cmd_writer, MAP_penguin);
                w_i32(&cmd_writer, stb__counter);
                map_saver_penguin(&cmd_writer, p);
                job_save(&cmd_writer, &p->job);
                game_host_command(MSG_map_change);
            }
        }

        // Send snowballs
        sbforeachp(Snowball* s, host_map->snowballs) {
            if (s->changed) {
                s->changed = false;
                w_u8(&cmd_writer, MAP_snowball);
                w_i32(&cmd_writer, stb__counter);
                map_saver_snowball(&cmd_writer, s);
                game_host_command(MSG_map_change);
            }
        }

        // Send tile changes.
        int count = host_map->size*host_map->size;
        for (int i=0; i<count; i++) {
            if (host_tile_changed[i]) {
                w_u8(&cmd_writer, MAP_tile);
                w_i32(&cmd_writer, i);
                w_u8(&cmd_writer, host_map->tile_types[i]);
                w_u8(&cmd_writer, host_map->tile_resource_counts[i]);
                game_host_command(MSG_map_change);
                host_tile_changed[i] = false;
            }
        }
    }

    // Send updated buildings.
    sbforeachp(Building* b, host_map->buildings) {
        if (b->changed) {
            b->changed = false;
            w_u8(&cmd_writer, MAP_building);
            w_i32(&cmd_writer, stb__counter);
            map_saver_building(&cmd_writer, b);
            game_host_command(MSG_map_change);
        }
    }
}

void game_host_shutdown() {
    if (server != 0) {
        enet_host_destroy(server);
        server = 0;
    }
    if (host_map != 0) {
        map_destroy(host_map);
        host_map = 0;
    }

    if (host_tile_changed) {
        free(host_tile_changed);
        host_tile_changed = 0;
    }

    for (int i=0; i<MAX_PLAYERS; i++) {
        if (player_names[i]) free(player_names[i]);
    }

    sbfree(cmd_writer.data);
    cmd_writer.data = 0;

    game_nav_shutdown();
    game_ai_shutdown();
}

static void game_host_execute_command(uint8_t** r, int player) {
    enum player_command_type cmd = r_u8(r);
    if (cmd == CMD_place_building) {
        enum building_type building_type = r_u8(r);
        int tile_x = r_i32(r);
        int tile_y = r_i32(r);
        if (map_can_place_building_at(host_map, tile_x, tile_y)) {
            int building_id = map_building_add(host_map, building_type, tile_x, tile_y);
            Building* b = &host_map->buildings[building_id];
            b->owner_id = player;
            b->level = BUILDING_LEVEL_CONSTRUCTING;
            b->status = BUILDING_STATUS_CONSTRUCTING;
            game_nav_tiles_changed();
        }
    } else if (cmd == CMD_assign_penguin_to_building) {
        int penguin_idx = r_i32(r);
        int building_idx = r_i32(r);
        assert(penguin_idx < sbcount(host_map->penguins));
        assert(building_idx < sbcount(host_map->buildings));
        Penguin* p = &host_map->penguins[penguin_idx];
        if (p->type != PENGUIN_TYPE_END && p->owner_id == player) {
            p->job.type = JOB_TYPE_IDLE;
            game_ai_clear_penguin_assignment(penguin_idx);
            game_ai_assign_penguin_to_building(host_map, penguin_idx, building_idx);
        }
    } else if (cmd == CMD_assign_penguin_job) {
        int penguin_idx = r_i32(r);
        assert(penguin_idx < sbcount(host_map->penguins));
        Penguin* p = &host_map->penguins[penguin_idx];
        if (p->owner_id == player) {
            job_load(r, &p->job);
        }
    } else if (cmd == CMD_recruit_penguin) {
        int building_id = r_i32(r);
        assert(building_id < sbcount(host_map->buildings));
        Building* b = &host_map->buildings[building_id];
        if (b->resources[RESOURCE_TYPE_TREE] >= 2) {
            // TODO Find closest penguin
            sbforeachp(Penguin* p, host_map->penguins) {
                if (p->owner_id == PLAYER_NEUTRAL && p->job.type == JOB_TYPE_IDLE) {
                    p->job.type = JOB_TYPE_ENLIST;
                    p->job.enlist.building = building_id;
                    p->job.enlist.progress = 0;
                    break;
                }
            }
        }
    }
}
