#ifndef __SB2_GAME_HOST__
#define __SB2_GAME_HOST__

#include <stdbool.h>

#define NET_PORT 8765

enum message_type {
    MSG_player_name = 0,
    MSG_entire_map,
    MSG_player_command,
    MSG_map_change,
    MSG_game_over,
    MSG_start_game,

    // Sent to a connected peer to notify the client which player they are.
    MSG_player_id,
};

// Events clients send to the server with MSG_player_command events.
enum player_command_type {
    CMD_assign_penguin_to_building = 0,
    CMD_assign_penguin_job,
    CMD_place_building,
    CMD_upgrade_penguin,
    CMD_recruit_penguin,
};

// Events server sends to clients for MSG_map_change events.
enum map_change_type {
    MAP_building,
    MAP_penguin,
    MAP_tile,
    MAP_snowball,
};

bool game_host(const char* map_path, bool wait_for_players);
void game_host_step(float dt);
void game_host_shutdown(void);

#endif
