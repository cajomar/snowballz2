#ifndef __SB2_GAME_PATHFINDING__
#define __SB2_GAME_PATHFINDING__

#include "../map.h"

#define MAX_NAV_PATH 64

typedef struct game_path {
    uint16_t goal_x;
    uint16_t goal_y;
    int length;
    uint16_t tiles[MAX_NAV_PATH * 2];
} Path;

typedef struct landmark {
    // In flattened map coords
    int position;
    float* distances;  // pointer into NavPath.landmark_distances
} Landmark;

typedef struct game_nav_path {
    int map_size;
    float* landmark_distances;  // len = num_landmarks * map_size * map_size
    Landmark* landmarks;        // stretchy_buffer
    uint8_t* obstacles;
    float* costs;    // len = map_size * map_size
    int* came_from;  // len = map_size * map_size
} NavPath;

void nav_path_gen(NavPath* nav_path, const Map* map);
void nav_path_find_path(const NavPath* nav_path, const Map* map, int sx, int sy,
                        int gx, int gy, Path* path);
#endif
