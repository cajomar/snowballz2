#ifndef __SB2_READER__
#define __SB2_READER__
#include <stdint.h>
#include <stdio.h>

int32_t r_i32(uint8_t**);
int16_t r_i16(uint8_t**);
uint8_t r_u8(uint8_t**);
char* r_str(uint8_t**);

int32_t fp_i32(FILE* fp);
int16_t fp_i16(FILE* fp);
uint8_t fp_u8(FILE* fp);
char* fp_str(FILE* fp);

#endif
