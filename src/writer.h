#ifndef __SB2_WRITER__
#define __SB2_WRITER__
#include <stdint.h>

struct i_writer {
    uint8_t* data;  // stretchy_buffer
};

void w_i32(struct i_writer* w, int32_t v);
void w_i16(struct i_writer* w, int16_t v);
void w_u8(struct i_writer* w, uint8_t v);
void w_str(struct i_writer* w, const char* v);

#endif
