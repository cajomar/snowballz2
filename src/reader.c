#include "reader.h"

#include <stdlib.h>
#include <string.h>

int32_t r_i32(uint8_t** r) {
    union {
        uint8_t bytes[4];
        int32_t i;
    } fun;
    memcpy(fun.bytes, *r, 4);
    *r += 4;
    return fun.i;
}

int16_t r_i16(uint8_t** r) {
    union {
        uint8_t bytes[2];
        int16_t i;
    } fun;
    memcpy(fun.bytes, *r, 2);
    *r += 2;
    return fun.i;
}

uint8_t r_u8(uint8_t** r) {
    uint8_t d = **r;
    *r += 1;
    return d;
}

char* r_str(uint8_t** r) {
    int len = r_i32(r);
    char* s = malloc(len + 1);
    s[len] = '\0';
    memcpy(s, *r, len);
    *r += len;
    return s;
}

int32_t fp_i32(FILE* fp) {
    union {
        uint8_t bytes[4];
        int32_t i;
    } fun;
    fread(fun.bytes, 1, 4, fp);
    return fun.i;
}

int16_t fp_i16(FILE* fp) {
    union {
        uint8_t bytes[2];
        int16_t i;
    } fun;
    fread(fun.bytes, 1, 2, fp);
    return fun.i;
}

uint8_t fp_u8(FILE* fp) {
    uint8_t d;
    fread(&d, 1, 1, fp);
    return d;
}

char* fp_str(FILE* fp) {
    int len = fp_i32(fp);
    char* s = malloc(len + 1);
    s[len] = '\0';
    fread(s, len, 1, fp);
    return s;
}
