#include "map_saver.h"

#include <stdio.h>
#include <stdlib.h>

#include "jobs.h"
#include "prefs.h"
#include "stretchy_buffer.h"
#include "writer.h"

uint8_t* map_saver_serialize(Map* m) {
    struct i_writer w = {.data = 0};

    int version = 1;
    w_i32(&w, version);

    w_str(&w, m->name);
    w_str(&w, m->author);

    w_i32(&w, m->size);
    for (int i = 0; i < m->size * m->size; i++) {
        w_u8(&w, m->tile_types[i]);
    }
    for (int i = 0; i < m->size * m->size; i++) {
        w_u8(&w, m->tile_resource_counts[i]);
    }

    // TODO filter out buildings with BUILDING_TYPE_END and update all building
    // references.
    w_i32(&w, sbcount(m->buildings));
    sbforeachp (Building* b, m->buildings) {
        map_saver_building(&w, b);
    }

    // TODO filter out penguins with PENGUIN_TYPE_END and update all building
    // references.
    w_i32(&w, sbcount(m->penguins));
    sbforeachp (Penguin* p, m->penguins) {
        map_saver_penguin(&w, p);
        job_save(&w, &p->job);
    }

    w_i32(&w, sbcount(m->snowballs));
    sbforeachp (Snowball* s, m->snowballs) {
        map_saver_snowball(&w, s);
    }

    return w.data;
}

void map_saver_building(struct i_writer* w, const Building* b) {
    w_u8(w, b->type);
    w_i32(w, b->tile_x);
    w_i32(w, b->tile_y);
    w_i32(w, b->owner_id);
    w_i32(w, b->health);
    w_i32(w, b->progress);
    w_u8(w, b->level);
    w_u8(w, b->status);
    for (int j = 0; j < RESOURCE_TYPE_END; j++) {
        w_i32(w, b->resources[j]);
    }
}

void map_saver_penguin(struct i_writer* w, const Penguin* p) {
    w_u8(w, p->type);
    w_u8(w, p->cargo);
    w_i32(w, p->x);
    w_i32(w, p->y);
    w_i32(w, p->owner_id);
    w_i32(w, p->colony_id);
    w_i32(w, p->health);
}

void map_saver_snowball(struct i_writer* w, const Snowball* s) {
    w_i32(w, s->x);
    w_i32(w, s->y);
    w_i32(w, s->target_x);
    w_i32(w, s->target_y);
    w_i32(w, s->progress);
    w_i32(w, s->total_time);
    w_u8(w, s->used);
}

void save_map(Map* map, const char* dir) {
    char path[1024];
    const size_t dir_len = strlen(dir);
    const size_t name_len = strlen(map->name);

    if (dir_len + name_len + 5 > 1024) return;

    memcpy(path, dir, dir_len);

    for (int i = 0; i < name_len; i++) {
        char c = map->name[i];
        if ((c < 'A' || c > 'z') && !(c >= '0' && c <= '9')) {
            c = '_';
        }
        path[dir_len + i] = c;
    }
    strcpy(path + dir_len + name_len, ".sb2");

    FILE* fp = fopen(path, "wb");
    if (fp) {
        uint8_t* data = map_saver_serialize(map);
        fwrite(data, sbcount(data), 1, fp);
        fclose(fp);
        sbfree(data);
    } else {
        fprintf(stderr, "Error: failed to write to file %s\n", path);
    }
}
