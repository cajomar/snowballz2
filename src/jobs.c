#include "jobs.h"

#include "reader.h"
#include "writer.h"

void job_save(struct i_writer* w, const Job* job) {
    w_i32(w, job->type);
    switch (job->type) {
        case JOB_TYPE_MOVE:
            w_i32(w, job->move.x);
            w_i32(w, job->move.y);
            break;
        case JOB_TYPE_HARVEST:
            w_i32(w, job->harvest.tile_x);
            w_i32(w, job->harvest.tile_y);
            w_i32(w, job->harvest.progress);
            w_i32(w, job->harvest.resource);
            break;
        case JOB_TYPE_UNLOAD:
            w_i32(w, job->unload.progress);
            w_i32(w, job->unload.building);
            break;
        case JOB_TYPE_SEARCH_RESOURCE:
            w_i32(w, job->search_resource.resource);
            break;
        case JOB_TYPE_IDLE:
            break;
        case JOB_TYPE_AIM:
            w_i32(w, job->aim.progress);
            break;
        case JOB_TYPE_REVIVE:
            w_i32(w, job->revive.progress);
            w_i32(w, job->revive.building);
            break;
        case JOB_TYPE_UPGRADE:
            w_i32(w, job->upgrade.progress);
            w_i32(w, job->upgrade.building);
            break;
        case JOB_TYPE_ENLIST:
            w_i32(w, job->enlist.progress);
            w_i32(w, job->enlist.building);
            break;
    }
}

void job_load(uint8_t** r, Job* job) {
    job->type = r_i32(r);
    switch (job->type) {
        case JOB_TYPE_MOVE:
            job->move.x = r_i32(r);
            job->move.y = r_i32(r);
            break;
        case JOB_TYPE_HARVEST:
            job->harvest.tile_x = r_i32(r);
            job->harvest.tile_y = r_i32(r);
            job->harvest.progress = r_i32(r);
            job->harvest.resource = r_i32(r);
            break;
        case JOB_TYPE_UNLOAD:
            job->unload.progress = r_i32(r);
            job->unload.building = r_i32(r);
            break;
        case JOB_TYPE_SEARCH_RESOURCE:
            job->search_resource.resource = r_i32(r);
            break;
        case JOB_TYPE_IDLE:
            break;
        case JOB_TYPE_AIM:
            job->aim.progress = r_i32(r);
            break;
        case JOB_TYPE_REVIVE:
            job->revive.progress = r_i32(r);
            job->revive.building = r_i32(r);
            break;
        case JOB_TYPE_UPGRADE:
            job->enlist.progress = r_i32(r);
            job->enlist.building = r_i32(r);
            break;
        case JOB_TYPE_ENLIST:
            job->enlist.progress = r_i32(r);
            job->enlist.building = r_i32(r);
            break;
    }
}
