#ifndef __SB2_MAP_LOADER__
#define __SB2_MAP_LOADER__

#include "map.h"

#define MAP_BINARY_VERSION 1

Map* map_loader_deserialize(uint8_t**);
Map* load_map(const char* path);
void map_loader_building(Building* b, uint8_t** r);
void map_loader_penguin(Penguin* p, uint8_t** r);
void map_loader_snowball(Snowball* s, uint8_t** r);
int load_map_metadata(const char* path, char* name, char* author, int* size,
                      int string_max);

#endif
