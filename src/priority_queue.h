/*
 * My (Caleb Marshall <calebmrshl@gmail.com>) single-file header-only library
 * for priority queues.
 *
 * You can find a (possibly more recent) copy of this file at
 * https://bitbucket.org/cajomar/queue/raw/HEAD/priority_queue.h
 *
 * To use a priority queue you need to first define the types you want to use:
 *
 * #define LESS_THAN(a, b) ((a) < (b))
 * #define PRIORITY_QUEUE_TEMPLATE(pq, int, float, LESS_THAN)
 *
 * This will define a min heap priority queue type called `pq` of int values
 * and float priorities. You can now use it like this:
 *
 * pq q;
 * pq_init(&q, 100);        // Initalize the queue to hold 100 elements
 * pq_push(&q, 4, 1.5f);    // Enqueue 4 with a priority of 1.5
 * while (p.count) {        // q.count stores how many enqueued items there are
 *     int v = pq_pop(&q);  // Remove item with the smallest priority
 * }
 * pq_deinit(&q);           // Free heap allocations
 *
 *
 * When more items are queued than the buffer can hold, the queue will be
 * resized automatically. If you wish to be warned about this you can edit
 * the value of the QUEUE_RESIZED macro. It's defined just below the include
 * guard.
 *
 * The queue uses malloc to expand its buffer. If malloc fails, the
 * QUEUE_OUT_OF_MEMORY macro will be expanded and the queue won't be grown.
 * By default it prints an error message and aborts. You can edit the macro to
 * whatever you want.
 *
 * If you request a value from an empty queue the
 * QUEUE_INVALID_ACCESS macro will be expanded. By default it prints an error
 * message and returns 0. Edit it to whatever you want.
 */


#ifndef PRIORITY_QUEUE_H_I1PGXRMR
#define PRIORITY_QUEUE_H_I1PGXRMR

#include <stdlib.h>

#define PRIORITY_QUEUE_STRINGIFY(x) #x
#define PRIORITY_QUEUE_STR(x) PRIORITY_QUEUE_STRINGIFY(x)
#define PRIORITY_QUEUE_FILE_AND_LINE __FILE__ ":" PRIORITY_QUEUE_STR(__LINE__)

#define PRIORITY_QUEUE_RESIZED        (/* printf("Resizing priority queue at " PRIORITY_QUEUE_FILE_AND_LINE "\n") */ (void)0)
#define PRIORITY_QUEUE_OUT_OF_MEMORY  (fprintf(stderr, "Couldn't allocate memory for priority queue at " PRIORITY_QUEUE_FILE_AND_LINE "\n"), abort())
#define PRIORITY_QUEUE_INVALID_ACCESS (fprintf(stderr, "Attempted to access from empty priority queue at " PRIORITY_QUEUE_FILE_AND_LINE "\n"), 0)

#ifdef _MSC_VER
#define PRIORITY_QUEUE_FUNC static inline
#else
#define PRIORITY_QUEUE_FUNC __attribute__(( unused )) static inline
#endif

#define PRIORITY_QUEUE_TEMPLATE(NAME, VALUE, PRIORITY, SOONER) \
typedef struct { VALUE value; PRIORITY priority; } NAME##_data_t; \
typedef struct { \
    unsigned int size; \
    unsigned int count; \
    NAME##_data_t* data; \
} NAME; \
PRIORITY_QUEUE_FUNC \
void NAME##_init(NAME* pq, unsigned int size) { \
    pq->data = malloc(sizeof(NAME##_data_t)*size); \
    pq->size = size; \
    if (!pq->data) { \
        PRIORITY_QUEUE_OUT_OF_MEMORY; \
        pq->size = 0; \
    } \
    pq->count = 0; \
} \
PRIORITY_QUEUE_FUNC \
void NAME##_deinit(NAME* pq) { \
    free(pq->data); \
    pq->size = 0; \
    pq->count = 0; \
} \
PRIORITY_QUEUE_FUNC \
VALUE NAME##_peek(const NAME* pq) { \
    return pq->data[0].value; \
} \
PRIORITY_QUEUE_FUNC \
void NAME##_maybegrow(NAME* pq, unsigned int increment) { \
    if (pq->count + increment > pq->size) { \
        PRIORITY_QUEUE_RESIZED; \
        unsigned int size = pq->size*2 + increment + 1; \
        NAME##_data_t* d = realloc(pq->data, sizeof(NAME##_data_t)*size); \
        if (d) { \
            pq->data = d; \
            pq->size = size; \
        } else { \
            PRIORITY_QUEUE_OUT_OF_MEMORY; \
        } \
    } \
} \
PRIORITY_QUEUE_FUNC \
void NAME##_heapify(NAME* pq, unsigned int i) { \
    while (1) { \
        unsigned int l = 2*i + 1; \
        unsigned int r = l + 1; \
        unsigned int p = i; \
        if (l < pq->count && SOONER(pq->data[l].priority, pq->data[p].priority)) { \
            p = l; \
        } \
        if (r < pq->count && SOONER(pq->data[r].priority, pq->data[p].priority)) { \
            p = r; \
        } \
        if (p == i) { \
            break; \
        } else { \
            /* Swap smallest with i*/ \
            NAME##_data_t tmp = pq->data[i]; \
            pq->data[i] = pq->data[p]; \
            pq->data[p] = tmp; \
            /* Set i to smallest and repeat */ \
            i = p; \
        } \
    } \
} \
PRIORITY_QUEUE_FUNC \
void NAME##_push(NAME* pq, VALUE value, PRIORITY priority) { \
    NAME##_maybegrow(pq, 1); \
    pq->data[pq->count].value = value; \
    pq->data[pq->count].priority = priority; \
    pq->count ++; \
    if (pq->count > 1) { \
        for (long i=pq->count/2-1; i>=0; i--) { \
            NAME##_heapify(pq, i); \
        } \
    } \
} \
PRIORITY_QUEUE_FUNC \
VALUE NAME##_pop(NAME* pq) { \
    if (!pq->count) { \
        return PRIORITY_QUEUE_INVALID_ACCESS; \
    } \
    VALUE v = pq->data[0].value; \
    pq->count --; \
    pq->data[0] = pq->data[pq->count]; \
    if (pq->count > 1) { \
        NAME##_heapify(pq, 0); \
    } \
    return v; \
}
#endif /* end of include guard: PRIORITY_QUEUE_H_I1PGXRMR */
