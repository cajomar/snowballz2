#ifndef __SB2_MAP__
#define __SB2_MAP__

#include <stdbool.h>
#include <stdint.h>

#include "jobs.h"
#include "types.h"

#define PLAYER_NEUTRAL 0
#define PLAYER_SPAWNPOINT -1

#define TILE_SIZE 128
#define PENGUIN_SIZE 64

#define NO_PENGUIN -1

// Pixels per second
#define SNOWBALL_SPEED 1000.f
// Pixels
#define MAX_SNOWBALL_DISTANCE 1000.f

#define HEALTH_MAX_IGLOO 100
#define HEALTH_MAX_COLONY 1000000000
#define HEALTH_MAX_BARRACKS 300

#define HEALTH_MAX_WORKER_PENGUIN 20
#define HEALTH_MAX_WARRIOR_PENGUIN 30

enum building_status {
    BUILDING_STATUS_IDLE,
    BUILDING_STATUS_WORKING,
    BUILDING_STATUS_CONSTRUCTING,
};

// Building's level when it is placed for construction.
#define BUILDING_LEVEL_CONSTRUCTING 0

// Value for building_id's that indicate that there is no building. (Such as in
// MapViews.building_draw_order)
#define NO_BUILDING -1

typedef struct building {
    enum building_type type;
    int tile_x, tile_y;
    int owner_id;
    int health;
    int level;
    int progress;
    enum building_status status;
    int resources[RESOURCE_TYPE_END];

    bool changed;
} Building;

typedef struct penguin {
    enum penguin_type type;
    enum resource_type cargo;
    // Position of the penguin in map pixels
    // TODO Change to tile floats?
    int x, y;

    // Player id that owns this penguin.
    int owner_id;

    // Which colony building this penguin was spawned at.
    int colony_id;

    int health;
    Job job;

    // Set to true if the ai has made a change to the penguin this represents
    // that hasn't been sent to clients but needs to.
    // Value ignored on clients.
    bool changed;
} Penguin;

// Views into the map's state are used by various components when the primary
// storage isn't well suited. For example, drawing buildings in the right order
// is very obtuse when buildings are stored as a flat list. So we have a view
// that stores buildings by which tile they are in. This makes it very fast and
// easy for the renderer to know if a building should be rendered at a given
// tile.
typedef struct map_views {
    // Building ids stored at their tile location:
    //   map->buildings[building_draw_order[y*map->size + x]]
    int* building_draw_order;
} MapViews;

typedef struct snowball {
    // In pixles
    int x, y;
    int target_x, target_y;
    int total_time;
    uint32_t progress : 30;
    uint32_t used : 1;
    uint32_t changed : 1;
} Snowball;

typedef struct map {
    int32_t size;
    enum map_tile_type* tile_types;
    uint8_t* tile_resource_counts;
    char* name;
    char* author;

    // Building and penguin references are by index into the map's array. So
    // when a building or penguin is removed from the game, all valid buildings
    // and penguins need to remain at their current index. We mark 'freed'
    // buildings and penguins as END to indicate that that memory index can
    // be used for new penguins and buildings.
    Building* buildings;  // stretchy_buffer
    Penguin* penguins;    // stretchy_buffer
    Snowball* snowballs;  // stretchy_buffer

    // Views into the map state. These should not be modified by anyone outside
    // of map.c
    // Views are not saved/loaded and must be recalculated after directly
    // editing map state outside of map functions using
    // `map_recalculate_state_views()`.
    // TODO move into client only module.
    MapViews views;

    // Time the game has been going
    size_t time;
} Map;

void map_recalculate_state_views(Map* m);

Map* map_create(int size, char* name, char* author);
void map_destroy(Map* map);

int map_building_add(Map* map, enum building_type type, int tile_x, int tile_y);
void map_building_destroy(Map* map, int building_id);

bool map_can_place_building_at(const Map* map, enum building_type type,
                               int tile_x, int tile_y);
int map_get_building(const Map* map, int tile_x, int tile_y);

void map_pixel_to_tile(const Map* map, int pixel_x, int pixel_y, int* tile_x,
                       int* tile_y);

#endif
