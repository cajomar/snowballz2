#include "types.h"

#include <assert.h>
#include <stdbool.h>

enum resource_type map_tile_type_to_resource_type(enum map_tile_type t) {
    switch (t) {
        case MAP_TILE_TYPE_TREE:
            return RESOURCE_TYPE_TREE;
        case MAP_TILE_TYPE_EMPTY:
            return RESOURCE_TYPE_END;
    }
}

enum map_tile_type resource_type_to_map_tile_type(enum resource_type t) {
    switch (t) {
        case RESOURCE_TYPE_TREE:
            return MAP_TILE_TYPE_TREE;
        case RESOURCE_TYPE_END:
            return MAP_TILE_TYPE_EMPTY;
    }
}
