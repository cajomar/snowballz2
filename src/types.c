#include "types.h"
#include <assert.h>
#include <stdbool.h>

enum resource_type
map_tile_type_to_resource_type(enum map_tile_type t) {
    switch (t) {
        case MAP_TILE_TYPE_TREE:
            return RESOURCE_TYPE_TREE;
        default:
            assert(false);
    }
}
