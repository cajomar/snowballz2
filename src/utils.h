#ifndef __SB2_UTILS__
#define __SB2_UTILS__
#include <stdbool.h>

typedef struct rect {
    int l, t, r, b;
} Rect;

void rect_normalize_inplace(Rect* rect);
void rect_normalize(Rect* out, const Rect* in);
bool rect_contains(Rect*, int x, int y);
int distance_to_squared(int tx, int ty, int fx, int fy);

#define DISTANCE_TO_SQUARED(a, b)            \
    (((a)->x - (b)->x) * ((a)->x - (b)->x) + \
     ((a)->y - (b)->y) * ((a)->y - (b)->y))

#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#define MAX(a, b) (((a) > (b)) ? (a) : (b))

#endif
