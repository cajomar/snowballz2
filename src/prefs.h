#ifndef __SB2_PREFS__
#define __SB2_PREFS__

const char* prefs_data_dir();
int prefs_data_file(char* out, int max_out, const char* filename);
int prefs_map_file(char* out, int max_out, const char* map_name);
#endif
