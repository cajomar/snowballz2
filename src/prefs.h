#ifndef __SB2_PREFS__
#define __SB2_PREFS__

const char* prefs_data_dir(void);
int prefs_data_file(char* out, int max_out, const char* filename);
int prefs_map_file(char* out, int max_out, const char* map_name);
int prefs_asset_file(char* out, int max_out, const char* asset_name);
const char* prefs_map_dir(void);
const char* prefs_save_dir(void);

#endif
