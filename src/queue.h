/*
 * My (Caleb Marshall) single-file header-only library for first-in-first-out
 * queues. It works similar to the stb stretchy_buffer.h or stb_ds.h
 *
 * You can find a (possibly more recent) copy of this file at
 * https://bitbucket.org/cajomar/queue/raw/HEAD/queue.h
 *
 * To create a queue initalize it to NULL:
 * int* queue = NULL; // queue
 *
 * Then free it when you're done:
 * queue_free(queue);
 *
 * When more items are queued than the buffer can hold, the queue will be
 * resized automatically. If you wish to be warned about this you can edit
 * the value of the QUEUE_RESIZED macro. It's defined just below the include
 * guard.
 *
 * The queue uses malloc to expand its buffer. If malloc fails, the
 * QUEUE_OUT_OF_MEMORY macro will be expanded and the queue won't be grown.
 * By default it prints an error message and aborts. You can edit the macro to
 * whatever you want, but it must return the same type as QUEUE_RESIZED or it
 * won't compile.
 *
 * If you request a value from an empty or non-existant queue the
 * QUEUE_INVALID_ACCESS macro will be expanded. By default it prints an error
 * message and returns 0. Edit it to whatever you want.
 *
 * The queue prefixes everything with `queue_`. If this conflicts with your
 * code or you want to change it for some other reason, you can use sed:
 * $ sed 's/\bqueue_/myq/g; s/\bQUEUE_/MYQ_/g' -i queue.h
 * Now you can use myqpush() instead of queue_push() et cetera.
 */

#ifndef QUEUE_H_G5KU82DH
#define QUEUE_H_G5KU82DH

#define QUEUE_STRINGIFY(x) #x
#define QUEUE_STR(x) QUEUE_STRINGIFY(x)

#define QUEUE_FILE_AND_LINE __FILE__ ":" QUEUE_STR(__LINE__)

#define QUEUE_RESIZED        (/* printf("Resizing queue at " QUEUE_FILE_AND_LINE "\n") */ (void)0)
#define QUEUE_OUT_OF_MEMORY  (fprintf(stderr, "Couldn't allocate memory for queue at " QUEUE_FILE_AND_LINE "\n"), abort())
#define QUEUE_INVALID_ACCESS (fprintf(stderr, "Attempted to access from empty queue at " QUEUE_FILE_AND_LINE "\n"), 0)


#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef long int queueint;

typedef struct {
    queueint capacity;
    queueint count;
    queueint front;
} queue__struct;

/* Entire data block */
#define queue_raw(a) (((queue__struct*) (a)) - 1)
#define queue_raw_const(a) ((const queue__struct* const) (a) - 1)

/* Reallocate queue with room for `n` more elements */
#define queue__grow(a,n) queue__growf((void**)&(a), (n), sizeof(*(a))) ? (QUEUE_OUT_OF_MEMORY) : (QUEUE_RESIZED)
/* False if queue buffer can hold `n` more elements */
#define queue__needgrow(a,n) (!(a) || queue_count(a)+(n) > queue_raw(a)->capacity)
/* Ensure that queue can hole `n` more elements, reallocating if necessary */
#define queue__maybegrow(a,n) (queue__needgrow((a),(n)) ? queue__grow((a),(n)) : (void)0)
/* Wrap `i` so that 0 < i < j
 * Will cause division-by-zero error if j == 0 */
#define queue__normalize_index(i,j) (((i)%(j)+(j))%(j))

/* Execute expression if a->capacity > 0 */
#define queue__if_buffer(a, expr) ((a) && queue_raw(a)->capacity ? (expr) : 0)
/* a->count ? expr : QUEUE_INVALID_ACCESS */
#define queue__if_items_return(a, expr) (queue_count(a) ? (expr) : (QUEUE_INVALID_ACCESS))

/* Get the number of queued elements. Return 0 if queue == NULL */
#define queue_count(a) ((a) ? queue_raw(a)->count : 0)
/* True if queue_count(queue) == 0 */
#define queue_empty(a) (!queue_count(a))

/* Append `v` to the queue, reallocating if necessary */
#define queue_push(a,v) (queue__maybegrow((a), 1), \
                        (a)[queue_wrap((a), queue_raw(a)->front+(queue_raw(a)->count++))] = (v))

/* Enqueue `n` more items, and return a pointer to the first one */
#define queue_append(a,n) (queue__maybegrow((a), (n)), \
                           (queue_raw(a)->capacity - queue_raw(a)->front - queue_raw(a)->count < (n) ? queue_normalize(a) : 0, \
                           queue_raw(a)->count+=(n), \
                           queue_getp((a), -(n)))

/* Return the next item and remove it from queue. */
#define queue_pop(a) queue__if_items_return((a), ( \
            queue_raw(a)->count--, \
            (queue_raw(a)->front < queue_raw(a)->capacity-1 ? (a)[queue_raw(a)->front++] : \
                (queue_raw(a)->front = 0, (a)[queue_raw(a)->capacity-1]))))

/* Return the last enqueued item and remove it from queue */
#define queue_poplast(a) queue__if_items_return((a), \
        (a)[queue_wrap((a), queue_raw(a)->front+(--queue_raw(a)->count))])

/* Return what value will be popped next. */
#define queue_peek(a) queue__if_items_return((a), (a)[queue_raw(a)->front])
/* Return the last enqueued item. */
#define queue_last(a) queue__if_items_return((a), queue_getv((a), -1))

/* Free a queue */
#define queue_free(a) ((a) ? free(queue_raw(a)), (a)=0 : 0);
/* Remove all items from the queue without freeing it */
#define queue_clear(a) (queue_count(a) ? queue_raw(a)->front = queue_raw(a)->count = 0 : 0)

/* Ensure queue can hold `n` elements, resizing the buffer if it can't */
#define queue_resize(a,n) queue__maybegrow((a), (n) - queue_count(a))
/* Reorder queue so that the first element is at a[0] */
#define queue_normalize(a) (queue_count(a) ? queue__normalize((a),(a),sizeof(*(a))) : 0)



/* Be careful with these functions. They assume that a (the queue) exists.
 * If a.count == 0 they will most likely cause a
 * division-by-zero error. */

/* Wrap `i` to queue so that a[i] won't cause a buffer overflow.
 * Do not use if a == NULL */
#define queue_wrap(a, i) queue__normalize_index((i), queue_raw(a)->capacity)
/* Get the index into the queue buffer of the `i`th queued element, where 0 is
 * the oldest, 1 is the second oldest, and -1 is the newest.
 * Do not use if queue is empty! It will cause a division-by-zero error. */
#define queue_index(a,i) queue__normalize_index( \
                /* Normalize i to number of queued elements (if -1 is passed) */ \
                queue__normalize_index((i), queue_count(a)) + queue_raw(a)->front, \
                queue_raw(a)->capacity)
/* Return a pointer to the `i`th queued element. Will be valid until
 * queue_push(), queue_free(), queue_normalize(), queue_resize(),
 * or queue_reverse().
 * Do not use if queue is empty! */
#define queue_getp(a,i) ((a)+queue_index((a),(i)))
/* Return the value of the `i`th queued element.
 * Do not use if queue is empty!  */
#define queue_getv(a,i) (*queue_getp((a),(i)))



#define queue_reverse(a) do { \
    queueint queue__counter; \
    for (queue__counter=0; queue__counter<queue_count(a)/2; queue__counter++) { \
        __typeof__(*(a)) tmp = queue_getv((a), queue__counter); \
        queue_getv((a), queue__counter) = queue_getv((a), queue_count(a)-1-queue__counter); \
        queue_getv((a), queue_count(a)-1-queue__counter) = tmp; \
    } \
} while (0)

/*
 * Move the queue from src into dest, with the first queue at index 0.
 * dest and src must either be the same or not overlap at all.
 * dest->capacity must be initalized and will not be overwritten.
 * dest.count will == src.count and dest.front will == 0
 * src must be a valid queue.
 */
#ifndef _MSC_VER
__attribute__ ((unused))
#endif
static void queue__normalize(void* dest, const void* src, size_t itemsize) {
    const queue__struct* srcq = queue_raw_const(src);
    const void* srcf = (const char*)src + srcq->front*itemsize;
    /* Number of bytes from the first element to the end of the buffer. */
    const size_t lena = (srcq->capacity - srcq->front) * itemsize;
    /* Number of bytes from the start of the buffer to the end of the queue. */
    const size_t lenb = (srcq->front + srcq->count - srcq->capacity) * itemsize;
    if (srcq->count == 0 || (srcq->front == 0 && src == dest)) {
        /* Queue is empty or it's already normalized and they're the same,
         * don't do anything */
    } else if (srcq->front + srcq->count < srcq->capacity) {
        /* The queue is not wrapped */
        memmove(dest, srcf, srcq->count*itemsize);
    } else if (dest == src) {
        /* The queue is wrapped and overlaps. */
        /* Save the wrapped part */
        void* tmp = malloc(lenb);
        memcpy(tmp, src, lenb);
        /* Slide the front to 0 */
        memmove(dest, srcf, lena);
        memcpy((char*)dest + lena, tmp, lenb);
        free(tmp);
    } else {
        /* The queue is wrapped but doesn't overlap. */
        memcpy(dest, srcf, lena);
        memcpy((char*)dest + lena, src, lenb);
    }
    queue_raw(dest)->front = 0;
    queue_raw(dest)->count = srcq->count;
}

/* Grow the size of *arr by increments*itemsize bytes.
 * Does not change queue_raw(a)->count
 * Return 0 on success, 1 on failure.
 */
#ifndef _MSC_VER
__attribute__ ((unused))
#endif
static int queue__growf(void** a, queueint increment, size_t itemsize) {
    queueint m = *a ? queue_raw(*a)->capacity*2 + increment : increment + 1;
    queue__struct* buf = malloc(sizeof(queue__struct) + itemsize*m);
    if (buf) {
        buf->capacity = m;
        if (*a) {
            queue__normalize(buf+1, *a, itemsize);
            queue_free(*a);
        } else {
            buf->count = 0;
            buf->front = 0;
        }
        *a = buf+1;
        return 0;
    } else {
        return 1;
    }
}
#endif /* end of include guard: QUEUE_H_G5KU82DH */
