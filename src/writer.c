#include "writer.h"

#include "stretchy_buffer.h"

void w_i32(struct i_writer* w, int32_t v) {
    union {
        uint8_t bytes[4];
        int32_t i;
    } fun = {.i = v};
    memcpy(sbadd(w->data, 4), fun.bytes, 4);
}

void w_i16(struct i_writer* w, int16_t v) {
    union {
        uint8_t bytes[2];
        int16_t i;
    } fun = {.i = v};
    memcpy(sbadd(w->data, 2), fun.bytes, 2);
}

void w_u8(struct i_writer* w, uint8_t v) { sbpush(w->data, v); }

void w_str(struct i_writer* w, const char* v) {
    int len = strlen(v);
    w_i32(w, len);
    memcpy(sbadd(w->data, len), v, len);
}
