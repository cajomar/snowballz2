#ifndef __SB2_MAP_SAVER__
#define __SB2_MAP_SAVER__

#include "map.h"

uint8_t* map_saver_serialize(Map* m);
void map_saver_building(struct i_writer* w, const Building* b);
void map_saver_penguin(struct i_writer* w, const Penguin* p);
void map_saver_snowball(struct i_writer* w, const Snowball* s);
void save_map(Map* map, const char* path);

#endif
