#ifndef __SB2_RULES__
#define __SB2_RULES__
#include "types.h"

#define RULE_unload_time 1000

// How long it takes for a single resource to be added to a buildings
// construction.
static const int RULE_construct_building_time[BUILDING_TYPE_END] = {
    3000,  // BUILDING_TYPE_IGLOO
    0,     // BUILDING_TYPE_COLONY
    3000,  // BUILDING_TYPE_IGLOO
};

static const int RULE_construct_building_cost[BUILDING_TYPE_END]
                                             [RESOURCE_TYPE_END] = {
                                                 // BUILDING_TYPE_IGLOO
                                                 {
                                                     5,  // RESOURCE_TYPE_TREE
                                                 },

                                                 // BUILDING_TYPE_COLONY
                                                 {
                                                     0,  // RESOURCE_TYPE_TREE
                                                 },

                                                 // BUILDING_TYPE_BARRACKS
                                                 {
                                                     5,  // RESOURCE_TYPE_TREE
                                                 },
};

#endif
