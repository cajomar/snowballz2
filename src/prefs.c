#include "prefs.h"

#include "cfgpath.h"
#define CUTE_FILES_IMPLEMENTATION
#include <stdio.h>

#include "cute_files.h"

#ifndef SB2_ASSET_DIR
#define SB2_ASSET_DIR "data/"
#endif

const char* prefs_data_dir() {
    static char* data_dir = 0;
    if (data_dir == 0) {
        char out[MAX_PATH];
        get_user_data_folder(out, MAX_PATH, "snowballz2");
        if (out[0] == 0) {
            fprintf(stderr, "Failed to find user data directory.\n");
            return 0;
        } else {
            data_dir = malloc(strlen(out) + 1);
            strcpy(data_dir, out);
        }
    }
    return data_dir;
}

static const char* prefs_data_subdir(const char* subdir) {
    const char* data_dir = prefs_data_dir();
    char* path = malloc(strlen(data_dir) + strlen(subdir) + 1);
    char* pos = stpcpy(path, data_dir);
    strcat(pos, subdir);
    if (cf_file_exists(path) == 0) {
        printf("Creating %s\n", path);
        mkdir(path, 0755);
    }
    return path;
}

const char* prefs_map_dir() {
    static const char* dir = 0;
    if (dir == 0) {
        dir = prefs_data_subdir("maps/");
    }
    return dir;
}

const char* prefs_save_dir() {
    static const char* dir = 0;
    if (dir == 0) {
        dir = prefs_data_subdir("saves/");
    }
    return dir;
}

int prefs_data_file(char* out, int max_out, const char* filename) {
    memset(out, 0, max_out);
    const char* dir = prefs_data_dir();
    if (strlen(dir) + strlen(filename) + 1 > max_out) return -1;
    char* pos = stpcpy(out, dir);
    strcat(pos, filename);
    return 0;
}

int prefs_map_file(char* out, int max_out, const char* map_name) {
    memset(out, 0, max_out);
    const char* dir = prefs_map_dir();
    if (strlen(dir) + strlen(map_name) + 5 > max_out) return -1;
    char* pos = stpcpy(out, dir);
    for (int i = 0; i < strlen(map_name); i++) {
        char c = map_name[i];
        if ((c < 'A' || c > 'z') && !(c >= '0' && c <= '9')) {
            c = '_';
        }
        *(pos++) = c;
    }
    strcpy(pos, ".sb2");
    return 0;
}

int prefs_asset_file(char* out, int max_out, const char* asset_name) {
    memset(out, 0, max_out);
    if (strlen(SB2_ASSET_DIR) + strlen(asset_name) + 1 > max_out) return -1;
    char* pos = stpcpy(out, SB2_ASSET_DIR);
    strcpy(pos, asset_name);
    return 0;
}
