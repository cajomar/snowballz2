#include "prefs.h"
#include "cfgpath.h"
#include <stdio.h>
#define CUTE_FILES_IMPLEMENTATION
#include "cute_files.h"

const char* prefs_data_dir() {
    static char* data_dir = 0;
    if (data_dir == 0) {
        char out[MAX_PATH];
        get_user_data_folder(out, MAX_PATH, "snowballz2");
        if (out[0] == 0) {
            fprintf(stderr, "Failed to find user data directory.\n");
            return 0;
        } else {
            data_dir = malloc(strlen(out) + 1);
            strcpy(data_dir, out);
        }
    }
    return data_dir;
}

int prefs_data_file(char* out, int max_out, const char* filename) {
    memset(out, 0, max_out);
    const char* dir = prefs_data_dir();
    if (strlen(dir) + strlen(filename) + 1 > max_out) return -1;
    strcpy(out, dir);
    strcat(out, filename);
    return 0;
}

// Like prefs_data_file(), but directly takes the map's name and appends the
// .sb2 suffix.
int prefs_map_file(char* out, int max_out, const char* map_name) {
    memset(out, 0, max_out);
    const char* dir = prefs_data_dir();
    if (strlen(dir) + strlen(map_name) + 5 > max_out) return -1;
    strcpy(out, dir);
    for (int i=0; i<strlen(map_name); i++) {
        char c = map_name[i];
        if ((c < 'A' || c > 'z') && !(c >= '0' && c <= '9')) {
            c = '_';
        }
        out[strlen(dir) + i] = c;
    }
    strcat(out, ".sb2");
    return 0;
}
