#include "map.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "stretchy_buffer.h"
#include "utils.h"

const int building_sizes[BUILDING_TYPE_END] = {
    1,  // BUILDING_TYPE_IGLOO
    2,  // BUILDING_TYPE_COLONY
    1,  // BUILDING_TYPE_BARRACKS
};

const int MAX_BUILDING_SIZE = 2;

// Allocates a map.
// Takes ownership of `name` and `author`
Map* map_create(int size, char* name, char* author) {
#define ALLOC(OUT, COUNT) \
    if (((OUT) = calloc((COUNT), sizeof(*(OUT)))) == NULL) goto ERROR;
    Map* map = 0;

    ALLOC(map, 1);

    if (size < 1) goto ERROR;
    map->size = size;

    ALLOC(map->tile_types, map->size * map->size);
    ALLOC(map->views.building_draw_order, map->size * map->size);
    ALLOC(map->tile_resource_counts, map->size * map->size);

    map->name = name;
    map->author = author;

    map_recalculate_state_views(map);

    return map;

ERROR:
    fprintf(stderr, "Fatal error\n");
    // These free's are safe if map is zeroed
    free(map->tile_types);
    free(map->views.building_draw_order);
    free(map->tile_resource_counts);
    free(map);
    exit(-1);
    return NULL;
#undef ALLOC
}

void map_destroy(Map* map) {
    free(map->name);
    free(map->author);
    free(map->tile_types);
    free(map->tile_resource_counts);
    free(map->views.building_draw_order);
    sbfree(map->buildings);
    sbfree(map->penguins);
    free(map);
}

// TODO Move into server-only module.
int map_building_add(Map* map, enum building_type type, int tile_x,
                     int tile_y) {
    Building* out = 0;
    int building_id;
    sbforeachp (Building* b, map->buildings) {
        if (b->type == BUILDING_TYPE_END) {
            out = b;
            building_id = stb__counter;
            break;
        }
    }

    if (out == 0) {
        building_id = sbcount(map->buildings);
        out = sbadd(map->buildings, 1);
    }

    memset(out, 0, sizeof(*out));
    out->type = type;
    switch (type) {
        case BUILDING_TYPE_IGLOO:
            out->health = HEALTH_MAX_IGLOO;
            break;
        case BUILDING_TYPE_COLONY:
            out->health = HEALTH_MAX_COLONY;
            break;
        case BUILDING_TYPE_BARRACKS:
            out->health = HEALTH_MAX_BARRACKS;
            break;
        default:
            assert(false);
            break;
    }
    out->tile_x = tile_x;
    out->tile_y = tile_y;
    out->level = 1;
    out->status = BUILDING_STATUS_IDLE;
    out->changed = true;
    map->views.building_draw_order[out->tile_y * map->size + out->tile_x] =
        building_id;
    return building_id;
}

void map_building_destroy(Map* map, int building_id) {
    Building* b = &map->buildings[building_id];
    b->type = BUILDING_TYPE_END;
    map->views.building_draw_order[b->tile_y * map->size + b->tile_x] =
        NO_BUILDING;
}

bool map_can_place_building_at(const Map* map, enum building_type type,
                               int tile_x, int tile_y) {
    const int size = building_sizes[type];

    for (int y = MAX(tile_y, 0); y < MIN(tile_y + size, map->size); y++) {
        for (int x = MAX(tile_x, 0); x < MIN(tile_x + size, map->size); x++) {
            if (map->tile_types[y * map->size + x] != MAP_TILE_TYPE_EMPTY) {
                return false;
            }
        }
    }

    sbforeachp (Building* b, map->buildings) {
        if (b->type == BUILDING_TYPE_END) continue;
        const int size_b = building_sizes[b->type];
        if (!(tile_x + size - 1 < b->tile_x || tile_y + size - 1 < b->tile_y ||
              tile_x >= b->tile_x + size_b || tile_y >= b->tile_y + size_b)) {
            return false;
        }
    }
    return true;
}

int map_get_building(const Map* map, int tile_x, int tile_y) {
    sbforeachp (Building* b, map->buildings) {
        if (b->type == BUILDING_TYPE_END) continue;
        const int size = building_sizes[b->type];
        if (tile_x < b->tile_x || tile_y < b->tile_y ||
            tile_x >= b->tile_x + size || tile_y >= b->tile_y + size) {
            continue;
        }
        return stb__counter;
    }
    return -1;
}

void map_recalculate_state_views(Map* m) {
    memset(m->views.building_draw_order, NO_BUILDING,
           m->size * m->size * sizeof(*m->views.building_draw_order));
    sbforeachp (Building* b, m->buildings) {
        if (b->type == BUILDING_TYPE_END) continue;
        m->views.building_draw_order[b->tile_y * m->size + b->tile_x] =
            stb__counter;
    }
}

void map_pixel_to_tile(const Map* map, int pixel_x, int pixel_y, int* tile_x,
                       int* tile_y) {
    int x = pixel_x;
    int y = pixel_y;
    x /= TILE_SIZE;
    y /= TILE_SIZE;
    if (x < 0) x = 0;
    if (y < 0) y = 0;
    if (x > map->size - 1) x = map->size - 1;
    if (y > map->size - 1) y = map->size - 1;
    *tile_x = x;
    *tile_y = y;
}
