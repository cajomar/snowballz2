#ifndef __SB2_TYPES__
#define __SB2_TYPES__

enum map_tile_type {
    MAP_TILE_TYPE_EMPTY = 0,
    MAP_TILE_TYPE_TREE,
};

enum building_type {
    BUILDING_TYPE_IGLOO = 0,
    BUILDING_TYPE_COLONY,
    BUILDING_TYPE_BARRACKS,
    BUILDING_TYPE_END
};

enum penguin_type {
    PENGUIN_TYPE_WORKER = 0,
    PENGUIN_TYPE_WARRIOR,
    PENGUIN_TYPE_END
};

// WARNING: Changing this enum changes the map binary format.
enum resource_type { RESOURCE_TYPE_TREE = 0, RESOURCE_TYPE_END };

enum resource_type map_tile_type_to_resource_type(enum map_tile_type);

enum map_tile_type resource_type_to_map_tile_type(enum resource_type t);

#endif
