#include <assert.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h> // for access()
#include <time.h>

#include <enet/enet.h>

#include "../host/game_host.h"
#include "../map.h"
#include "../prefs.h"
#include "../queue.h"

#define MAXIN 512

static void new_game(const char* filename, bool* running) {
    if (running && *running) {
        game_host_shutdown();
        *running = false;
    }
    // If filename doesn't exist, treat it as a map name.
    if (access(filename, R_OK) != 0) {
        char new_filename[2048];
        prefs_map_file(new_filename, 2048, filename);
        game_host(new_filename);
        if (running) *running = true;
    } else {
        game_host(filename);
        if (running) *running = true;
    }
}

int main(int argc, const char** argv) {
    if (enet_initialize() != 0) {
        fprintf(stderr, "ENet initialization failed.\n");
        return -1;
    }

    bool game_running = false;

    for (int i=1; i<argc-1; i++) {
        if (strcmp(argv[i], "-h") == 0) {
            new_game(argv[++i], &game_running);
        }
    }

    bool server_running = true;
    while (server_running) {
        // TODO get precise delta time
        float dt = 10.f / 1000.f;
        game_host_step(dt);
    }

    if (game_running) {
        game_host_shutdown();
    }
    enet_deinitialize();
    return 0;
}
