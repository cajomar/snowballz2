#include <assert.h>
#include <enet/enet.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "../host/game_host.h"
#include "../prefs.h"

#define MAXIN 512

static void new_game(const char* filename, bool* running) {
    if (*running) {
        game_host_shutdown();
        *running = false;
    }

    // If the filename passed as an arg doesn't exist, treat it as a map
    // name instead of a file name.
    if (game_host(filename, true)) {
        *running = true;
        return;
    }

    char new_host_map[2048];
    prefs_map_file(new_host_map, 2048, filename);
    if (game_host(new_host_map, true)) {
        *running = true;
        return;
    }
}

int main(int argc, const char** argv) {
    if (enet_initialize() != 0) {
        fprintf(stderr, "ENet initialization failed.\n");
        return -1;
    }

    bool game_running = false;

    for (int i = 1; i < argc - 1; i++) {
        if (strcmp(argv[i], "-h") == 0) {
            new_game(argv[++i], &game_running);
        }
    }

    struct timespec last_frame;
    clock_gettime(CLOCK_REALTIME, &last_frame);

    while (game_running) {
        struct timespec now;
        clock_gettime(CLOCK_REALTIME, &now);

        struct timespec elapsed = {
            .tv_sec = now.tv_sec - last_frame.tv_sec,
            .tv_nsec = now.tv_nsec - last_frame.tv_nsec,
        };

        const uint64_t min_elapsed =
            (1000 * 1000) * 10;  // 10 milliseconds, measured in nanoseconds

        if (elapsed.tv_sec == 0 && elapsed.tv_nsec < min_elapsed) {
            elapsed.tv_nsec = min_elapsed - elapsed.tv_nsec;
            nanosleep(&elapsed, NULL);
            continue;
        }
        double dt =
            elapsed.tv_sec + elapsed.tv_nsec / (double)(1000 * 1000 * 1000);
        last_frame = now;

        game_host_step(dt);
    }

    if (game_running) {
        game_host_shutdown();
    }
    enet_deinitialize();
    return 0;
}
