#include "lobby.h"

#include "../host/game_host.h"
#include "../map.h"
#include "../map_loader.h"
#include "game.h"
#include "game_client.h"
#include "gui.h"
#include "idents.h"

#include <ink_data.h>
#include <ink_experimental.h>

extern struct ink* ink;
static inkd_listener* listener = 0;

void lobby_init() {
    if (listener) return;
    listener = inkd_listener_new(0);
    inkd source = ink_source(ink, I_lobby);
    inkd_add_listener(source, listener);
    load_map_list(source);
}

void lobby_shutdown() {
    if (!listener) return;
    inkd_clear_all(ink_source(ink, I_lobby));
    inkd_listener_delete(listener);
    listener = 0;
}

static void start_game(struct ink_data_event* ev) {
    inkd source = ink_source(ink, I_lobby);
    inkd data = ink_data_event_get_meta_hold_EXP(ev, I_lobby);
    const char* path = inkd_get_charp(data, I_path);

    game_start(path, NULL, NULL, source);

    /* game_init();
    game_host(path);
    // TODO player name should be saved/loaded from a preferences file.
    game_client("localhost", "Joey");
    inkd_release(data);
    inkd_trigger(source, I_launch); */
}

void lobby_step() {
    if (!listener) return;
    struct ink_data_event ev;
    while (inkd_listener_poll(listener, &ev)) {
        switch (ev.type) {
        case INK_DATA_EVENT_IMPULSE:
            switch (ev.field) {
            case I_start_game:
                start_game(&ev);
                break;
            }
            break;
        default:
            break;
        }
    }
}
