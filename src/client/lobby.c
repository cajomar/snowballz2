#include "lobby.h"

#include <ink_data.h>
#include <stdio.h>

#include "gui.h"
#include "idents.h"

extern struct ink* ink;

void lobby_init(const char* map_dir) {
    inkd source = ink_source(ink, I_lobby);
    load_map_list(map_dir, source);
    puts("Lobby init");
}

void lobby_shutdown(void) {
    inkd_clear_all(ink_source(ink, I_lobby));
    puts("Lobby shutdown");
}

void lobby_step(void) {}
