#ifndef __SB2_GAME_SELECTION__
#define __SB2_GAME_SELECTION__

#include <SDL2/SDL_events.h>
#include <stdbool.h>

#include "../map.h"

void game_selection_init(void);
void game_selection_shutdown(void);
bool game_selection_event(const Map* map, SDL_Event* ev);
void game_selection_step(const Map* map);

#endif
