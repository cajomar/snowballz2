#include "game.h"

#include <assert.h>
#include <ink_data.h>
#include <ink_query.h>
#include <threads.h>

#include "../host/game_host.h"
#include "../map_saver.h"
#include "../prefs.h"
#include "../stretchy_buffer.h"
#include "../writer.h"
#include "command.h"
#include "game_client.h"
#include "idents.h"

extern struct ink* ink;
extern int viewport[4];
extern const int building_sizes[BUILDING_TYPE_END];

static mtx_t msg_mtx;
static bool host_should_quit;
thrd_t host_thread_handle;

static inkd_listener* listener = 0;

static bool check_for_message(void) {
    mtx_lock(&msg_mtx);
    bool m = host_should_quit;
    mtx_unlock(&msg_mtx);
    return m;
}

static void send_message(void) {
    mtx_lock(&msg_mtx);
    host_should_quit = true;
    mtx_unlock(&msg_mtx);
}

static int host_thread(void* arg) {
    (void)arg;
    uint32_t frame_start = SDL_GetTicks();

    while (true) {
        uint32_t now = SDL_GetTicks();
        uint32_t dt = now - frame_start;
        if (dt < 10) {
            SDL_Delay(10 - dt);
            continue;
        }
        frame_start = now;

        if (check_for_message()) break;

        game_host_step(dt * 0.001f);
    }

    game_host_shutdown();
    return 0;
}

/*
 * Starts the server thread and initalizes the client
 * If `host_map` is NULL then no server will be created.
 * If `host_ip` is NULL then it will default to `127.0.0.1`
 * If `client_name` is NULL then it will try to find the user's name by
 * reading environment variables.
 * `launch_from` is the ink data with a `launch` trigger to start the game
 * Usually, either `host_map` or `host_ip` should be NULL
 */
bool game_start(const char* host_map, const char* host_ip,
                const char* client_name, bool allow_client_connections) {
    if (!listener) {
        listener = inkd_listener_new(0);
        inkd source = ink_source(ink, I_game);
        inkd_add_listener(source, listener);
    }

    mtx_init(&msg_mtx, mtx_plain);
    if (host_map) {
        host_should_quit = false;
        if (game_host(host_map, allow_client_connections)) {
            thrd_create(&host_thread_handle, host_thread, NULL);
        } else {
            // Failed to create the host
            game_end();
            return false;
        }
    }

    if (!host_ip) host_ip = "127.0.0.1";
    if (!client_name) client_name = getenv("USER");
    if (!client_name) client_name = getenv("USERNAME");
    if (!client_name) client_name = "player";
    game_client(host_ip, client_name);
    return true;
}

void game_end(void) {
    if (!listener) return;
    inkd_clear_all(ink_source(ink, I_game));
    inkd_listener_delete(listener);
    listener = 0;

    inkd_bool(ink_source(ink, I_main), I_game, false);
    inkd_bool(ink_source(ink, I_main), I_game_pending, false);

    send_message();
    // TODO is this necessary?
    /* int res;
    thrd_join(host_thread_handle, &res); */

    game_client_shutdown();
}

void game_step(float dt) {
    if (!listener) return;
    inkd source = ink_source(ink, I_game);
    struct ink_data_event ev;
    while (inkd_listener_poll(listener, &ev)) {
        switch (ev.type) {
            case INK_DATA_EVENT_IMPULSE:
                switch (ev.field) {
                    case I_end:
                        game_end();
                        return;
                    case I_start:
                        game_client_start();
                        return;
                    case I_save: {
                        Map* map = game_client_get_map();
                        assert(map);
                        save_map(map, prefs_save_dir());
                        break;
                    }
                    case I_recruit_penguin: {
                        struct i_writer w = {.data = 0};
                        w_u8(&w, CMD_recruit_penguin);
                        w_i32(&w, inkd_get_int(ev.data, I_id));
                        game_client_command(w);
                        sbfree(w.data);
                        break;
                    }
                }
                break;
            case INK_DATA_EVENT_CHANGE:
                switch (ev.field) {
                    case I_build_mode:
                        if (inkd_get_bool(source, I_build_mode)) {
                            game_command_mode(COMMAND_MODE_BUILD);
                        } else {
                            game_command_mode(COMMAND_MODE_DEFAULT);
                        }
                        break;
                }
                break;
            default:
                break;
        }
    }
    game_client_step(dt);
}
