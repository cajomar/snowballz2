#include "game_client.h"

#include <assert.h>
#include <enet/enet.h>
#include <ink_query.h>

#include "../host/game_host.h"
#include "../map_loader.h"
#include "../mat.h"
#include "../reader.h"
#include "../stretchy_buffer.h"
#include "../utils.h"
#include "../writer.h"
#include "command.h"
#include "idents.h"
#include "minimap.h"
#include "render.h"
#include "render_debug.h"
#include "selection.h"

const uint32_t PLAYER_COLORS[4] = {0xddddddff, 0xff0000ff, 0x00ff00ff,
                                   0x0000ffff};

extern struct ink* ink;
extern int viewport[4];
extern const int building_sizes[BUILDING_TYPE_END];

ClientPenguin* client_penguins = 0;  // stretchy_buffer
int local_player_id;
int winner_player_id;

static ENetHost* client = 0;
static ENetPeer* peer = 0;
static Map* client_map = 0;
static char* player_name = 0;

Map* game_client_get_map(void) { return client_map; }

static void send_client_info(void) {
    ENetPacket* packet = enet_packet_create(NULL, 1 + strlen(player_name) + 1,
                                            ENET_PACKET_FLAG_RELIABLE);
    packet->data[0] = MSG_player_name;
    strcpy((char*)packet->data + 1, player_name);
    enet_peer_send(peer, 0, packet);
}

void game_client(const char* host, const char* name) {
    assert(client == 0);
    player_name = strdup(name);
    game_selection_init();
    game_minimap_init();
    local_player_id = 0;
    winner_player_id = 0;

    client = enet_host_create(NULL, 1, 2, 0, 0);
    if (client == 0) {
        fprintf(stderr, "Error while trying to create an ENet client.\n");
        exit(EXIT_FAILURE);
    }

    ENetAddress address;
    enet_address_set_host(&address, host);
    address.port = NET_PORT;
    peer = enet_host_connect(client, &address, 2, 0);
    if (peer == 0) {
        fprintf(stderr, "Failed to create ENet peer.\n");
        exit(EXIT_FAILURE);
    }
}

static void handle_map_scroll(float dt);

static void load_map_from_network(const char* data, int count) {
    bool first_time = !client_map;
    if (client_map) map_destroy(client_map);
    uint8_t* r = (uint8_t*)data;
    client_map = map_loader_deserialize(&r);
    assert(r == (uint8_t*)data + count);

    if (client_map == 0) {
        fprintf(stderr, "Bad message\n");
        return;
    }

    while (sbcount(client_penguins) < sbcount(client_map->penguins)) {
        ClientPenguin* c = sbadd(client_penguins, 1);
        memset(c, 0, sizeof(*c));
    }
    sbforeachp (ClientPenguin* c, client_penguins) {
        Penguin* p = &client_map->penguins[stb__counter];
        c->lerp_x = p->x;
        c->lerp_y = p->y;
    }

    if (first_time) {
        sbforeachp (const Building* b, client_map->buildings) {
            if (b->type == BUILDING_TYPE_COLONY &&
                b->owner_id == local_player_id) {
                viewport[0] =
                    (b->tile_x + building_sizes[b->type] / 2) * TILE_SIZE -
                    viewport[2] / 2;
                viewport[1] =
                    (b->tile_y + building_sizes[b->type] / 2) * TILE_SIZE -
                    viewport[3] / 2;
                break;
            }
        }
    }
}

static void handle_map_change(uint8_t** r) {
    enum map_change_type t = r_u8(r);
    if (t == MAP_building) {
        int building_id = r_i32(r);
        while (building_id >= sbcount(client_map->buildings)) {
            sbadd(client_map->buildings, 1)->type = BUILDING_TYPE_END;
        }
        Building* b = &client_map->buildings[building_id];
        map_loader_building(b, r);
        client_map->views
            .building_draw_order[b->tile_y * client_map->size + b->tile_x] =
            building_id;
    } else if (t == MAP_penguin) {
        int penguin_id = r_i32(r);
        while (penguin_id >= sbcount(client_map->penguins)) {
            sbadd(client_map->penguins, 1)->type = PENGUIN_TYPE_END;
            ClientPenguin* c = sbadd(client_penguins, 1);
            memset(c, 0, sizeof(*c));
        }
        Penguin* p = &client_map->penguins[penguin_id];
        map_loader_penguin(p, r);
        job_load(r, &p->job);
    } else if (t == MAP_tile) {
        int tile_idx = r_i32(r);
        client_map->tile_types[tile_idx] = r_u8(r);
        client_map->tile_resource_counts[tile_idx] = r_u8(r);
    } else if (t == MAP_snowball) {
        int snowball_idx = r_i32(r);
        while (snowball_idx >= sbcount(client_map->snowballs)) {
            sbadd(client_map->snowballs, 1)->used = false;
        }
        map_loader_snowball(&client_map->snowballs[snowball_idx], r);
    }
}

void game_client_step(float dt) {
    ENetEvent event;
    while (enet_host_service(client, &event, 0)) {
        switch (event.type) {
            case ENET_EVENT_TYPE_CONNECT:
                printf("Client: connected to server.\n");
                send_client_info();
                break;
            case ENET_EVENT_TYPE_RECEIVE: {
                const char* data = (const char*)(event.packet->data) + 1;
                switch ((enum message_type)event.packet->data[0]) {
                    case MSG_entire_map:
                        load_map_from_network(data,
                                              event.packet->dataLength - 1);
                        break;
                    case MSG_map_change:
                        handle_map_change((uint8_t**)&data);
                        break;
                    case MSG_player_id:
                        local_player_id = data[0];
                        break;
                    case MSG_game_over:
                        winner_player_id = data[0];
                        break;
                    default:
                        printf("Client got unexpected message type: %d\n",
                               event.packet->data[0]);
                        break;
                }
                enet_packet_destroy(event.packet);
                break;
            }
            case ENET_EVENT_TYPE_DISCONNECT:
                printf("Client: Disconnect\n");
                break;
            case ENET_EVENT_TYPE_NONE:
                break;
        }
    }

    {
        inkd main = ink_source(ink, I_main);
        inkd_bool(main, I_game, !!client_map);
        inkd_bool(main, I_game_pending, !client_map);
    }

    if (client_map) {
        handle_map_scroll(dt);  // Determines draw positions

        sbforeachp (Penguin* p, client_map->penguins) {
            if (p->type == PENGUIN_TYPE_END) continue;
            ClientPenguin* c = &client_penguins[stb__counter];

            vec2 pos = {c->lerp_x, c->lerp_y};
            vec2 targ = {p->x, p->y};
            vec2 velocity;
            vec2_sub(velocity, targ, pos);
            c->magnitude = vec2_mul_inner(velocity, velocity);
            assert(c->magnitude >= 0);
            if (c->magnitude < 1.f) {
                c->lerp_x = p->x;
                c->lerp_y = p->y;
            } else {
                float speed = c->magnitude * dt * 0.0005f;
                if (speed < dt * 8) speed = dt * 8;
                vec2_scale(velocity, velocity, speed);
                vec2_add(pos, pos, velocity);
                c->lerp_x = pos[0];
                c->lerp_y = pos[1];
                c->direction = atan2f(velocity[1], velocity[0]);
            }
        }

        render_step(client_map, dt);      // Determines draw positions
        game_selection_step(client_map);  // Uses draw positions
        game_minimap_step(client_map);    // Uses map scroll

        inkd source = ink_source(ink, I_game);

        ink_query q;
        ink_query_init(&q);

        inkd_rebuild_begin(source, I_penguins);
        sbforeachp (Penguin* p, client_map->penguins) {
            if (p->type == PENGUIN_TYPE_END) continue;
            ClientPenguin* c = &client_penguins[stb__counter];

            ink_query_int(&q, I_id, stb__counter);
            inkd d = ink_query_get(&q, source, I_penguins);
            inkd_int(d, I_x, c->draw_x - PENGUIN_SIZE / 2.f);
            inkd_int(d, I_y, c->draw_y - PENGUIN_SIZE / 2.f);
            inkd_color(d, I_player_color, PLAYER_COLORS[p->owner_id]);
            inkd_bool(d, I_selected, c->selected);
            inkd_int(d, I_type, p->type);
            inkd_int(d, I_health, p->health);
        }
        inkd_rebuild_end(source, I_penguins);

        inkd_rebuild_begin(source, I_buildings);
        sbforeachp (Building* b, client_map->buildings) {
            if (b->type == BUILDING_TYPE_END) continue;
            ink_query_int(&q, I_id, stb__counter);
            inkd d = ink_query_get(&q, source, I_buildings);
            inkd_int(d, I_x, b->tile_x * TILE_SIZE - viewport[0]);
            inkd_int(d, I_y, b->tile_y * TILE_SIZE - viewport[1]);
            inkd_int(d, I_size, TILE_SIZE * building_sizes[b->type]);
            inkd_color(d, I_player_color,
                       PLAYER_COLORS[MAX(PLAYER_NEUTRAL, b->owner_id)]);

            inkd_int(d, I_type, b->type);
            inkd_rebuild_begin(d, I_resources);
            for (int r = 0; r < RESOURCE_TYPE_END; r++) {
                inkd rd = inkd_append(d, I_resources);
                inkd_int(rd, I_type, r);
                inkd_int(rd, I_ammount, b->resources[r]);
            }
            inkd_rebuild_end(d, I_resources);
        }
        inkd_rebuild_end(source, I_buildings);

        ink_query_deinit(&q);

        inkd_charp(source, I_winner,
                   winner_player_id == PLAYER_NEUTRAL ? NULL : "somebody");

        sbforeachp (Snowball* s, client_map->snowballs) {
            s->progress += dt * 1000.f;
        }
    }
}

void game_client_shutdown(void) {
    if (player_name) {
        free(player_name);
        player_name = 0;
    }
    if (client_map) {
        map_destroy(client_map);
        client_map = 0;
    }
    if (client != 0) {
        enet_peer_disconnect(peer, 0);
        ENetEvent event;
        while (enet_host_service(client, &event, 0)) {
        }
        enet_host_destroy(client);
        peer = 0;
    }
    client = 0;

    sbfree(client_penguins);
    client_penguins = 0;
    game_minimap_shutdown();
    game_selection_shutdown();
}

void ink_clear_framebuffer(struct ink* ink);

bool game_client_render(void) {
    if (client_map) {
        if (inkd_get_bool(ink_source(ink, I_debug_dialog), I_show_nav_map)) {
            render_debug(client_map);
        }
        render(client_map);
        return true;
    }
    return false;
}

static bool keyboard_move[4];
#define SCROLL_SPEED 2200

static void handle_map_scroll(float dt) {
    // TODO only allow edge scrolling when fullscreened
    /*
    int mouse_x, mouse_y;
    SDL_GetMouseState(&mouse_x, &mouse_y);
    if (mouse_x > viewport[2] - 40)
        viewport[0] += SCROLL_SPEED*dt;
    else if (mouse_x < 40)
        viewport[0] -= SCROLL_SPEED*dt;
    if (mouse_y > viewport[3] - 40)
        viewport[1] += SCROLL_SPEED*dt;
    else if (mouse_y < 40)
        viewport[1] -= SCROLL_SPEED*dt;
    */

    if (keyboard_move[0])
        viewport[0] -= SCROLL_SPEED * dt;
    else if (keyboard_move[2])
        viewport[0] += SCROLL_SPEED * dt;
    if (keyboard_move[1])
        viewport[1] -= SCROLL_SPEED * dt;
    else if (keyboard_move[3])
        viewport[1] += SCROLL_SPEED * dt;

    // Clamp viewport to map
    int padding = 300;
    if (viewport[0] < -padding) viewport[0] = -padding;
    if (viewport[1] < -padding) viewport[1] = -padding;
    int s = client_map->size * TILE_SIZE + padding;
    if (viewport[0] > s - viewport[2]) viewport[0] = s - viewport[2];
    if (viewport[1] > s - viewport[3]) viewport[1] = s - viewport[3];
}

static void map_unhandled_event(const SDL_Event* ev) {
    if (ev->type == SDL_KEYDOWN || ev->type == SDL_KEYUP) {
        SDL_Keycode c = ev->key.keysym.sym;
        int d = 0;
        if (c == SDLK_w)
            d = 1;
        else if (c == SDLK_a)
            d = 0;
        else if (c == SDLK_s)
            d = 3;
        else if (c == SDLK_d)
            d = 2;
        else
            return;
        if (ev->type == SDL_KEYDOWN) {
            keyboard_move[d] = true;
        } else {
            keyboard_move[d] = false;
        }
    }
}

bool game_client_event(SDL_Event* ev) {
    if (!client_map) return false;
    if (game_command_event(client_map, ev)) return true;
    if (game_selection_event(client_map, ev)) return true;
    map_unhandled_event(ev);
    return false;
}

void game_client_command(struct i_writer w) {
    int count = sbcount(w.data);
    ENetPacket* packet =
        enet_packet_create(NULL, 1 + count, ENET_PACKET_FLAG_RELIABLE);
    packet->data[0] = MSG_player_command;
    memcpy((char*)packet->data + 1, w.data, count);
    enet_peer_send(peer, 0, packet);
}

void game_client_start(void) {
    ENetPacket* packet = enet_packet_create(NULL, 1, ENET_PACKET_FLAG_RELIABLE);
    packet->data[0] = MSG_start_game;
    enet_peer_send(peer, 0, packet);
}
