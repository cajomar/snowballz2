#ifndef __SB2_LOBBY__
#define __SB2_LOBBY__

void lobby_init(const char* map_dir);
void lobby_shutdown(void);
void lobby_step(void);

#endif
