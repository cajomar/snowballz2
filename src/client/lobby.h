#ifndef __SB2_LOBBY__
#define __SB2_LOBBY__

void lobby_init();
void lobby_shutdown();
void lobby_step();

#endif
