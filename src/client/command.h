#ifndef __SB2_GAME_COMMAND__
#define __SB2_GAME_COMMAND__
#include <SDL2/SDL_events.h>
#include <stdbool.h>

#include "../map.h"

enum command_mode {
    COMMAND_MODE_DEFAULT,
    COMMAND_MODE_BUILD,
};

void game_command_mode(enum command_mode);
bool game_command_event(const Map* map, SDL_Event* ev);

#endif
