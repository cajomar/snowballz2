#include "command.h"

#include <assert.h>
#include <ink_data.h>

#include "../host/game_host.h"
#include "../jobs.h"
#include "../map.h"
#include "../stretchy_buffer.h"
#include "../writer.h"
#include "game_client.h"
#include "idents.h"
#include "render.h"

extern struct ink* ink;
extern int viewport[4];

// Managed by game_client.c
extern ClientPenguin* client_penguins;

enum command_mode command_mode = COMMAND_MODE_DEFAULT;

void game_command_mode(enum command_mode mode) { command_mode = mode; }

static void exit_build_mode(void) {
    inkd source = ink_source(ink, I_game);
    inkd_bool(source, I_build_mode, false);
    game_command_mode(COMMAND_MODE_DEFAULT);
}

static bool command_build(const Map* map, SDL_Event* ev) {
    if (ev->type == SDL_MOUSEBUTTONDOWN) {
        if (ev->button.button == SDL_BUTTON_RIGHT) {
            exit_build_mode();
        } else if (ev->button.button == SDL_BUTTON_LEFT) {
            int x, y, tile_x, tile_y;
            render_screen_to_map_pixel(ev->button.x, ev->button.y, &x, &y);
            map_pixel_to_tile(map, x, y, &tile_x, &tile_y);
            inkd source = ink_source(ink, I_game);
            enum building_type type = inkd_get_int(source, I_active_building);
            if (map_can_place_building_at(map, type, tile_x, tile_y)) {
                struct i_writer w = {.data = 0};
                w_u8(&w, CMD_place_building);
                w_u8(&w, type);
                w_i32(&w, tile_x);
                w_i32(&w, tile_y);
                game_client_command(w);
                sbfree(w.data);
                exit_build_mode();
            }
        }
        return true;
    }
    return false;
}

static bool command_default(const Map* map, SDL_Event* ev) {
    if (ev->type == SDL_MOUSEBUTTONDOWN &&
        ev->button.button == SDL_BUTTON_RIGHT) {
        int x, y, tile_x, tile_y;
        render_screen_to_map_pixel(ev->button.x, ev->button.y, &x, &y);
        map_pixel_to_tile(map, x, y, &tile_x, &tile_y);

        Job job;

        int building = -1;
        enum map_tile_type tt = map->tile_types[tile_x + tile_y * map->size];
        if (tt == MAP_TILE_TYPE_EMPTY) {
            building = map_get_building(map, tile_x, tile_y);
            if (building != -1) {
                job.type = JOB_TYPE_UNLOAD;
                job.unload.building = building;
                job.unload.progress = 0;
            } else {
                // TODO see all other selected penguin's relative positions to
                // target goto into formation.
                job.type = JOB_TYPE_MOVE;
                job.move.x = x;
                job.move.y = y;
            }
        } else {
            job.type = JOB_TYPE_HARVEST;
            job.harvest.tile_x = tile_x;
            job.harvest.tile_y = tile_y;
            job.harvest.progress = 0;
            job.harvest.resource = map_tile_type_to_resource_type(tt);
        }

        // TODO instead of creating a new message for each penguin, send the
        // command once with a list of all the penguins to apply it to.
        struct i_writer w = {.data = 0};
        sbforeachp (const Penguin* p, map->penguins) {
            if (p->type == PENGUIN_TYPE_END) continue;
            int penguin_idx = stb__counter;
            ClientPenguin* c = &client_penguins[penguin_idx];
            if (c->selected) {
                if (building != -1) {
                    w_u8(&w, CMD_assign_penguin_to_building);
                    w_i32(&w, penguin_idx);
                    w_i32(&w, building);
                    game_client_command(w);
                    sbresize(w.data, 0);
                } else {
                    w_u8(&w, CMD_assign_penguin_job);
                    w_i32(&w, penguin_idx);
                    job_save(&w, &job);
                    game_client_command(w);
                    sbresize(w.data, 0);
                }
            }
        }
        sbfree(w.data);
        return true;
    }
    return false;
}

bool game_command_event(const Map* map, SDL_Event* ev) {
    switch (command_mode) {
        case COMMAND_MODE_DEFAULT:
            return command_default(map, ev);
        case COMMAND_MODE_BUILD:
            return command_build(map, ev);
        default:
            assert(false);
    }
}
