#include "../render_debug.h"

// #include <lib2d.h>

#include "../../utils.h"

// For game_nav_get_tree()
#include "../../host/nav.h"

extern int viewport[4];

void render_game_nav_tree(const Map* map) {
    /* if (map == 0) return;
    lib2d_drawable d = {
        .w = TILE_SIZE,
        .h = TILE_SIZE,
    };
    int start_x = MAX(0, viewport[0] / TILE_SIZE);
    int start_y = MAX(0, viewport[1] / TILE_SIZE);
    int end_x = MIN(map->size - 1, start_x + viewport[2] / TILE_SIZE + 2);
    int end_y = MIN(map->size - 1, start_y + viewport[3] / TILE_SIZE + 2);
    for (int y = start_y; y < end_y; y++) {
        for (int x = start_x; x < end_x; x++) {
            d.x = x * TILE_SIZE - viewport[0];
            d.y = y * TILE_SIZE - viewport[1];
            int32_t v = game_nav_get_tree(x, y);
            d.color = 0xff00 | MIN(0xff, v * 20);
            lib2d_draw(&d);
        }
    } */
}

void render_debug(const Map* map) { render_game_nav_tree(map); }
