#include "../render.h"

#include <assert.h>
#include <ink.h>
#include <ink_data.h>
#include <ink_query.h>
#include <lib3d.h>

#include "../../map.h"
#include "../../mat.h"
#include "../../reader.h"
#include "../../rules.h"
#include "../../stretchy_buffer.h"
#include "../../utils.h"
#include "../game_client.h"
#include "../idents.h"

#define NOISE_RANGE 70
#define NOISE_COUNT ((512 * 512) + 3)

#define STRINGIFY2(X) #X
#define STRINGIFY(X) STRINGIFY2(X)

#define PRINT_VEC3(X) printf(STRINGIFY(X) ": %f %f %f\n", X[0], X[1], X[2])
#define PRINT_FLOAT(X) printf(STRINGIFY(X) ": %f\n", X)

extern struct ink* ink;
extern ClientPenguin* client_penguins;  // stretchy_buffer
extern const uint32_t PLAYER_COLORS[4];
extern const int building_sizes[BUILDING_TYPE_END];

static int* noise = 0;

static unsigned int time = 0;  // In milliseconds
static uint64_t delta_time = 0;

extern int viewport[4];

static const int PASS_MAIN = 0;
static struct l3d_scene* scene;

static struct l3d_model* penguin;
static int penguin_model_instance;
static int num_penguin_instances = 0;

static struct l3d_model* igloo;
static int igloo_model_instance;
static int num_building_instances = 0;

static struct l3d_model* tree;
static int tree_model_instance;
static int num_tree_instances = 0;

void render_init() {
    l3d_init();
#define DATA_PATH "data/3d/"
    l3d_set_shaders_path(DATA_PATH "shaders/", ".glsl");
    l3d_add_shader_directive("", "#version 140");

    struct l3d_mesh_bank* bank = l3d_mesh_bank_load(DATA_PATH "models.bin");

    {
        scene = l3d_scene_new();

        {
            penguin = l3d_mesh_bank_get(bank, l3d_ident_from_str("untitled"));
            struct l3d_shader_config conf;
            conf.texture_slots[0] = NULL;
            struct l3d_shader* shader =
                l3d_shader_load("default", "default", &conf);

            vec3 color = {0.3, 0.3, 0.3};
            l3d_shader_set_uniform(shader, "color", 3, color);

            l3d_model_set_shader(penguin, shader, -1, PASS_MAIN);
            l3d_scene_add_model(scene, penguin);
            mat4x4 t;
            mat4x4_identity(t);
            float s = PENGUIN_SIZE / (float)TILE_SIZE / 2.f;
            mat4x4_scale_aniso(t, t, s, s, s);
            l3d_model_set_transform(penguin, t);

            penguin_model_instance = l3d_model_new_instance_attr(
                penguin, "model_instance_matrix", 2);
        }

        {
            igloo = l3d_mesh_bank_get(bank, l3d_ident_from_str("untitled"));
            struct l3d_shader_config conf;
            conf.texture_slots[0] = NULL;
            struct l3d_shader* shader =
                l3d_shader_load("default", "igloo", &conf);

            vec3 color = {0.5, 0.3, 0.1};
            l3d_shader_set_uniform(shader, "color", 3, color);

            l3d_model_set_shader(igloo, shader, -1, PASS_MAIN);
            l3d_scene_add_model(scene, igloo);
            mat4x4 t;
            mat4x4_identity(t);
            float s = building_sizes[BUILDING_TYPE_IGLOO] / 2.f;
            mat4x4_scale_aniso(t, t, s, s, 1);
            l3d_model_set_transform(igloo, t);

            igloo_model_instance =
                l3d_model_new_instance_attr(igloo, "model_instance_matrix", 2);
        }

        {
            tree = l3d_mesh_bank_get(bank, l3d_ident_from_str("tree"));
            struct l3d_shader_config conf;
            conf.texture_slots[0] = NULL;
            struct l3d_shader* shader =
                l3d_shader_load("default", "tree", &conf);

            vec3 color = {0.2, 0.7, 0.2};
            l3d_shader_set_uniform(shader, "color", 3, color);

            l3d_model_set_shader(tree, shader, -1, PASS_MAIN);
            l3d_scene_add_model(scene, tree);
            mat4x4 t;
            mat4x4_identity(t);
            mat4x4_scale_aniso(t, t, 0.4, 0.4, 0.2);
            l3d_model_set_transform(tree, t);

            tree_model_instance =
                l3d_model_new_instance_attr(tree, "model_instance_matrix", 2);
        }

        struct l3d_model* plane = l3d_model_new();
        l3d_model_set_mesh(plane, 0, l3d_geom_plane(256, 256));

        struct l3d_texture* tex =
            l3d_texture_from_path("datasrc/snow_normal.png", 0);
        l3d_model_set_texture(plane, 0, 0, tex);

        struct l3d_shader_config conf;
        conf.texture_slots[0] = "snow";
        conf.texture_slots[1] = NULL;
        l3d_model_set_shader(plane, l3d_shader_load("default", "ground", &conf),
                             -1, PASS_MAIN);
        l3d_scene_add_model(scene, plane);
    }

    if (noise == 0) {
        noise = malloc(sizeof(int) * NOISE_COUNT);
        for (int i = 0; i < NOISE_COUNT; i++) {
            noise[i] = (rand() % NOISE_RANGE);
        }
    }
}

void render_shutdown() {
    free(noise);
    noise = 0;

    l3d_shutdown();
}

static void get_pv_matrix(mat4x4 pv) {
    struct l3d_view* view = l3d_scene_view(scene);
    mat4x4 view_matrix;
    float eye[3];
    eye[0] = 0;
    eye[1] = 0;
    eye[2] = 0;
    float up[3];
    up[0] = 0;
    up[1] = 0;
    up[2] = 1;
    mat4x4_look_at(view_matrix, eye, view->look_at, up);
    mat4x4_translate_in_place(view_matrix, view->translate[0],
                              view->translate[1], view->translate[2]);
    mat4x4_mul(pv, view->projection, view_matrix);
}

void render_screen_to_map_pixel(int x, int y, int* map_x, int* map_y) {
    mat4x4 camera, inverse_camera;
    get_pv_matrix(camera);
    mat4x4_invert(inverse_camera, camera);

    vec4 vec = {(x / (float)viewport[2]) * 2.f - 1.f,
                (1 - y / (float)viewport[3]) * 2.f - 1.f, 1.f, 1.f};

    vec4 ray;
    mat4x4_mul_vec4(ray, inverse_camera, vec);
    vec3_scale(ray, ray, 1.f / ray[3]);
    vec3_norm(ray, ray);
    assert(ray[1] > 0);
    assert(ray[2] < 0);

    vec3 up = {0, 0, 1};

    struct l3d_view* view = l3d_scene_view(scene);

    float d = vec3_mul_inner(up, ray);
    vec3 diff;
    vec3 center = {0, 0, 0};
    // view->translate is -camera_pos, and double subtraction becomes addition
    vec3_add(diff, center, view->translate);

    float t = vec3_mul_inner(diff, up) / d;

    vec3 intersect;
    vec3_scale(intersect, ray, t);
    vec3_sub(intersect, intersect, view->translate);

    *map_x = intersect[0] * TILE_SIZE;
    *map_y = -intersect[1] * TILE_SIZE;
}

void render_world_space_to_screen(const vec3 world, float* x, float* y) {
    mat4x4 pv_matrix;
    get_pv_matrix(pv_matrix);

    vec4 world4;
    vec3_scale(world4, world, 1.f);
    world4[3] = 1;
    vec4 p;
    mat4x4_mul_vec4(p, pv_matrix, world4);

    // We only care about xy here
    vec2_scale(p, p, 1.f / p[3]);
    // Convert from -1..1 to 0..viewport[2-3]
    p[0] += 1.f;
    p[1] += 1.f;
    vec2_scale(p, p, 0.5);
    p[0] *= viewport[2];
    p[1] *= viewport[3];
    // Flip the y coordinate
    p[1] = viewport[3] - p[1];

    *x = p[0];
    *y = p[1];
}

void render_step(const Map* map, float dt) {
    time += dt * 1000;
    delta_time = dt * 1000;

    {
        struct l3d_view* view = l3d_scene_view(scene);
        view->translate[0] =
            (-viewport[0] - viewport[2] / 2.f) / (float)TILE_SIZE;
        float zoom = 10;
        view->translate[1] = zoom + (viewport[1] * 1.f) / (float)TILE_SIZE;
        view->translate[2] = -zoom;
        view->look_at[0] = 0;
        view->look_at[1] = 1;
        view->look_at[2] = -1;
    }

    sbforeachp (const Penguin* p, map->penguins) {
        if (p->type == PENGUIN_TYPE_END) continue;
        ClientPenguin* c = &client_penguins[stb__counter];

        vec3 model = {
            c->lerp_x / (float)TILE_SIZE,
            -c->lerp_y / (float)TILE_SIZE,
            0,
        };
        render_world_space_to_screen(model, &c->draw_x, &c->draw_y);
    }
}

void render(const Map* map) {
    l3d_viewport(viewport[2], viewport[3]);

    assert(map);

    int num_visible_penguins = 0;

    sbforeachp (const Penguin* p, map->penguins) {
        if (p->type == PENGUIN_TYPE_END) continue;
        vec2 model;

        if (sbcount(client_penguins) == sbcount(map->penguins)) {
            // Only use `client_penguins` if it looks up to date with the map.
            const ClientPenguin* c = &client_penguins[stb__counter];

            num_visible_penguins++;
            while (num_penguin_instances < num_visible_penguins) {
                int i = l3d_model_instance(penguin);
                num_penguin_instances++;
                assert(i == num_penguin_instances - 1);
            }

            model[0] = c->lerp_x / (float)TILE_SIZE;
            model[1] = -c->lerp_y / (float)TILE_SIZE;
        } else {
            // Otherwize draw a still penguin
            model[0] = p->x / (float)TILE_SIZE;
            model[0] = p->x / (float)TILE_SIZE;
        }

        l3d_instance_attr(penguin, num_visible_penguins - 1,
                          penguin_model_instance, model);
    }

    while (num_building_instances < sbcount(map->buildings)) {
        num_building_instances = l3d_model_instance(igloo) + 1;
    }

    sbforeachp (const Building* b, map->buildings) {
        if (b->type == BUILDING_TYPE_END) continue;
        vec2 model = {
            b->tile_x + building_sizes[b->type] / 2.f,
            -b->tile_y - building_sizes[b->type] / 2.f,
        };
        l3d_instance_attr(igloo, stb__counter, igloo_model_instance, model);
    }

    int num_trees = 0;

    for (int y = 0; y < map->size; y++) {
        // Each tree tile has three trees, draw the back row first, then the
        // front.
        for (int tree_row = 0; tree_row < 3; tree_row++) {
            for (int x = 0; x < map->size; x++) {
                int index = y * map->size + x;
                enum map_tile_type t = map->tile_types[index];
                uint8_t count = map->tile_resource_counts[index];
                if (t == MAP_TILE_TYPE_TREE) {
                    num_trees += count;
                    while (num_tree_instances < num_trees) {
                        num_tree_instances = l3d_model_instance(tree) + 1;
                    }
                    for (int tree_row = 0; tree_row < count; tree_row++) {
                        assert(index < NOISE_COUNT);
                        int rand = noise[index + tree_row];
                        float offset_x = (1.f / 3) * ((rand % 3) - 1);
                        float offset_y = (1.f / 3) * (tree_row - 1);
                        vec2 model = {
                            x + 0.3 + offset_x,
                            -y - 0.3 - offset_y,
                        };
                        l3d_instance_attr(tree, num_trees - tree_row,
                                          tree_model_instance, model);
                    }
                }
            }
        }
    }

    struct l3d_view* view = l3d_scene_view(scene);
    mat4x4_perspective(view->projection, 45, viewport[2] / (float)viewport[3],
                       0.01, 1000);

    struct l3d_scene_render_conf conf = {.target = NULL,
                                         .time_past = delta_time,
                                         .pass = PASS_MAIN,
                                         .clear = true,
                                         .blend = false,
                                         .depth_test = true,
                                         .depth_writable = true,
                                         .cull_face = true};
    l3d_scene_render(scene, &conf);
}
