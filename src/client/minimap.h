#ifndef __SB2_GAME_MINIMAP__
#define __SB2_GAME_MINIMAP__

#include "../map.h"

void game_minimap_init();
void game_minimap_shutdown();
void game_minimap_step(const Map* map);

#endif
