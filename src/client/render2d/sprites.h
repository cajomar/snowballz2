#ifndef SB2_SRC_CLIENT_RENDER2D_SPRITES_H_5HFAI97U
#define SB2_SRC_CLIENT_RENDER2D_SPRITES_H_5HFAI97U

#define SKETCH_COLOR 0xffe09fff
#define SKETCH_SNOW_SHADOW_COLOR 0xff
#define SNOW_SHADOW_COLOR 0xaaffff

#define PENGUIN_SPRITE_DIRECTIONS 16
#define PENGUIN_ANIM_LENGTH 6

#define BUILDING_RESOURCE_OVERLAY_COUNT 3
#define BUILDING_CONSTRUCTION_SPRITE_COUNT 3

#define TREE_TYPE_COUNT 3

#include <lib2d.h>

#include "../../map.h"

struct building_sprite {
    lib2d_drawable building;
    lib2d_drawable marker;
    lib2d_drawable shadow;
    struct {
        lib2d_drawable building;
        lib2d_drawable shadow;
    } construction[BUILDING_CONSTRUCTION_SPRITE_COUNT];
    lib2d_drawable overlays[RESOURCE_TYPE_END][BUILDING_RESOURCE_OVERLAY_COUNT];
    lib2d_drawable sketch;
    lib2d_drawable sketch_shadow;
};

enum penguin_sprite_type {
    PENGUIN_SPRITE_TYPE_PENGUIN = 0,
    PENGUIN_SPRITE_TYPE_SHADOW,
    PENGUIN_SPRITE_TYPE_OUTFIT,
    PENGUIN_SPRITE_TYPE_END
};

typedef lib2d_drawable penguin_directional_anim[PENGUIN_ANIM_LENGTH]
                                               [PENGUIN_SPRITE_TYPE_END];
typedef penguin_directional_anim penguin_sprite_anim[PENGUIN_SPRITE_DIRECTIONS];
typedef penguin_directional_anim* penguin_anim;

void sprites_init(void);
void sprites_shutdown(void);
penguin_anim sprites_get_penguin_anim(int penguin_idx, const Map* map);
void sprites_get_building_sprite(const Building* b, lib2d_drawable* building,
                                 lib2d_drawable* shadow,
                                 lib2d_drawable* marker);
lib2d_drawable sprites_get_building_overlay(const Building* b,
                                            enum resource_type type);
lib2d_drawable sprites_get_tree(int tree_type, int tree_row, int count);

#endif /* SB2_SRC_CLIENT_RENDER2D_SPRITES_H_5HFAI97U */
