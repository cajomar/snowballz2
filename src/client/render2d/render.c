#include "../render.h"

#include <assert.h>
#include <ink.h>
#include <ink_data.h>
#include <lib2d.h>

#include "../../map.h"
#include "../../mat.h"
#include "../../reader.h"
#include "../../rules.h"
#include "../../stretchy_buffer.h"
#include "../../utils.h"
#include "../game_client.h"
#include "../idents.h"
#include "sprites.h"

#define NOISE_RANGE 70
#define NOISE_COUNT ((512 * 512) + 3)

extern lib2d_drawable snow;
extern lib2d_drawable snow_sketch;
extern lib2d_drawable snow_overlay;
extern lib2d_drawable snowball;

extern struct ink* ink;
extern ClientPenguin* client_penguins;  // stretchy_buffer
extern const uint32_t PLAYER_COLORS[4];
extern const int building_sizes[BUILDING_TYPE_END];

static int* noise = 0;
static int** penguin_rows = 0;  // stretchy_buffer of stretchy_buffers

static unsigned int time = 0;  // In milliseconds

bool sketch;

extern int viewport[4];

void render_init() {
    if (lib2d_init(LIB2D_RENDER_BACKEND_GL, 0) != 0) {
        fprintf(stderr, "%s\n", lib2d_get_error());
        exit(EXIT_FAILURE);
    }

    sprites_init();

    if (noise == 0) {
        noise = malloc(sizeof(int) * NOISE_COUNT);
        for (int i = 0; i < NOISE_COUNT; i++) {
            noise[i] = (rand() % NOISE_RANGE);
        }
    }
}

void render_shutdown() {
    sprites_shutdown();

    free(noise);
    noise = 0;

    sbforeachv (int* penguin_row, penguin_rows) {
        sbfree(penguin_row);
    }
    sbfree(penguin_rows);

    lib2d_shutdown();
}

static void render_building(Building* b) {
    assert(b->type != BUILDING_TYPE_END);

    /*
    // Align to bottom of sprite
    d.y += TILE_SIZE - d.h;
    // Compensate for building size
    d.y += (building_sizes[b->type]-1) * TILE_SIZE;
    */
    lib2d_drawable bs, ss, m;
    sprites_get_building_sprite(b, &bs, &ss, &m);

    // Draw shadows
    ss.x += b->tile_x * TILE_SIZE - viewport[0];
    ss.y += b->tile_y * TILE_SIZE - viewport[1];
    lib2d_draw(&ss);

    // Draw building
    bs.x += b->tile_x * TILE_SIZE - viewport[0];
    bs.y += b->tile_y * TILE_SIZE - viewport[1];
    lib2d_draw(&bs);

    // Draw player marker
    m.x += b->tile_x * TILE_SIZE - viewport[0];
    m.y += b->tile_y * TILE_SIZE - viewport[1];
    m.color = PLAYER_COLORS[MAX(PLAYER_NEUTRAL, b->owner_id)];
    lib2d_draw(&m);

    // Draw resource overlays
    if (!sketch && b->status != BUILDING_STATUS_CONSTRUCTING) {
        for (int i = 0; i < RESOURCE_TYPE_END; i++) {
            if (b->resources[i] == 0) continue;

            lib2d_drawable d = sprites_get_building_overlay(b, i);
            d.x += b->tile_x * TILE_SIZE - viewport[0];
            d.y += b->tile_y * TILE_SIZE - viewport[1];
            lib2d_draw(&d);
        }
    }
}

static unsigned int get_penguin_anim_frame(const Penguin* p) {
    // 30 frames per second
    unsigned int frame = (time / 1000.f * 36.f);
    // unsigned int frame = (time / 1000.f * 21.f);
    // add random penguin-specific number
    frame += noise[((uintptr_t)p / sizeof(Penguin)) % NOISE_COUNT];
    frame %= PENGUIN_ANIM_LENGTH * 2;
    if (frame >= PENGUIN_ANIM_LENGTH) {
        frame = PENGUIN_ANIM_LENGTH * 2 - frame - 1;
    }
    assert(frame < PENGUIN_ANIM_LENGTH);
    return frame;
}

static void render_penguin(int i, const Map* map) {
    const Penguin* p = &map->penguins[i];
    assert(p->type != PENGUIN_TYPE_END);

    float draw_x, draw_y;
    int direction;
    if (sbcount(client_penguins) == sbcount(map->penguins)) {
        // Use client_penguins if it appears to be in sync with map.
        const ClientPenguin* c = &client_penguins[i];
        draw_x = c->draw_x;
        draw_y = c->draw_y;
        direction = (int)(roundf((-c->direction + M_PI * 2) *
                                 PENGUIN_SPRITE_DIRECTIONS / (M_PI * 2.f))) %
                    PENGUIN_SPRITE_DIRECTIONS;
    } else {
        // Other wise draw still penguins
        draw_x = p->x - TILE_SIZE;
        draw_y = p->y - TILE_SIZE;
        direction = 0;
    }

    unsigned int frame = p->health > 0 ? get_penguin_anim_frame(p) : 0;

    penguin_anim anim = sprites_get_penguin_anim(i, map);

    assert(direction >= 0 && direction <= PENGUIN_SPRITE_DIRECTIONS);

    // Draw the shadow
    lib2d_drawable d = anim[direction][frame][PENGUIN_SPRITE_TYPE_SHADOW];
    d.x += draw_x;
    d.y += draw_y;
    d.color = sketch ? SKETCH_SNOW_SHADOW_COLOR : SNOW_SHADOW_COLOR;
    lib2d_draw(&d);

    if (p->health <= 0) {
        // Draw the top of an ice cube
        float w = anim[direction][frame][PENGUIN_SPRITE_TYPE_PENGUIN].w;
        d.w = w * 1.1;
        float h = anim[direction][frame][PENGUIN_SPRITE_TYPE_PENGUIN].h;
        d.h = h / 4.f;
        d.x = anim[direction][frame][PENGUIN_SPRITE_TYPE_PENGUIN].x + draw_x -
              w * 0.05;
        d.y = anim[direction][frame][PENGUIN_SPRITE_TYPE_PENGUIN].y + draw_y +
              h / 12.f;
        d.color = 0xa8e4f6cb;
        d.image = NULL;
        lib2d_draw(&d);
    }

    // Draw the penguin
    d = anim[direction][frame][PENGUIN_SPRITE_TYPE_PENGUIN];
    d.x += draw_x;
    d.y += draw_y;
    d.color = sketch ? SKETCH_COLOR : 0xffffffff;
    lib2d_draw(&d);
    // Draw the outfit
    d = anim[direction][frame][PENGUIN_SPRITE_TYPE_OUTFIT];
    d.x += draw_x;
    d.y += draw_y;
    d.color = PLAYER_COLORS[p->owner_id];
    lib2d_draw(&d);

    if (p->health <= 0) {
        // Draw the side of an ice cube
        float w = anim[direction][frame][PENGUIN_SPRITE_TYPE_PENGUIN].w;
        d.w = w * 1.1;
        float h = anim[direction][frame][PENGUIN_SPRITE_TYPE_PENGUIN].h;
        d.h = h / 3.f * 2.f + 8.f;
        d.x = anim[direction][frame][PENGUIN_SPRITE_TYPE_PENGUIN].x + draw_x -
              w * 0.05;
        d.y = anim[direction][frame][PENGUIN_SPRITE_TYPE_PENGUIN].y + draw_y +
              h / 3.f;
        d.color = 0x69d1efbb;
        d.image = NULL;
        lib2d_draw(&d);
    }
}

void render_screen_to_map_pixel(int x, int y, int* map_x, int* map_y) {
    *map_x = x + viewport[0];
    *map_y = y + viewport[1];
}

int compar(const void* a, const void* b) {
    return client_penguins[*(int*)a].draw_y - client_penguins[*(int*)b].draw_y;
}

void render_step(const Map* map, float dt) {
    time += dt * 1000;

    sketch = inkd_get_bool(ink_source(ink, I_debug_dialog), I_show_sketch);

    int top = MAX(0, MIN(viewport[1] / TILE_SIZE - 1, map->size));
    int bottom =
        MAX(0, MIN((viewport[1] + viewport[3]) / TILE_SIZE + 2, map->size));

    const int height = bottom - top;
    while (sbcount(penguin_rows) < height) {
        sbpush(penguin_rows, 0);
    }
    for (int i = 0; i < height; i++) {
        sbresize(penguin_rows[i], 0);
    }

    sbforeachp (const Penguin* p, map->penguins) {
        if (p->type == PENGUIN_TYPE_END) continue;
        ClientPenguin* c = &client_penguins[stb__counter];
        c->draw_x = c->lerp_x - viewport[0];
        c->draw_y = c->lerp_y - viewport[1];

        if (c->draw_x < -PENGUIN_SIZE || c->draw_y < -PENGUIN_SIZE ||
            c->draw_x > viewport[2] + PENGUIN_SIZE ||
            c->draw_y > viewport[3] + PENGUIN_SIZE) {
            continue;
        }
        // Add penguin to its row
        const int row = c->lerp_y / TILE_SIZE - top;
        // This could fail if c->lerp_y is -nan, probably caused when dt is very
        // large
        assert(0 <= row && row < height);
        sbpush(penguin_rows[row], stb__counter);
    }
}

void render(const Map* map) {
    lib2d_viewport(viewport[2], viewport[3]);

    assert(map);

    inkd debug_source = ink_source(ink, I_debug_dialog);
    bool sketch = inkd_get_bool(debug_source, I_show_sketch);
    if (!inkd_get_bool(debug_source, I_show_nav_map)) {
        lib2d_drawable* d = sketch ? &snow_sketch : &snow;
        int w = d->w;
        int h = d->h;
        int left = MAX(0, viewport[0] / w);
        int top = MAX(0, viewport[1] / w);
        int right =
            MIN((viewport[0] + viewport[2]) / h + 1, map->size * TILE_SIZE / w);
        int bottom =
            MIN((viewport[1] + viewport[3]) / h + 1, map->size * TILE_SIZE / h);
        for (int y = top; y < bottom; y++) {
            for (int x = left; x < right; x++) {
                d->x = x * w - viewport[0];
                d->y = y * h - viewport[1];
                lib2d_draw(d);
            }
        }
        snow_overlay.color = sketch ? 0x5d3409ff : SNOW_SHADOW_COLOR;
        snow_overlay.color = (snow_overlay.color & 0xffffff00) | 0xf;
        w = snow_overlay.w;
        h = snow_overlay.h;
        left = MAX(0, viewport[0] / w - 1);
        top = MAX(0, viewport[1] / w - 1);
        right =
            MIN((viewport[0] + viewport[2]) / h + 1, map->size * TILE_SIZE / w);
        bottom =
            MIN((viewport[1] + viewport[3]) / h + 1, map->size * TILE_SIZE / h);
        for (int y = top; y < bottom; y++) {
            for (int x = left; x < right; x++) {
                snow_overlay.x = x * w - viewport[0];
                snow_overlay.y = y * w - viewport[1];
                lib2d_draw(&snow_overlay);
            }
        }
    }

    int left = MAX(0, MIN(viewport[0] / TILE_SIZE - 1, map->size));
    int top = MAX(0, MIN(viewport[1] / TILE_SIZE - 1, map->size));
    int right =
        MAX(0, MIN((viewport[0] + viewport[2]) / TILE_SIZE + 2, map->size));
    int bottom =
        MAX(0, MIN((viewport[1] + viewport[3]) / TILE_SIZE + 2, map->size));

    for (int y = top; y < bottom; y++) {
        // Each tree tile has three trees, draw the back row first, then the
        // front.
        for (int tree_row = 0; tree_row < 3; tree_row++) {
            for (int x = left; x < right; x++) {
                int index = y * map->size + x;
                enum map_tile_type t = map->tile_types[index];
                uint8_t count = map->tile_resource_counts[index];
                if (t == MAP_TILE_TYPE_TREE) {
                    assert(index < NOISE_COUNT);
                    int rand = noise[index + tree_row];
                    int tree_type = (rand + tree_row) % TREE_TYPE_COUNT;
                    int offset_x = (TILE_SIZE / 3) * ((rand % 3) - 1);
                    int offset_y = (TILE_SIZE / 3) * (tree_row - 1);

                    lib2d_drawable d =
                        sprites_get_tree(tree_type, tree_row, count);

                    d.x += x * TILE_SIZE + offset_x - viewport[0];
                    d.y += y * TILE_SIZE + offset_y - viewport[1];
                    lib2d_draw(&d);
                }
            }
        }
        for (int x = left; x < right; x++) {
            int index = y * map->size + x;
            // Draw any buildings on this tile.
            int building_id = map->views.building_draw_order[index];
            if (building_id != NO_BUILDING) {
                render_building(&map->buildings[building_id]);
            }
        }
        // Draw all the penguins in this row
        int row = y - top;
        sbsort(penguin_rows[row], compar);
        sbforeachv (int index, penguin_rows[row]) {
            render_penguin(index, map);
        }
    }

    sbforeachp (const Snowball* s, map->snowballs) {
        if (!s->used) continue;

        lib2d_drawable d = snowball;

        if (s->progress > s->total_time) {
            float f = (s->progress / (float)s->total_time);
            assert(f > 1);
            d.w *= f * 1.4f;
            d.h *= f * 1.4f;

            d.color = 0xffffff00 | (int)(0xff / f);

            d.x = s->target_x - d.w / 2.f;
            d.y = s->target_y - d.h / 2.f;
        } else {
            vec2 dist = {
                s->target_x - s->x,
                s->target_y - s->y,
            };

            vec2_scale(dist, dist, s->progress / (float)s->total_time);

            d.x = s->x + dist[0];
            d.y = s->y + dist[1];
        }
        d.x -= viewport[0];
        d.y -= viewport[1];

        lib2d_draw(&d);
    }

    lib2d_clear(1.f, 1.f, 1.f, 1.f);
    lib2d_render();
}
