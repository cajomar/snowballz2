#include "sprites.h"

#include <assert.h>
#include <stdlib.h>

#include "../../rules.h"
#include "../../utils.h"
#include "../game_client.h"

#ifndef SB2_ASSET_DIR
#define SB2_ASSET_DIR "data/"
#endif

// The most a building will likely have of any one type of resources
#define MAX_RESOURCES 30

#define lib2d_image_delete(img) ((img) ? lib2d_image_delete(img) : (void)0)

extern ClientPenguin* client_penguins;  // stretchy_buffer
extern bool sketch;

extern const int building_sizes[BUILDING_TYPE_END];

static struct building_sprite building_sprites[BUILDING_TYPE_END];

static penguin_sprite_anim worker_stand_anims[RESOURCE_TYPE_END + 1];
static penguin_sprite_anim worker_walk_anims[RESOURCE_TYPE_END + 1];
static penguin_sprite_anim worker_harvest_anims[RESOURCE_TYPE_END];
// The +1 one is for building
static penguin_sprite_anim worker_unload_anims[RESOURCE_TYPE_END + 1];
static penguin_sprite_anim warrior_stand_anim;
static penguin_sprite_anim warrior_walk_anim;
static penguin_sprite_anim warrior_throw_anim;

static lib2d_drawable tree_sprites[TREE_TYPE_COUNT][2];  // Trees and stumps
static lib2d_drawable tree_sketch_sprites[TREE_TYPE_COUNT][2];

lib2d_drawable snow;
lib2d_drawable snow_sketch;
lib2d_drawable snow_overlay;
lib2d_drawable snowball;

static lib2d_image_info atlas_infos[4];
static lib2d_image* atlases[4];

static lib2d_drawable* load_sprite_image(FILE* databin, lib2d_drawable* sprite,
                                         const char* path,
                                         uint16_t offsets[4]) {
    uint8_t bpp;
    fread(&bpp, 1, 1, databin);

    uint16_t xywh[4];
    fread(xywh, 2, 4, databin);

    if (offsets) {
        fread(offsets, 2, 4, databin);
    } else {
        fseek(databin, 8, SEEK_CUR);
    }
    assert(bpp > 0 && bpp <= 4);

    sprite->image = lib2d_image_sub(
        atlases[bpp - 1], xywh[0] / (float)atlas_infos[bpp - 1].w,
        xywh[1] / (float)atlas_infos[bpp - 1].h,
        (xywh[0] + xywh[2]) / (float)atlas_infos[bpp - 1].w,
        (xywh[1] + xywh[3]) / (float)atlas_infos[bpp - 1].h);

    if (sprite->image) {
        sprite->w = xywh[2];
        sprite->h = xywh[3];
        sprite->color = 0xffffffff;
    } else {
        fprintf(stderr, "Error loading %s: %s\n", path, lib2d_get_error());
        sprite->w = 32;
        sprite->h = 32;
        sprite->color = ((rand() % 0x1000000) << 8) | 0xff;
    }
    sprite->rot = 0.f;
    sprite->x = 0.f;
    sprite->y = 0.f;

    return sprite;
}

static void load_penguin_anim(FILE* databin, const char* p, penguin_anim anim) {
    const char* penguin_sprite_names[PENGUIN_SPRITE_TYPE_END] = {
        "body",
        "shadow",
        "outfit",
    };
    char path[128];

    for (int d = 0; d < PENGUIN_SPRITE_DIRECTIONS; d++) {
        for (int f = 0; f < PENGUIN_ANIM_LENGTH; f++) {
            for (int s = 0; s < PENGUIN_SPRITE_TYPE_END; s++) {
                snprintf(path, 128, "%s/%s_%d_%d.png", p,
                         penguin_sprite_names[s], d, f);
                uint16_t o[4];
                lib2d_drawable* drawable =
                    load_sprite_image(databin, &anim[d][f][s], path, o);
                drawable->x = o[0] - o[2] / 2;
                drawable->y = o[1] - o[3] * 5.f / 8.f;
            }
            anim[d][f][PENGUIN_SPRITE_TYPE_SHADOW].color = SNOW_SHADOW_COLOR;
        }
    }
}

static void load_penguin_sprites(FILE* databin) {
    const char* resource_names[RESOURCE_TYPE_END + 1] = {
        "tree",  // RESOURCE_TYPE_TREE
        "none",  // RESOURCE_TYPE_END
    };
    char path[128];
    for (int i = 0; i < RESOURCE_TYPE_END + 1; i++) {
        snprintf(path, 128, "penguins/worker/stand_%s", resource_names[i]);
        load_penguin_anim(databin, path, worker_stand_anims[i]);
        snprintf(path, 128, "penguins/worker/walk_%s", resource_names[i]);
        load_penguin_anim(databin, path, worker_walk_anims[i]);
        snprintf(path, 128, "penguins/worker/unload_%s", resource_names[i]);
        load_penguin_anim(databin, path, worker_unload_anims[i]);
    }
    for (int i = 0; i < RESOURCE_TYPE_END; i++) {
        sprintf(path, "penguins/worker/harvest_%s", resource_names[i]);
        load_penguin_anim(databin, path, worker_harvest_anims[i]);
    }
    load_penguin_anim(databin, "penguins/warrior/walk_none", warrior_walk_anim);
    load_penguin_anim(databin, "penguins/warrior/stand_none",
                      warrior_stand_anim);
    load_penguin_anim(databin, "penguins/warrior/unload_snowball",
                      warrior_throw_anim);
}

static void load_building_sprites(FILE* databin) {
    const char* building_names[BUILDING_TYPE_END] = {
        "igloo",   // BUILDING_TYPE_IGLOO
        "colony",  // BUILDING_TYPE_COLONY
    };
    const char* resource_names[RESOURCE_TYPE_END] = {
        "tree",  // RESOURCE_TYPE_TREE
    };
    for (int i = 0; i < BUILDING_TYPE_END; i++) {
        char full_path[512];
        char* path = strncpy(full_path, SB2_ASSET_DIR, 512);
        uint16_t o[4];
        lib2d_drawable* d;

#define LOAD(D, ...)                                        \
    {                                                       \
        sprintf(path, __VA_ARGS__);                         \
        d = load_sprite_image(databin, D, path, o);         \
        d->x = o[0];                                        \
        d->y = o[1] + building_sizes[i] * TILE_SIZE - o[3]; \
    }
        LOAD(&building_sprites[i].building, "buildings/%s/building.png",
             building_names[i]);

        LOAD(&building_sprites[i].marker, "buildings/%s/marker.png",
             building_names[i]);

        LOAD(&building_sprites[i].shadow, "buildings/%s/shadow.png",
             building_names[i]);
        d->color = SNOW_SHADOW_COLOR;

        LOAD(&building_sprites[i].sketch, "buildings/%s/building_sketch.png",
             building_names[i]);
        d->color = SKETCH_COLOR;

        LOAD(&building_sprites[i].sketch_shadow,
             "buildings/%s/shadow_sketch.png", building_names[i]);
        d->color = SKETCH_SNOW_SHADOW_COLOR;

        for (int j = 0; j < BUILDING_CONSTRUCTION_SPRITE_COUNT; j++) {
            LOAD(&building_sprites[i].construction[j].building,
                 "buildings/%s/building_construction%d.png", building_names[i],
                 j + 1);

            LOAD(&building_sprites[i].construction[j].shadow,
                 "buildings/%s/shadow_construction%d.png", building_names[i],
                 j + 1);
            d->color = SNOW_SHADOW_COLOR;
        }
        for (int j = 0; j < RESOURCE_TYPE_END; j++) {
            for (int k = 0; k < BUILDING_RESOURCE_OVERLAY_COUNT; k++) {
                LOAD(&building_sprites[i].overlays[j][k],
                     "buildings/%s/overlays/%s%d.png", building_names[i],
                     resource_names[j], k + 1);
            }
        }
#undef LOAD
    }
}

static void load_tree_sprites(FILE* databin) {
    // 0 is tree, 1 is stump
    const char* tree_names[] = {"tree", "stump"};

    for (int i = 0; i < TREE_TYPE_COUNT; i++) {
        for (int j = 0; j < 2; j++) {
            char full_path[512];
            char* path = strncpy(full_path, SB2_ASSET_DIR, 512);
            uint16_t o[4];
            snprintf(path, 128, "trees/%s_%d.png", tree_names[j], i + 1);
            lib2d_drawable* d =
                load_sprite_image(databin, &tree_sprites[i][j], path, o);
            // Store offset to sprite's intended base relative to the tile
            // center. When rendering, we'll copy the drawable and add the
            // position.
            d->x = o[0] - o[2] / 2 + TILE_SIZE / 2;
            d->y = o[1] - o[3] + TILE_SIZE / 2;

            snprintf(path, 128, "trees/%s_%d_sketch.png", tree_names[j], i + 1);
            d = load_sprite_image(databin, &tree_sketch_sprites[i][j], path, o);
            d->color = SKETCH_COLOR;
            d->x = o[0] - o[2] / 2 + TILE_SIZE / 2;
            d->y = o[1] - o[3] + TILE_SIZE / 2;
        }
    }
}

void sprites_init() {
#define DATA_PATH "data/2d/"
    FILE* databin = fopen(DATA_PATH "sprites.bin", "rb");
    assert(databin);

    for (int i = 0; i < 4; i++) {
        char p[32];
        snprintf(p, 32, DATA_PATH "sprites%d.png", i + 1);
        atlases[i] = lib2d_image_load(p, atlas_infos + i);
    }

    load_sprite_image(databin, &snow, DATA_PATH "data/terrain/snow.png", 0);
    load_sprite_image(databin, &snow_sketch,
                      DATA_PATH "data/terrain/snow_sketch.png", 0);
    snow_sketch.color = SKETCH_COLOR;
    load_sprite_image(databin, &snow_overlay, DATA_PATH "data/noise.png", 0);
    load_sprite_image(databin, &snowball, DATA_PATH "data/snowball.png", 0);

    snow_overlay.color = (SNOW_SHADOW_COLOR & 0xffffff00) | 0xf;
    load_tree_sprites(databin);
    load_building_sprites(databin);
    load_penguin_sprites(databin);

    // Make sure we've read everything
    char c = getc(databin);
    assert(c == EOF);
    fclose(databin);
}

void sprites_shutdown() {
    for (int i = 0; i < 4; i++) {
        lib2d_image_delete(atlases[i]);
    }
}

penguin_anim sprites_get_penguin_anim(int penguin_idx, const Map* map) {
    const Penguin* p = &map->penguins[penguin_idx];
    const ClientPenguin* c = &client_penguins[penguin_idx];

    switch (p->type) {
        case PENGUIN_TYPE_WORKER:
            if (c->magnitude < 1.f) {
                switch (p->job.type) {
                    case JOB_TYPE_HARVEST:
                        return worker_harvest_anims[p->job.harvest.resource];
                    case JOB_TYPE_UNLOAD:
                        // FIXME What if the penguin is standing still but not
                        // at the building?
                        if (map->buildings[p->job.unload.building].status ==
                            BUILDING_STATUS_CONSTRUCTING) {
                            return worker_unload_anims[RESOURCE_TYPE_END];

                        } else {
                            return worker_unload_anims[p->cargo];
                        }
                    default:
                        return worker_stand_anims[p->cargo];
                }
            } else {
                return worker_walk_anims[p->cargo];
            }
        case PENGUIN_TYPE_WARRIOR:
            if (c->magnitude < 1.f) {
                switch (p->job.type) {
                    case JOB_TYPE_AIM:
                        return warrior_throw_anim;
                    default:
                        return warrior_stand_anim;
                }
            } else {
                return warrior_walk_anim;
            }
        default:
            return warrior_stand_anim;
    }
}

void sprites_get_building_sprite(const Building* b, lib2d_drawable* building,
                                 lib2d_drawable* shadow,
                                 lib2d_drawable* marker) {
    struct building_sprite* sprite = &building_sprites[b->type];
    if (sketch) {
        *building = sprite->sketch;
        *shadow = sprite->sketch_shadow;
    } else if (b->status == BUILDING_STATUS_CONSTRUCTING) {
        int has = 0;
        int needs = 0;
        for (int i = 0; i < RESOURCE_TYPE_END; i++) {
            has += b->resources[i];
            needs += RULE_construct_building_cost[b->type][i];
        }
        int i = (float)has / needs * BUILDING_CONSTRUCTION_SPRITE_COUNT;
        *building = sprite->construction[i].building;
        *shadow = sprite->construction[i].shadow;
    } else {
        *building = sprite->building;
        *shadow = sprite->shadow;
    }
    *marker = sprite->marker;
}

lib2d_drawable sprites_get_building_overlay(const Building* b,
                                            enum resource_type type) {
    int overlay_num = MIN(
        b->resources[type] * BUILDING_RESOURCE_OVERLAY_COUNT / MAX_RESOURCES,
        BUILDING_RESOURCE_OVERLAY_COUNT - 1);

    assert(overlay_num >= 0 && overlay_num < BUILDING_RESOURCE_OVERLAY_COUNT);

    return building_sprites[b->type].overlays[type][overlay_num];
}

lib2d_drawable sprites_get_tree(int tree_type, int tree_row, int count) {
    assert(tree_type >= 0 && tree_type < TREE_TYPE_COUNT);
    return (sketch ? tree_sketch_sprites
                   : tree_sprites)[tree_type][tree_row >= count ? 1 : 0];
}
