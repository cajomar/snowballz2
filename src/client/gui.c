#include "gui.h"

#include <string.h>

#include "../cute_files.h"
#include "../map_loader.h"
#include "../prefs.h"
#include "../reader.h"
#include "idents.h"

static bool ends_with(const char* name, const char* suffix) {
    int n = strlen(name);
    int s = strlen(suffix);
    if (n < s) return false;
    return strcmp(suffix, name + (n - s)) == 0;
}

int load_map_metadata(const char* path, char* name, char* author, int* size,
                      int string_max) {
    memset(name, 0, string_max);
    memset(author, 0, string_max);
    FILE* fp = fopen(path, "rb");
    if (fp == NULL) {
        fprintf(stderr, "Failed to open file: %s\n", path);
        return -1;
    }

    int version = fp_i32(fp);
    if (version != MAP_BINARY_VERSION) {
        fprintf(stderr, "Invalid map binary version: %d (expected %d)\n",
                version, MAP_BINARY_VERSION);
        fclose(fp);
        return -1;
    }
    int name_len = fp_i32(fp);
    if (name_len > string_max - 1) {
        fprintf(stderr, "Map name too long: %d\n", name_len);
        fclose(fp);
        return -1;
    }
    fread(name, 1, name_len, fp);
    int author_len = fp_i32(fp);
    if (author_len > string_max - 1) {
        fprintf(stderr, "Map author name too long: %d\n", author_len);
        fclose(fp);
        return -1;
    }
    fread(author, 1, author_len, fp);
    *size = fp_i32(fp);
    fclose(fp);
    return 0;
}

void load_map_list(const char* path, inkd source) {
    cf_dir_t dir;
    cf_dir_open(&dir, path);
    while (dir.has_next) {
        cf_file_t file;
        cf_read_file(&dir, &file);
        if (ends_with(file.name, ".sb2")) {
            char name[256];
            char author[256];
            int size;
            if (load_map_metadata(file.path, name, author, &size, 256) == 0) {
                inkd d = inkd_append(source, I_maps);
                inkd_charp(d, I_name, name);
                inkd_charp(d, I_author, author);
                inkd_charp(d, I_path, file.path);
                inkd_charp(d, I_file_name, file.name);
                inkd_int(d, I_size, size);
            }
        }
        cf_dir_next(&dir);
    }
    cf_dir_close(&dir);
}
