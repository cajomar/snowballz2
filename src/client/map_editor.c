#include "map_editor.h"

#include <assert.h>
#include <ink_data.h>
#include <ink_query.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../map.h"
#include "../map_loader.h"
#include "../map_saver.h"
#include "../prefs.h"
#include "../stretchy_buffer.h"
#include "../utils.h"
#include "gui.h"
#include "idents.h"
#include "minimap.h"
#include "render.h"

static inkd_listener* listener = 0;
static Map* editing_map = 0;

extern struct ink* ink;
extern int viewport[4];
extern const int building_sizes[BUILDING_TYPE_END];

void map_editor_init() {
    if (listener) return;
    listener = inkd_listener_new(0);
    inkd source = ink_source(ink, I_map_editor);
    inkd_add_listener(source, listener);
    load_map_list(prefs_map_dir(), source);
    game_minimap_init();
}

static void close_editing_map() {
    if (editing_map) {
        map_destroy(editing_map);
        editing_map = 0;
    }
}

void map_editor_shutdown() {
    if (!listener) return;
    close_editing_map();
    inkd_clear_all(ink_source(ink, I_map_editor));
    inkd_listener_delete(listener);
    listener = 0;
    game_minimap_shutdown();
}

static void create_new_map(inkd source) {
    close_editing_map();

    const char* name = inkd_get_charp(source, I_name);
    const char* author = inkd_get_charp(source, I_author);

    int size = inkd_get_int(source, I_size);
    size = MAX(64, MIN(size, 512));
    Map* m = map_create(size, strdup(name), strdup(author));

    save_map(m, prefs_map_dir());
    editing_map = m;
    inkd_bool(ink_source(ink, I_map_editor), I_map_loaded, true);
}

static void open_map(inkd data) {
    close_editing_map();
    const char* path = inkd_get_charp(data, I_path);
    editing_map = load_map(path);
    inkd_bool(ink_source(ink, I_map_editor), I_map_loaded, true);
}

static bool valid_tile(Map* map, int x, int y) {
    bool out_of_bounds = (x < 0 || x >= map->size || y < 0 || y >= map->size);
    return !out_of_bounds;
}

enum brush_types {
    BRUSH_TYPE_tree = 0,
    BRUSH_TYPE_igloo = 1,
    BRUSH_TYPE_colony = 2,
};

static bool handle_mouse(int mouse_x, int mouse_y, bool placing) {
    inkd source = ink_source(ink, I_map_editor);
    int brush_size = inkd_get_int(source, I_brush_size);
    enum brush_types brush_type = inkd_get_int(source, I_brush_type);

    int x, y;
    render_screen_to_map_pixel(mouse_x, mouse_y, &x, &y);
    map_pixel_to_tile(editing_map, x, y, &x, &y);

    if (brush_type == BRUSH_TYPE_tree) {
        for (int dy = -brush_size; dy <= brush_size; dy++) {
            for (int dx = -brush_size; dx <= brush_size; dx++) {
                if (dx * dx + dy * dy > brush_size * brush_size + 1) continue;
                int tx = x + dx;
                int ty = y + dy;
                if (valid_tile(editing_map, tx, ty)) {
                    if (placing) {
                        // TODO check for conflicting buildings and penguins
                        editing_map->tile_types[ty * editing_map->size + tx] =
                            MAP_TILE_TYPE_TREE;
                        editing_map
                            ->tile_resource_counts[ty * editing_map->size +
                                                   tx] = 3;
                    } else {
                        editing_map->tile_types[ty * editing_map->size + tx] =
                            MAP_TILE_TYPE_EMPTY;
                    }
                }
            }
        }
    } else {
        if (placing) {
            enum building_type type;
            if (brush_type == BRUSH_TYPE_igloo) {
                type = BUILDING_TYPE_IGLOO;
            } else if (brush_type == BRUSH_TYPE_colony) {
                type = BUILDING_TYPE_COLONY;
            } else {
                assert(false);
                return true;
            }
            if (!map_can_place_building_at(editing_map, type, x, y)) {
                return true;
            }
            int building_id = map_building_add(editing_map, type, x, y);
            Building* b = &editing_map->buildings[building_id];
            // For some reason if these are stringed up b becomes 0x0
            // Building* b =
            // &editing_map->buildings[map_building_add(editing_map, type, x,
            // y)];
            b->owner_id = PLAYER_NEUTRAL;
            editing_map->views
                .building_draw_order[b->tile_y * editing_map->size +
                                     b->tile_x] = building_id;
        } else {
            sbforeachp (Building* b, editing_map->buildings) {
                if (b->tile_x == x && b->tile_y == y) {
                    map_building_destroy(editing_map, stb__counter);
                    break;
                }
            }
        }
    }
    return true;
}

static bool keyboard_move[4];
#define SCROLL_SPEED 2200

static void handle_map_scroll(float dt) {
    // TODO only allow edge scrolling when fullscreened
    /*
    int mouse_x, mouse_y;
    SDL_GetMouseState(&mouse_x, &mouse_y);
    if (mouse_x > viewport[2] - 40)
        viewport[0] += SCROLL_SPEED*dt;
    else if (mouse_x < 40)
        viewport[0] -= SCROLL_SPEED*dt;
    if (mouse_y > viewport[3] - 40)
        viewport[1] += SCROLL_SPEED*dt;
    else if (mouse_y < 40)
        viewport[1] -= SCROLL_SPEED*dt;
    */

    if (keyboard_move[0])
        viewport[0] -= SCROLL_SPEED * dt;
    else if (keyboard_move[2])
        viewport[0] += SCROLL_SPEED * dt;
    if (keyboard_move[1])
        viewport[1] -= SCROLL_SPEED * dt;
    else if (keyboard_move[3])
        viewport[1] += SCROLL_SPEED * dt;

    // Clamp viewport to map
    int padding = 300;
    if (viewport[0] < -padding) viewport[0] = -padding;
    if (viewport[1] < -padding) viewport[1] = -padding;
    int s = editing_map->size * TILE_SIZE + padding;
    if (viewport[0] > s - viewport[2]) viewport[0] = s - viewport[2];
    if (viewport[1] > s - viewport[3]) viewport[1] = s - viewport[3];
}

bool map_editor_event(SDL_Event* ev) {
    if (!listener) return false;
    if (!editing_map) return false;

    if (ev->type == SDL_MOUSEBUTTONDOWN) {
        return handle_mouse(ev->button.x, ev->button.y,
                            ev->button.button == SDL_BUTTON_LEFT);
    } else if (ev->type == SDL_MOUSEMOTION) {
        if ((ev->motion.state & SDL_BUTTON_LMASK) ||
            (ev->motion.state & SDL_BUTTON_RMASK)) {
            return handle_mouse(ev->motion.x, ev->motion.y,
                                ev->motion.state & SDL_BUTTON_LMASK);
        }
    } else if (ev->type == SDL_KEYDOWN || ev->type == SDL_KEYUP) {
        SDL_Keycode c = ev->key.keysym.sym;
        int d = 0;
        if (c == SDLK_w)
            d = 1;
        else if (c == SDLK_a)
            d = 0;
        else if (c == SDLK_s)
            d = 3;
        else if (c == SDLK_d)
            d = 2;
        else
            return false;
        if (ev->type == SDL_KEYDOWN) {
            keyboard_move[d] = true;
        } else {
            keyboard_move[d] = false;
        }
        return true;
    }

    return false;
}

void map_editor_step(float dt) {
    if (!listener) return;
    inkd source = ink_source(ink, I_map_editor);

    struct ink_data_event ev;
    while (inkd_listener_poll(listener, &ev)) {
        switch (ev.type) {
            case INK_DATA_EVENT_IMPULSE:
                switch (ev.field) {
                    case I_create:
                        create_new_map(ev.source_data);
                        break;
                    case I_save:
                        if (editing_map != NULL) {
                            save_map(editing_map, prefs_map_dir());
                        }
                        break;
                    case I_open:
                        open_map(ev.data);
                        break;
                }
                break;
            case INK_DATA_EVENT_CHANGE:
                if (ev.field == I_spawn) {
                    int building = inkd_get_int(ev.data, I_id);
                    int spawn = inkd_get_bool(ev.data, I_spawn);
                    if (editing_map) {
                        editing_map->buildings[building].owner_id =
                            spawn ? PLAYER_SPAWNPOINT : PLAYER_NEUTRAL;
                    }
                }
                break;
            default:
                break;
        }
    }

    if (editing_map != 0) {
        handle_map_scroll(dt);
        game_minimap_step(editing_map);
        render_step(editing_map, dt);

        inkd_rebuild_begin(source, I_buildings);
        ink_query q;
        ink_query_init(&q);
        sbforeachp (Building* b, editing_map->buildings) {
            ink_query_int(&q, I_id, stb__counter);
            inkd d = ink_query_get(&q, source, I_buildings);
            inkd_int(d, I_x, b->tile_x * TILE_SIZE - viewport[0]);
            inkd_int(d, I_y, b->tile_y * TILE_SIZE - viewport[1]);
            inkd_int(d, I_size, TILE_SIZE * building_sizes[b->type]);
            inkd_bool(d, I_spawn_allowed, b->type == BUILDING_TYPE_COLONY);
            inkd_bool(d, I_spawn, b->owner_id == PLAYER_SPAWNPOINT);
        }
        ink_query_deinit(&q);
        inkd_rebuild_end(source, I_buildings);
    }
}

bool map_editor_render() {
    if (editing_map) {
        render(editing_map);
        return true;
    }
    return false;
}
