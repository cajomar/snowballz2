#include "minimap.h"

#include <ink_data.h>

#include "../stretchy_buffer.h"
#include "../utils.h"
#include "idents.h"

extern struct ink* ink;
extern int viewport[4];

extern const int building_sizes[BUILDING_TYPE_END];

extern int local_player_id;
extern uint32_t PLAYER_COLORS[4];

static inkd_listener* listener = 0;
struct ink_image* image = 0;

void game_minimap_init() {
    if (listener) return;
    listener = inkd_listener_new(0);
    inkd source = ink_source(ink, I_minimap);
    inkd_add_listener(source, listener);
    image = ink_image_new(ink);
    inkd_image(ink_source(ink, I_minimap), I_minimap, image);
}

void game_minimap_shutdown() {
    if (!listener) return;
    inkd_listener_delete(listener);
    listener = 0;
}

static void set_color(uint8_t* pixel, uint32_t color) {
    pixel[0] = (color >> 24) & 0xff;
    pixel[1] = (color >> 16) & 0xff;
    pixel[2] = (color >> 8) & 0xff;
}

static void update_image(const Map* map) {
    uint8_t* data = malloc(map->size * map->size * 3);

    for (int y = 0; y < map->size; y++) {
        for (int x = 0; x < map->size; x++) {
            int i = y * map->size + x;
            if (map->tile_types[i] == MAP_TILE_TYPE_TREE) {
                set_color(data + i * 3, 0x2fb735ff);
            } else {
                set_color(data + i * 3, 0xffffffff);
            }
        }
    }
    sbforeachp (Building* b, map->buildings) {
        if (b->type == BUILDING_TYPE_END) continue;
        for (int y = 0; y < building_sizes[b->type]; y++) {
            for (int x = 0; x < building_sizes[b->type]; x++) {
                int i = (y + b->tile_y) * map->size + x + b->tile_x;
                set_color(data + i * 3,
                          PLAYER_COLORS[MAX(PLAYER_NEUTRAL, b->owner_id)]);
            }
        }
    }
    sbforeachp (Penguin* p, map->penguins) {
        if (p->type == PENGUIN_TYPE_END) continue;
        int x = p->x / TILE_SIZE;
        int y = p->y / TILE_SIZE;
        set_color(data + (y * map->size + x) * 3, PLAYER_COLORS[p->owner_id]);
    }
    ink_image_setFromData(ink, image, map->size, map->size,
                          INK_IMAGE_FORMAT_RGB_888, data, INK_IMAGE_NO_ATLAS);
    free(data);
}

void game_minimap_step(const Map* map) {
    inkd source = ink_source(ink, I_minimap);
    float map_pixel_size = map->size * TILE_SIZE;

    static int steps = 0;
    steps--;
    if (steps < 0) {
        // inkd_image(source, I_image, get_new_image(map));
        update_image(map);
        steps = 30;
    }

    struct ink_data_event ev;
    while (inkd_listener_poll(listener, &ev)) {
        if (ev.type == INK_DATA_EVENT_CHANGE) {
            if (ev.field == I_vp0) {
                viewport[0] =
                    inkd_get_float(ev.data, ev.field) * map_pixel_size -
                    viewport[2] / 2;
            } else if (ev.field == I_vp1) {
                viewport[1] =
                    inkd_get_float(ev.data, ev.field) * map_pixel_size -
                    viewport[3] / 2;
            }
        }
    }
    inkd_float(source, I_vp0,
               (viewport[0] + viewport[2] / 2.f) / map_pixel_size);
    inkd_float(source, I_vp1,
               (viewport[1] + viewport[3] / 2.f) / map_pixel_size);
    inkd_float(source, I_vp2, viewport[2] / map_pixel_size);
    inkd_float(source, I_vp3, viewport[3] / map_pixel_size);
}
