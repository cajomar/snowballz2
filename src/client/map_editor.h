#ifndef __SB2_MAP_EDITOR__
#define __SB2_MAP_EDITOR__

#include <stdbool.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_events.h>

void map_editor_init();
void map_editor_shutdown();
void map_editor_step(float dt);
bool map_editor_event(SDL_Event* ev);
bool map_editor_render(void);

#endif
