#ifndef __SB2_MAP_EDITOR__
#define __SB2_MAP_EDITOR__

#include <SDL2/SDL.h>
#include <SDL2/SDL_events.h>
#include <stdbool.h>

void map_editor_init(void);
void map_editor_shutdown(void);
void map_editor_step(float dt);
bool map_editor_event(SDL_Event* ev);
bool map_editor_render(void);

#endif
