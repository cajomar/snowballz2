#ifndef __SB2_GAME__
#define __SB2_GAME__

#include <ink_data.h>

void game_step(float dt);
void game_start(const char* host_map, const char* host_ip, const char* client_name, inkd launch_from);
void game_end(void);

#endif
