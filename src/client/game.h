#ifndef __SB2_GAME__
#define __SB2_GAME__

#include <stdbool.h>

void game_step(float dt);
bool game_start(const char* host_map, const char* host_ip,
                const char* client_name, bool allow_client_connections);
void game_end(void);

#endif
