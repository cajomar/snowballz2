#include <assert.h>
#include <stdio.h>

#define SDL_MAIN_HANDLED
#include <SDL2/SDL.h>
#include <SDL2/SDL_events.h>
#include <enet/enet.h>
#include <ink.h>
#include <ink_data.h>
#include <ink_experimental.h>

#include "../prefs.h"
#include "event_layer.h"
#include "game.h"
#include "game_client.h"
#include "idents.h"
#include "lobby.h"
#include "map_editor.h"
#include "render.h"

void ink_clear_framebuffer(struct ink* ink);

static bool running;

// Globals
struct ink* ink = 0;
int viewport[4];

// Locals
static inkd_listener* main_listener = 0;

static void step(float dt) {
    struct ink_data_event ev;
    while (inkd_listener_poll(main_listener, &ev)) {
        switch (ev.type) {
            case INK_DATA_EVENT_IMPULSE:
                switch (ev.field) {
                    case I_quit:
                        running = false;
                        break;
                    case I_reload_graphics:
                        render_shutdown();
                        render_init();
                        break;
                    case I_host_game:
                    case I_start_game: {
                        inkd data =
                            ink_data_event_get_meta_hold_EXP(&ev, I_start_game);
                        const char* path = inkd_get_charp(data, I_path);
                        game_start(path, NULL, NULL, ev.field == I_host_game);
                        inkd_release(data);
                        break;
                    }
                    case I_connect: {
                        inkd data =
                            ink_data_event_get_meta_hold_EXP(&ev, I_start_game);
                        const char* host =
                            inkd_get_charp(data, ink_ident_hash("host"));
                        const char* name = inkd_get_charp(data, I_name);
                        game_start(NULL, host, name, false);
                        inkd_release(data);
                        break;
                    }
                    case I_create_game:
                        lobby_init(prefs_map_dir());
                        inkd_bool(ev.data, I_lobby, true);
                        break;
                    case I_load_game:
                        lobby_init(prefs_save_dir());
                        inkd_bool(ev.data, I_lobby, true);
                        break;
                    default:
                        fprintf(stderr,
                                "Error: listener on %s didn't expect impulse "
                                "on field %s\n",
                                ink_ident_as_char(ev.source_name),
                                ink_ident_as_char(ev.field));
                }
                break;
            case INK_DATA_EVENT_CHANGE:
                if (ev.field == I_map_editor) {
                    if (inkd_get_bool(ev.data, ev.field)) {
                        map_editor_init();
                    } else {
                        map_editor_shutdown();
                    }
                } else if (ev.field == I_lobby) {
                    if (inkd_get_bool(ev.data, ev.field)) {
                        fprintf(stderr,
                                "Error: lobby shouldn't be opened by GUI!\n");
                        assert(false);
                    } else {
                        lobby_shutdown();
                    }
                }
                break;
            default:
                break;
        }
    }
    map_editor_step(dt);
    lobby_step();
    game_step(dt);
}

int main(int argc, const char** argv) {
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        fprintf(stderr, "SDL_Init failed: %s\n", SDL_GetError());
        return -1;
    }

    viewport[0] = 0;
    viewport[1] = 0;
    viewport[2] = 800;
    viewport[3] = 600;

    SDL_DisplayMode DM;
    if (SDL_GetCurrentDisplayMode(0, &DM) == 0) {
        viewport[2] = DM.w;
        viewport[3] = DM.h;
    }

    int window_flags = SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE;

    if (argc > 1) {
        for (int i = 1; i < argc; i++) {
            if (strcmp(argv[i], "-f") == 0) {
                window_flags |= SDL_WINDOW_FULLSCREEN;
                break;
            }
        }
    }

#ifdef GLES
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
#else
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
                        SDL_GL_CONTEXT_PROFILE_CORE);
#endif
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);

    SDL_Window* win = SDL_CreateWindow("Snowballz 2.0", SDL_WINDOWPOS_UNDEFINED,
                                       SDL_WINDOWPOS_UNDEFINED, viewport[2],
                                       viewport[3], window_flags);
    if (win == 0) {
        fprintf(stderr, "SDL_CreateWindow failed: %s\n", SDL_GetError());
        return -1;
    }

    SDL_GLContext ctx = SDL_GL_CreateContext(win);
    if (ctx == 0) {
        fprintf(stderr, "SDL_GL_CreateContext failed: %s\n", SDL_GetError());
        return -1;
    }
    SDL_GL_MakeCurrent(win, ctx);
    SDL_GL_SetSwapInterval(1);

    ink = ink_new(0);
    main_listener = inkd_listener_new(0);
    inkd_add_listener(ink_source(ink, I_main), main_listener);
    inkd_add_listener(ink_source(ink, I_debug_dialog), main_listener);

    if (ink_init_renderer(ink, INK_RENDERER_API_GL, NULL) != 0) {
        fprintf(stderr, "ink renderer init failure.\n");
        return -1;
    }
    {
        char path[512];
        prefs_asset_file(path, 512, "gui_assets.bin");
        ink_load_assets_bin(ink, path);

        prefs_asset_file(path, 512, "gui.bin");
        ink_load_gui_bin(ink, path);
    }
    ink_net_listen(ink, "ipc://gui/runner.ipc");
    ink_instantiate_main_template(ink);
    ink_setViewport(ink, viewport[2], viewport[3]);

    if (enet_initialize() != 0) {
        fprintf(stderr, "ENet initialization failed.\n");
        return -1;
    }

    render_init();

    const char* name = "Player";
    const char* host_map = 0;
    const char* connect = 0;

    for (int i = 1; i < argc - 1; i++) {
        if (strcmp(argv[i], "-n") == 0) {
            name = argv[i + 1];
            i++;
        } else if (strcmp(argv[i], "-c") == 0) {
            connect = argv[i + 1];
            i++;
        } else if (strcmp(argv[i], "-h") == 0) {
            host_map = argv[i + 1];
            i++;
        }
    }

    if (connect) {
        game_start(NULL, connect, name, false);
    } else if (host_map) {
        // If the filename passed as an arg doesn't exist, treat it as a map
        // name instead of a file name.
        bool started = game_start(host_map, NULL, name, false);
        if (!started) {
            char new_host_map[2048];
            prefs_map_file(new_host_map, 2048, host_map);
            game_start(new_host_map, NULL, name, false);
        }
    }

    uint32_t frame_start = 0;
    running = true;
    while (running) {
        SDL_Event event;
        while (SDL_PollEvent(&event)) {
            switch (event.type) {
                case SDL_QUIT:
                    running = false;
                    break;
                case SDL_WINDOWEVENT:
                    switch (event.window.event) {
                        case SDL_WINDOWEVENT_SIZE_CHANGED:
                            viewport[2] = event.window.data1;
                            viewport[3] = event.window.data2;
                            ink_setViewport(ink, viewport[2], viewport[3]);
                            break;
                    }
                    break;
            }
            if (event.type == SDL_KEYDOWN &&
                event.key.keysym.sym == SDLK_BACKQUOTE) {
                inkd source = ink_source(ink, I_main);
                inkd_bool(source, I_debug_dialog,
                          !inkd_get_bool(source, I_debug_dialog));
            }
            if (!handle_ink_events(ink, event) ||
                event.type == SDL_MOUSEBUTTONUP) {
                if (game_client_event(&event)) continue;
                if (map_editor_event(&event)) continue;
            }
        }
        uint32_t now = SDL_GetTicks();
        uint32_t dt = now - frame_start;
        if (dt < 1) {
            SDL_Delay(1 - dt);
            dt += 1 - dt;
        }
        frame_start = now;
        step(dt * 0.001f);
        ink_step(ink, dt * 0.001f);

        if (game_client_render())
            ;
        else if (map_editor_render())
            ;
        else
            ink_clear_framebuffer(ink);

        ink_render(ink);
        SDL_GL_SwapWindow(win);
    }
    render_shutdown();
    map_editor_shutdown();
    lobby_shutdown();
    inkd_listener_delete(main_listener);
    ink_delete(ink);
    ink_free_static_globals();
    SDL_DestroyWindow(win);
    SDL_Quit();
    enet_deinitialize();
    return 0;
}
