#include "event_layer.h"

static
enum ink_pointer_type
sdl_button_to_ink(int button) {
	switch (button) {
	case SDL_BUTTON_LEFT:
		return INK_MOUSE_LEFT;
	case SDL_BUTTON_RIGHT:
		return INK_MOUSE_RIGHT;
	case SDL_BUTTON_MIDDLE:
		return INK_MOUSE_MIDDLE;
	default:
		return INK_MOUSE_X1;
	}
}



static
int
handle_key(struct ink* ink, SDL_Event event, enum ink_key_state state) {
    struct ink_event ev;
    ev.event_type = INK_EVENT_TYPE_ACTION;
    ev.data.action.key_state = state;

    switch (event.key.keysym.sym) {
    case SDLK_TAB:
        if (state != INK_KEY_UP) {
            bool backwards = event.key.keysym.mod&(KMOD_LSHIFT | KMOD_RSHIFT);
            ink_focus_next(ink, backwards);
            return true;
        }
        return false;
    case SDLK_BACKSPACE:
        ev.data.action.action = INK_EVENT_ACTION_K_BACKSPACE;
        break;
    case SDLK_HOME:
        ev.data.action.action = INK_EVENT_ACTION_K_HOME;
        break;
    case SDLK_END:
        ev.data.action.action = INK_EVENT_ACTION_K_END;
        break;
    case SDLK_DELETE:
        ev.data.action.action = INK_EVENT_ACTION_K_DELETE;
        break;
    case SDLK_ESCAPE:
        ev.data.action.action = INK_EVENT_ACTION_K_ESCAPE;
        break;
    case SDLK_RETURN:
        ev.data.action.action = INK_EVENT_ACTION_K_RETURN;
        break;
    case SDLK_LEFT:
        ev.data.action.action = INK_EVENT_ACTION_NAVIGATE_LEFT;
        break;
    case SDLK_RIGHT:
        ev.data.action.action = INK_EVENT_ACTION_NAVIGATE_RIGHT;
        break;
    case SDLK_DOWN:
        ev.data.action.action = INK_EVENT_ACTION_NAVIGATE_DOWN;
        break;
    case SDLK_UP:
        ev.data.action.action = INK_EVENT_ACTION_NAVIGATE_UP;
        break;
    case SDLK_LCTRL:
        ev.data.action.action = INK_EVENT_ACTION_K_LCTRL;
        break;
    case SDLK_RCTRL:
        ev.data.action.action = INK_EVENT_ACTION_K_RCTRL;
        break;
    case SDLK_LSHIFT:
        ev.data.action.action = INK_EVENT_ACTION_K_LSHIFT;
        break;
    case SDLK_RSHIFT:
        ev.data.action.action = INK_EVENT_ACTION_K_RSHIFT;
        break;
    default:
        return false;
    }

    return ink_event_feed_event(ink, &ev);
}

static
bool
window_shift_held() {
    const Uint8 *keys = SDL_GetKeyboardState(NULL);
    return keys[SDL_SCANCODE_LSHIFT] | keys[SDL_SCANCODE_RSHIFT];
}
static
bool
window_control_held() {
    const Uint8 *keys = SDL_GetKeyboardState(NULL);
    return keys[SDL_SCANCODE_LCTRL] | keys[SDL_SCANCODE_RCTRL];
}
static
bool
window_alt_held() {
    const Uint8 *keys = SDL_GetKeyboardState(NULL);
    return keys[SDL_SCANCODE_LALT] | keys[SDL_SCANCODE_RALT];
}
static
bool
window_super_held() {
    const Uint8 *keys = SDL_GetKeyboardState(NULL);
    return keys[SDL_SCANCODE_APPLICATION];
}



bool
handle_ink_events(struct ink* ink, SDL_Event event) {
    bool handled = false;
    switch (event.type) {
        case SDL_MOUSEBUTTONDOWN: {
            struct ink_event ev;
            ev.event_type = INK_EVENT_TYPE_POINTER;
            ev.data.pointer.pointer_id = 0;
            ev.data.pointer.x = event.button.x;
            ev.data.pointer.y = event.button.y;
            ev.data.pointer.pointer = sdl_button_to_ink(event.button.button);
            ev.data.pointer.action = INK_POINTER_DOWN;
            handled = ink_event_feed_event(ink, &ev);
            break;
        }
        case SDL_MOUSEBUTTONUP: {
            struct ink_event ev;
            ev.event_type = INK_EVENT_TYPE_POINTER;
            ev.data.pointer.pointer_id = 0;
            ev.data.pointer.x = event.button.x;
            ev.data.pointer.y = event.button.y;
            ev.data.pointer.pointer = sdl_button_to_ink(event.button.button);
            ev.data.pointer.action = INK_POINTER_UP;
            handled = ink_event_feed_event(ink, &ev);
            handled = true;
            break;
        }
        case SDL_MOUSEMOTION: {
            struct ink_event ev;
            ev.event_type = INK_EVENT_TYPE_POINTER;
            ev.data.pointer.pointer_id = 0;
            ev.data.pointer.x = event.button.x;
            ev.data.pointer.y = event.button.y;
            ev.data.pointer.pointer = sdl_button_to_ink(event.button.button);
            ev.data.pointer.action = INK_POINTER_MOTION;
            handled = ink_event_feed_event(ink, &ev);
            break;
        }
        case SDL_TEXTINPUT:
        {
            struct ink_event ev;
            if (event.text.text[0] == 'z' && window_control_held(NULL)) {
                ev.event_type = INK_EVENT_TYPE_ACTION;
                ev.data.action.key_state = INK_KEY_DOWN;
                ev.data.action.action = INK_EVENT_ACTION_UNDO;
                handled = ink_event_feed_event(ink, &ev);
            } else if ((event.text.text[0] == 'y' || event.text.text[0] == 'r') && window_control_held(NULL)) {
                ev.event_type = INK_EVENT_TYPE_ACTION;
                ev.data.action.key_state = INK_KEY_DOWN;
                ev.data.action.action = INK_EVENT_ACTION_REDO;
                handled = ink_event_feed_event(ink, &ev);
            } else {
                ev.event_type = INK_EVENT_TYPE_TEXT;
                ev.data.text.text = ink_str_empty();
                ink_str_assign_hold(&ev.data.text.text, ink_str_from_copy(event.text.text, 1));
                ev.data.text.key_state = INK_TEXT_INPUT;
                ev.data.text.shift = window_shift_held(NULL);
                ev.data.text.ctrl = window_control_held(NULL);
                ev.data.text.super = window_super_held(NULL);
                ev.data.text.alt = window_alt_held(NULL);
                handled = ink_event_feed_event(ink, &ev);
                ink_str_assign_hold(&ev.data.text.text, ink_str_empty());
            }
            break;
        }
        case SDL_KEYDOWN:
            handled = handle_key(ink, event, INK_KEY_DOWN);
            switch (event.key.keysym.sym) {
                case SDLK_LSHIFT:
                case SDLK_RSHIFT:
                    ink_event_select_on_navigate(ink, window_shift_held(NULL));
                break;
            }
            if (!handled) {
                switch (event.key.keysym.sym) {
                    case SDLK_LEFT:
                        ink_focus_next(ink, INK_DIRECTION_LEFT);
                        break;
                    case SDLK_RIGHT:
                        ink_focus_next(ink, INK_DIRECTION_RIGHT);
                        break;
                    case SDLK_UP:
                        ink_focus_next(ink, INK_DIRECTION_UP);
                        break;
                    case SDLK_DOWN:
                        ink_focus_next(ink, INK_DIRECTION_DOWN);
                        break;

                    case SDLK_ESCAPE:
                        ink_focus_clear(ink);
                        break;
                    case SDLK_RETURN:
                        handled = ink_focus_activate(ink);
                        break;
                }
            }
            break;
        case SDL_KEYUP:
            handled = handle_key(ink, event, INK_KEY_UP);
            switch (event.key.keysym.sym) {
                case SDLK_LSHIFT:
                case SDLK_RSHIFT:
                    ink_event_select_on_navigate(ink, window_shift_held(NULL));
                break;
            }
            break;
        case SDL_MOUSEWHEEL: {
            float tx = event.wheel.x;
            float ty = event.wheel.y;
            if (SDL_GetModState()&(KMOD_LCTRL | KMOD_RCTRL)) {
                float f = tx;
                tx = ty, ty = f;
            }
            struct ink_event ev;
            ev.event_type = INK_EVENT_TYPE_SCROLL;
            ev.data.scroll.dx = tx * 40;
            ev.data.scroll.dy = ty * 40;
            ev.data.scroll.pointer_id = 0;
            handled = ink_event_feed_event(ink, &ev);
        }
        default:
            break;
    }
    return handled;
}
