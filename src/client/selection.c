// Handles selecting things
// This is a sub-module of game.c, and will only be initialized while game is
// initialized.

#include "selection.h"

#include <ink_data.h>
#include <ink_experimental.h>
#include <ink_query.h>

#include "../stretchy_buffer.h"
#include "../utils.h"
#include "game_client.h"
#include "idents.h"
#include "render.h"

// Globals
extern struct ink* ink;
extern int viewport[4];

// Managed by game_client.c
extern ClientPenguin* client_penguins;
extern int local_player_id;

// Select box globals
static bool dragging = false;
static Rect drag;
static inkd_listener* listener = 0;

void game_selection_init() {
    if (listener) return;
    dragging = false;
    listener = inkd_listener_new(0);
    inkd_add_listener(ink_source(ink, I_selection), listener);
}

void game_selection_shutdown() {
    if (!listener) return;
    inkd_listener_delete(listener);
    listener = 0;
}

static void deselect_type(enum penguin_type type, const Penguin* penguins) {
    sbforeachp (ClientPenguin* c, client_penguins) {
        if (penguins[stb__counter].type == type) c->selected = false;
    }
}

void game_selection_step(const Map* map) {
    inkd select_box = ink_source(ink, I_select_box);
    inkd_bool(select_box, I_selecting, dragging);
    if (dragging) {
        int mouse_x, mouse_y;
        SDL_GetMouseState(&mouse_x, &mouse_y);
        drag.r = mouse_x + viewport[0];
        drag.b = mouse_y + viewport[1];

        Rect r;
        rect_normalize(&r, &drag);
        inkd_int(select_box, I_x, r.l - viewport[0]);
        inkd_int(select_box, I_y, r.t - viewport[1]);
        inkd_int(select_box, I_w, r.r - r.l);
        inkd_int(select_box, I_h, r.b - r.t);
    }
    struct ink_data_event ev;
    while (inkd_listener_poll(listener, &ev)) {
        if (ev.field == I_deselect_penguins) {
            inkd data = ink_data_event_get_meta_hold_EXP(&ev, I_type);
            enum penguin_type type = inkd_get_int(data, I_type);
            for (enum penguin_type i = 0; i < PENGUIN_TYPE_END; i++) {
                if (i != type) {
                    deselect_type(i, map->penguins);
                }
            }
        }
    }
}

static void select_game_elements(const Map* map) {
    rect_normalize_inplace(&drag);
    sbforeachp (const Penguin* p, map->penguins) {
        ClientPenguin* c = &client_penguins[stb__counter];
        c->selected = p->owner_id == local_player_id &&
                      rect_contains(&drag, c->draw_x + viewport[0],
                                    c->draw_y + viewport[1]);
    }
}

static void deselect_all() {
    sbforeachp (ClientPenguin* c, client_penguins) {
        c->selected = false;
    }
}

bool game_selection_event(const Map* map, SDL_Event* ev) {
    bool captured = false;
    if (ev->type == SDL_MOUSEBUTTONDOWN) {
        deselect_all();
        if (ev->button.button == SDL_BUTTON_LEFT) {
            drag.l = ev->button.x + viewport[0];
            drag.t = ev->button.y + viewport[1];
            dragging = true;
            captured = true;
        }
    } else if (ev->type == SDL_MOUSEBUTTONUP) {
        if (dragging) {
            select_game_elements(map);
            dragging = false;
        }
        captured = true;
    }

    return captured;
}
