#ifndef SB2_SRC_RENDER_DEBUG_H_WBZH5CWI
#define SB2_SRC_RENDER_DEBUG_H_WBZH5CWI

#include "../map.h"

void render_debug(const Map* map);

#endif /* SB2_SRC_RENDER_DEBUG_H_WBZH5CWI */
