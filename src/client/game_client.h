#ifndef __SB2_GAME_CLIENT__
#define __SB2_GAME_CLIENT__
#include <SDL2/SDL.h>
#include <SDL2/SDL_events.h>
#include <stdbool.h>

struct i_writer;

typedef struct client_penguin {
    // Position of the penguin smoothed out between updates in map pixels
    float lerp_x, lerp_y;
    // Position of the penguin inside the screen. In a 2D render this would be
    // draw_x = lerp_x - viewport[0];
    float draw_x, draw_y;
    float magnitude;
    float direction;  // In radians
    bool selected;
} ClientPenguin;

typedef struct map Map;
Map* game_client_get_map(void);
void game_client(const char* host, const char* player_name);
void game_client_step(float dt);
void game_client_shutdown(void);
bool game_client_event(SDL_Event* ev);
bool game_client_render(void);
void game_client_command(struct i_writer);
void game_client_start(void);

#endif
