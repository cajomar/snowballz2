#ifndef __SB2_CLIENT_RENDER__
#define __SB2_CLIENT_RENDER__

#include "../map.h"
#include "../mat.h"

void render_init(void);
void render_shutdown(void);
void render_step(const Map* map, float dt);
void render_screen_to_map_pixel(int x, int y, int* map_x, int* map_y);
void render_world_space_to_screen(const vec3 world, float* x, float* y);
void render(const Map* map);

#endif
