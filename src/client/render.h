#ifndef __SB2_CLIENT_RENDER__
#define __SB2_CLIENT_RENDER__

#include "../map.h"
#include "../mat.h"

void render_init(void);
void render_shutdown(void);
void render_step(const Map* map, float dt);
/*
 * Unprojects screen pixels to map pixels, where one map tile is `TILE_SIZE`
 * pixels.
 * Does not check if map_xy is a valid tile.
 */
void render_screen_to_map_pixel(int x, int y, int* map_x, int* map_y);

/*
 * Converts world space to screen space.
 */
void render_world_space_to_screen(const vec3 world, float* x, float* y);

/*
 * Renders `map` to the screen.
 */
void render(const Map* map);

#endif
