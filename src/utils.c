#include "utils.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void rect_normalize_inplace(Rect* rect) {
    Rect r = *rect;
    if (r.r < r.l) {
        rect->l = r.r;
        rect->r = r.l;
    }
    if (r.b < r.t) {
        rect->b = r.t;
        rect->t = r.b;
    }
}

void rect_normalize(Rect* out, const Rect* in) {
    if (in->r < in->l) {
        out->l = in->r;
        out->r = in->l;
    } else {
        out->r = in->r;
        out->l = in->l;
    }
    if (in->b < in->t) {
        out->b = in->t;
        out->t = in->b;
    } else {
        out->t = in->t;
        out->b = in->b;
    }
}

bool rect_contains(Rect* r, int x, int y) {
    bool outside = x < r->l || x > r->r || y < r->t || y > r->b;
    return !outside;
}

int distance_to_squared(int tx, int ty, int fx, int fy) {
    int dx = tx - fx;
    int dy = ty - fy;
    return dx * dx + dy * dy;
}
