#include "utils.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

void rect_normalize_inplace(Rect* rect) {
    Rect r = *rect;
    if (r.r < r.l) {
        rect->l = r.r;
        rect->r = r.l;
    }
    if (r.b < r.t) {
        rect->b = r.t;
        rect->t = r.b;
    }
}

void rect_normalize(Rect* out, const Rect* in) {
    if (in->r < in->l) {
        out->l = in->r;
        out->r = in->l;
    } else {
        out->r = in->r;
        out->l = in->l;
    }
    if (in->b < in->t) {
        out->b = in->t;
        out->t = in->b;
    } else {
        out->t = in->t;
        out->b = in->b;
    }
}

bool rect_contains(Rect* r, int x, int y) {
    bool outside = x < r->l || x > r->r || y < r->t || y > r->b;
    return !outside;
}

int distance_to_squared(int tx, int ty, int fx, int fy) {
    int dx = tx - fx;
    int dy = ty - fy;
    return dx*dx + dy*dy;
}

char* strdup(const char* src) {
    size_t len = strlen(src);
    char* dest = malloc(len + 1);
    if (dest == NULL) {
        fprintf(stderr, "Malloc failed\n");
        exit(-1);
    }
    memcpy(dest, src, len);
    dest[len] = '\0';
    return dest;
}

