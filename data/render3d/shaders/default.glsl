-- Vertex

in vec4 position;
in vec3 normal;
in vec2 uv;
in vec2 model_instance_matrix;

out vec2 uv_v;
out vec3 normal_v;
out vec4 pos;

uniform mat4 model_matrix;
uniform mat4 pv;

void main() {
    normal_v = normal;
    pos = model_matrix * position;
    pos.xy += model_instance_matrix;
    uv_v = uv;
    gl_Position = pv * pos;
}

-- Fragment

in vec3 normal_v;
in vec4 pos;

uniform vec3 camera;
uniform vec3 color;

out vec4 FragColor;

vec3 lamp0_dir = normalize(vec3(1, -1, 2));

void main() {
    vec4 c = vec4(color, 1.0);

    vec3 norm = vec3(normal_v);

    float l = clamp(dot(norm, lamp0_dir), 0.0, 1.0);
    float specular = pow(clamp(dot(normalize(camera - pos.xyz), normalize(reflect(-lamp0_dir, norm))), 0.0, 1.0), 2.0);
    FragColor = c * (0.4 + l*0.8 + specular*0.1);
    FragColor.a = 1.0;
}

-- Fragment.ground

in vec3 normal_v;
in vec4 pos;
in vec2 uv_v;

uniform vec3 camera;
uniform sampler2D snow;

out vec4 FragColor;

vec3 lamp0_dir = normalize(vec3(1, -1, 2));

vec3 s(float scale) {
    vec3 normal = texture2D(snow, uv_v * scale).rgb;
    normal = normal * 2.0 - 1.0;
    return normal;
}

void main() {
    vec3 norm = normalize(s(7)*0.05 * (int(pos.x) % 5)/5 + s(100)*0.05 + normalize(normal_v));

    vec4 c = vec4(0.9, 0.9, 0.95, 1);

    float l = clamp(dot(norm, lamp0_dir), 0.0, 1.0);
    FragColor = c * (0.4 + l*0.8);
    FragColor.a = 1.0;
    /* FragColor = texture2D(snow, uv_v * 100); */
}
