# Source this file for some handy commands

export PATH="$(realpath $(dirname "$PWD/$BASH_SOURCE")):$PATH"

sb2-build () {
    [[ -f build ]] || cmake -B build -DCMAKE_BUILD_TYPE=Debug && cmake --build build
}

sb2    () { sb2-build &&                    ./bin/sb2 -n "$USER" -h map "$@"; }
sb2gdb () { sb2-build && gdb -ex run --args ./bin/sb2 -n "$USER" -h map "$@"; }
sb2val () { sb2-build && valgrind           ./bin/sb2 -n "$USER" -h map "$@"; }
