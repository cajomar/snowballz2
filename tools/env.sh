# Source this file for some handy commands

export PATH="$(realpath $(dirname "$PWD/$BASH_SOURCE")):$PATH"

DEFAULT_MAP="$HOME/.local/share/snowballz2/map.sb2"

sb2    () { cmake --build build &&                    ./bin/sb2 -n "$USER" -h "$DEFAULT_MAP" "$@"; }
sb2gdb () { cmake --build build && gdb -ex run --args ./bin/sb2 -n "$USER" -h "$DEFAULT_MAP" "$@"; }
sb2val () { cmake --build build && valgrind           ./bin/sb2 -n "$USER" -h "$DEFAULT_MAP" "$@"; }
