import bpy
import sys
import importlib

sys.path.append(bpy.path.abspath("//scripts"))

import building as m

importlib.reload(m)
m.register()
