import math
import sys
import bpy
from collections import OrderedDict

d = bpy.path.abspath("//scripts")
if d not in sys.path:
    sys.path.append(d)
import utils  # noqa E402


bl_info = {
    "name": "Snowballz2 Penguin Addon",
    "blender": (2, 92, 0),
    "category": "System",
}


stuff = {
        "Worker": {
            "None": {
                "Stand": ["Stand"],
                "Walk": ["Walk"],
                "Unload": ["Sit", "Hammer"],
                },
            "Tree": {
                "Stand": ["Stand", "Carry"],
                "Walk": ["Walk", "Carry"],
                "Harvest": ["Stand", "Chop"],
                "Unload": ["Bend", "Unload Tree"],
                },
            },
        "Warrior": {
            "None": {"Stand": ["Stand"], "Walk": ["Walk"]},
            "Snowball": {"Unload": ["Throw"]},
            },
        }

for p in stuff:
    stuff[p] = OrderedDict(stuff[p])
    for r in stuff[p]:
        stuff[p][r] = OrderedDict(stuff[p][r])
stuff = OrderedDict(stuff)


NUM_DIRECTIONS = 16


def first(s):
    return next(iter(s))


def firstval(s):
    return s[first(s)]


def resources():
    return firstval(stuff)


def actions():
    return firstval(firstval(stuff))


def cycle(s):
    if type(s) == list:
        s.append(s.pop(0))
    elif type(s) == OrderedDict:
        s.move_to_end(first(s))
    else:
        raise TypeError(f"cycle() expects list or OrderedDict, not {type(s)}")


def cycleto(list, e):
    i = list.index(e)
    s = list[:i]
    del list[:i]
    list.extend(s)


def update_types(context):
    p = first(stuff)
    r = first(resources())
    a = first(actions())

    context.scene.node_tree.nodes["File Output"].base_path = \
        f"//../rawdata/penguins/{p.lower()}/{a.lower()}_{r.lower()}"

    rig = context.scene.objects[f"{p} Metarig"]

    # rig.animation_data.action = bpy.data.actions[f"{a} {r}"]

    for t in rig.animation_data.nla_tracks:
        t.mute = not (t.name == "Base" or t.name in stuff[p][r][a])


    for o in bpy.data.collections[f"{p} Outfit"].children:
        if o.type == "ARMATURE":
            try:
                o.animation_data.action = bpy.data.actions[f"{o.name} {a} {r}"]
            except KeyError:
                o.animation_data.action = bpy.data.actions[f"{o.name} {a}"]

    for c in bpy.data.collections["Outfits"].children:
        c.hide_render = p.lower() not in c.name.lower()
        c.hide_viewport = c.hide_render

    for i, t in enumerate(stuff):
        c = bpy.data.collections[f"{t} Penguin"]
        c.hide_render = bool(i)
        c.hide_viewport = c.hide_viewport if i else False
        context.scene.objects[f"{t} Metarig"].location[0] = i * 10

    for c in bpy.data.collections["Actions"].children:
        show = c.name == f"{a} {r}" or (c.name == r and f"{a} {r}" not in bpy.data.collections["Actions"].children)
        c.hide_render = not show
        c.hide_viewport = c.hide_render
        if show:
            for o in c.objects:
                if o.animation_data:
                    for t in o.animation_data.nla_tracks:
                        t.mute = a not in t.name



class ChangeActivePenguin(bpy.types.Operator):
    """Cycle through the penguin types"""
    bl_idname = "sb2.change_active_penguin"
    bl_label = "Change Active Penguin"
    bl_options = {"UNDO"}

    def execute(self, context):
        cycle(stuff)
        update_types(context)
        return {"FINISHED"}


class ChangeActiveResource(bpy.types.Operator):
    """Cycle through the resource types"""
    bl_idname = "sb2.change_active_resource"
    bl_label = "Change Active Resource"
    bl_options = {"UNDO"}

    def execute(self, context):
        cycle(resources())
        update_types(context)
        return {"FINISHED"}


class ChangeActiveAction(bpy.types.Operator):
    """Cycle through the actions"""
    bl_idname = "sb2.change_active_action"
    bl_label = "Change Active Action"
    bl_options = {"UNDO"}

    def execute(self, context):
        cycle(actions())
        update_types(context)
        return {"FINISHED"}


class RenderAnimation(bpy.types.Operator):
    """Render a penguin animation"""
    bl_idname = "sb2.render"
    bl_label = "Render active animation"

    def execute(self, context):
        update_types(context)
        file_output_node = context.scene.node_tree.nodes["File Output"]
        with utils.UnmutedNode(file_output_node):
            print(f"Rendering {first(stuff)}")
            rig = bpy.data.objects[f"{first(stuff)} Metarig"]
            for i in range(NUM_DIRECTIONS):
                rig.rotation_euler = (0, 0,
                                      math.pi * 2 / NUM_DIRECTIONS * i
                                      + math.pi/2)
                p = utils.get_padding(context)
                file_output_node.file_slots[0].path = f"body_{i}_{p}.png"
                file_output_node.file_slots[1].path = f"outfit_{i}_{p}.png"
                file_output_node.file_slots[2].path = f"shadow_{i}_{p}.png"
                bpy.ops.render.render(animation=True)

            rig.rotation_euler = (0, 0, 0)
        return {"FINISHED"}


class RenderAllAnimations(bpy.types.Operator):
    """Render all penguin animations"""
    bl_idname = "sb2.render_all"
    bl_label = "Render all animations"

    def execute(self, context):
        for i in range(len(stuff)):
            cycle(stuff)
            for j in range(len(resources())):
                cycle(resources())
                for k in range(len(actions())):
                    cycle(actions())
                    bpy.ops.sb2.render()
        return {"FINISHED"}

class Snowballz2ToolsPanel(bpy.types.Panel):
    bl_label = "Snowballz2 Tools"
    bl_idname = "SCENE_PT_snowballz2_tools"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "scene"

    def draw(self, context):
        self.layout.operator("sb2.change_active_penguin")
        self.layout.operator("sb2.change_active_action")
        self.layout.operator("sb2.change_active_resource")


class Snowballz2ToolsPanelRender(bpy.types.Panel):
    bl_label = "Snowballz2 Tools"
    bl_idname = "LAYOUT_PT_sb2_render"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "render"

    def draw(self, context):
        self.layout.operator("sb2.render")
        self.layout.operator("sb2.render_all")


classes = [
    ChangeActiveAction,
    ChangeActivePenguin,
    ChangeActiveResource,
    RenderAnimation,
    RenderAllAnimations,
    Snowballz2ToolsPanel,
    Snowballz2ToolsPanelRender,
    ]


def register():
    for c in classes:
        bpy.utils.register_class(c)


def unregister():
    for c in classes:
        bpy.utils.unregister_class(c)


if __name__ == "__main__":
    register()
