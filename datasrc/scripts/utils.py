import os
import bpy


class UnmutedNode:
    """Unmutes the specified for the duration of a with-statment.
    Example:
        >>> with UnmutedNode(node):
        >>>     pass
    """
    def __init__(self, node):
        self.node = node
    def __enter__(self):
        self.node.mute = False
    def __exit__(self, *args):
        self.node.mute = True


class NoComposite:
    def __init__(self, scene):
        self.render = scene.render
    def __enter__(self):
        self.render.use_compositing = False
    def __exit__(self, *args):
        self.render.use_compositing = True


def get_padding(context):
    """Returns a string of # the length of the decimal width of the current
    frame"""
    return "#" * len(str(context.scene.frame_current))


def rename_image(p):
    os.rename("%s%04d" % (p, bpy.context.scene.frame_current), p)


def rename_output_images(out, context):
    for s in out.file_slots:
        try:
            p = bpy.path.abspath(os.path.join(out.base_path, s.path))
            os.rename("%s%04d" % (p, context.scene.frame_current), p)
        except FileNotFoundError as e:
            print("Failed:", e)
