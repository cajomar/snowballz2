import bpy
from os.path import basename
import sys

d = bpy.path.abspath("//scripts")
if d not in sys.path:
    sys.path.append(d)

import utils  # noqa E402

bl_info = {
    "name": "Snowballz2 Building Addon",
    "blender": (2, 92, 0),
    "category": "System",
}


class Render(bpy.types.Operator):
    """Renders the building"""
    bl_idname = "sb2.render"
    bl_label = "Render the building"

    def execute(self, context):
        out = context.scene.node_tree.nodes["File Output"]
        with utils.UnmutedNode(out):
            # Render building
            bpy.ops.render.render()
            utils.rename_output_images(out, context)

        base = "//../rawdata/buildings/" + \
            basename(bpy.data.filepath).replace('_', '.').split('.')[0]

        # Render resource stockpiles
        for vln in ("Tree", "Fish"):
            if vln in context.scene.view_layers:
                context.window.view_layer = context.scene.view_layers[vln]
                for i in range(1, 4):
                    hide = False
                    for c in bpy.data.collections[vln].children:
                        c.hide_render = hide
                        if str(i) in c.name:
                            hide = True
                    context.scene.render.filepath = \
                        f"{base}/overlays/{vln.lower()}{i}.png"

                    with utils.NoComposite(context.scene):
                        bpy.ops.render.render(layer=vln, write_still=True)

        return {"FINISHED"}


class Snowballz2BuildingToolsPanel(bpy.types.Panel):
    bl_label = "Snowballz2 Tools"
    bl_idname = "SCENE_PT_layout"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "render"

    def draw(self, context):
        self.layout.operator("sb2.render")


classes = [
    Render,
    Snowballz2BuildingToolsPanel,
    ]


def register():
    for c in classes:
        bpy.utils.register_class(c)


def unregister():
    for c in classes:
        bpy.utils.unregister_class(c)


if __name__ == "__main__":
    register()
