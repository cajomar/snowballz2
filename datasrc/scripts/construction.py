import sys
import bpy

d = bpy.path.abspath("//scripts")
if d not in sys.path:
    sys.path.append(d)

import utils  # noqa E402

bl_info = {
    "name": "Snowballz2 Building Construction Addon",
    "blender": (2, 92, 0),
    "category": "System",
}


class Render(bpy.types.Operator):
    bl_idname = "sb2.render"
    bl_label = "Render the building"

    def execute(self, context):
        out = context.scene.node_tree.nodes["File Output"]
        with utils.UnmutedNode(out):
            for i in range(1, 4):
                for c in bpy.data.collections["Building"].children:
                    c.hide_render = str(i) not in c.name
                out.file_slots[0].path = f"building_construction{i}.png"
                out.file_slots[1].path = f"shadow_construction{i}.png"
                bpy.ops.render.render()
                utils.rename_output_images(out, context)

        return {"FINISHED"}


class Snowballz2BuildingConstructionToolsPanel(bpy.types.Panel):
    bl_label = "Snowballz2 Tools"
    bl_idname = "SCENE_PT_layout"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "render"

    def draw(self, context):
        self.layout.operator("sb2.render")


classes = [
    Render,
    Snowballz2BuildingConstructionToolsPanel,
    ]


def register():
    for c in classes:
        bpy.utils.register_class(c)


def unregister():
    for c in classes:
        bpy.utils.unregister_class(c)


if __name__ == "__main__":
    register()
