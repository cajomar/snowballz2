const Build = @import("std").Build;
const zcc = @import("compile_commands");
const std = @import("std");

pub fn build(b: *Build) void {
    const optimize = b.standardOptimizeOption(.{});
    const target = b.standardTargetOptions(.{});

    const flags = [_][]const u8{
        // "-std=c17",
        "-Wall",
        "-Wextra",
        "-pedantic",
        "-Werror=implicit-function-declaration",
    };

    const common = b.addStaticLibrary(.{ .name = "common", .target = target, .optimize = optimize });
    common.addCSourceFiles(.{
        .files = &.{
            "src/map.c",
            "src/map_saver.c",
            "src/map_loader.c",
            "src/prefs.c",
            "src/utils.c",
            "src/jobs.c",
            "src/writer.c",
            "src/reader.c",
            "src/types.c",
        },
        .flags = &flags,
    });

    common.linkLibC();

    const host = b.addStaticLibrary(.{
        .name = "host",
        .target = target,
        .optimize = optimize,
        .root_source_file = b.path("src/host/game_player_ai.zig"),
    });
    host.addIncludePath(b.path("src/host"));
    host.addCSourceFiles(.{ .files = &.{
        "src/host/ai.c",
        "src/host/game_host.c",
        "src/host/nav.c",
        "src/host/nav_path.c",
        "src/host/nav_resource.c",
    }, .flags = &flags });

    host.linkSystemLibrary("enet");
    host.linkLibC();

    const server = b.addExecutable(.{ .name = "sb2-server", .target = target, .optimize = optimize });
    server.addCSourceFiles(.{ .files = &.{"src/server/server.c"}, .flags = &flags });
    server.linkLibrary(host);
    server.linkLibrary(common);
    server.linkLibC();
    b.installArtifact(server);

    const render2d = b.addStaticLibrary(.{ .name = "render2d", .target = target, .optimize = optimize });
    render2d.addCSourceFiles(.{ .files = &.{
        "src/client/render2d/render.c",
        "src/client/render2d/render_debug.c",
        "src/client/render2d/sprites.c",
    }, .flags = &flags });
    render2d.linkLibC();
    render2d.linkSystemLibrary("lib2d");

    const sb2 = b.addExecutable(.{ .name = "sb2", .target = target, .optimize = optimize });
    sb2.addCSourceFiles(.{ .files = &.{
        "src/client/command.c",
        "src/client/event_layer.c",
        "src/client/game.c",
        "src/client/gui.c",
        "src/client/game_client.c",
        "src/client/lobby.c",
        "src/client/main.c",
        "src/client/map_editor.c",
        "src/client/minimap.c",
        "src/client/selection.c",
    }, .flags = &flags });
    sb2.linkLibC();
    sb2.linkSystemLibrary("ink");
    sb2.linkSystemLibrary("SDL2");
    sb2.linkLibrary(render2d);
    sb2.linkLibrary(common);
    sb2.linkLibrary(host);
    b.installArtifact(sb2);

    const run_exe = b.addRunArtifact(sb2);
    const run_step = b.step("run", "Run the game");
    run_step.dependOn(&run_exe.step);

    var targets = std.ArrayList(*std.Build.Step.Compile).init(b.allocator);
    targets.append(common) catch @panic("OOM");
    targets.append(host) catch @panic("OOM");
    targets.append(server) catch @panic("OOM");
    targets.append(render2d) catch @panic("OOM");
    targets.append(sb2) catch @panic("OOM");

    zcc.createStep(b, "zcc", .{ .targets = targets.toOwnedSlice() catch @panic("OOM") });
}
