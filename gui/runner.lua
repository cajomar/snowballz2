function load_maps(source)
  for i = 1,4 do
    local d = source:append("maps")
    d:set_string("author", "me")
    d:set_string("name", "map"..i)
    d:set_int("size", 2 ^ ((i % 4) + 6))
  end
end
manager.on_source("lobby", function (source)
  manager.on_impulse(source, "start_game", function () source:trigger("launch") end)
  load_maps(source)
end)

manager.on_source("game", function (source)
  manager.on_change(source, "build_mode", function () source:set_bool("build_mode", false) end)
  manager.on_impulse(source, "end", function () ink.source("lobby"):trigger("end_game", false) end)
end)

manager.on_source("map_editor", function (source)
  manager.on_impulse(source, "create", function () source:set_bool("map_loaded", true) end)
  manager.on_impulse(source, "open", function () source:set_bool("map_loaded", true) end)
  load_maps(source)
end)
